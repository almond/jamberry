beautifulsoup
distribute
django==1.3
django-app-plugins
django-compressor
django-extensions
django-flatpages-tinymce
django-keyedcache
django-livesettings
django-registration
django-signals-ahoy
django-tinymce
django-threaded-multihost
# A PIL wrapper that does the magic. Also make sure that you have 
# You python-dev headers installed or it will break.
pillow
pycrypto
python-memcached
satchmo
south
sorl-thumbnail==3.2.5
yolk
# On Mac download from source.
MySQL-python==1.2.3
