<?php
include('nusoap-0.9.5/lib/nusoap.php');
$service_url = "http://www.integracoreb2b.com/intCore/IncomingOrders.asmx?WSDL";

// Unpack post to a nicer named variable
$p = json_decode(file_get_contents("php://input"), true);

// Also makes it easier to work with
$username = $p['username'];
$password = $p['password'];
$contact = $p['contact'];
$items = $p['order_items'];

// Create the client
$client = new nusoap_client($service_url, 'wsdl');

$err = $client->getError();

// If error has been created by the client...
if($err) {
    echo json_encode(array('status'=>'error', 'message'=>$err));
    return false;
}

// Format the sent timestamp in the order to the weird format asked for in SOAP service
$time = strtotime($p['time_stamp']);

$p['timestamp'] = date("Y-m-d\TH:i:s", $time);

// Now generate order details
$order_items = '';
foreach ($items as $value) {
    $order_items .= "<OrderDetail>\n";
    $order_items .= "   <ItemID>$value[sku]</ItemID>\n";
    $order_items .= "   <Quantity>$value[quantity]</Quantity>\n";
    $order_items .= "   <SalePrice>$value[price]</SalePrice>\n";
    $order_items .= "</OrderDetail>\n";
}

//$client->setUseCurl(true);

$headers = <<<EOT
<AuthHeader xmlns="http://www.integracoreb2b.com/">
    <Username>$username</Username>
    <Password>$password</Password>
</AuthHeader>
EOT;

// Separate everything because PHP sux

$xml_body = <<<EOT
<OrderImport xmlns="http://www.integracoreb2b.com/">
      <Orders>
        <Order>
          <SiteID>1</SiteID>
          <OrderNumber>JM$p[id]</OrderNumber>
          <ShipToCustID>JAMBERRY</ShipToCustID>
          <BillToCustID>JAMBERRY</BillToCustID>
          <CreateDate>$p[timestamp]</CreateDate>
          <RequiredShipDate>$p[timestamp]</RequiredShipDate>
          <Carrier>FDX</Carrier>
          <CarrierServiceCode>FDX 01</CarrierServiceCode>
          <ThirdPartyAccountNumber></ThirdPartyAccountNumber>
          <PrepaidOrCollect>03</PrepaidOrCollect>
          <ShipToContact>$contact[first_name]</ShipToContact>
          <ShipToName>$contact[first_name] $contact[last_name]</ShipToName>
          <ShipToAddr1>$p[ship_street1]</ShipToAddr1>
          <ShipToAddr2>$p[ship_street2]</ShipToAddr2>
          <ShipToAddr3></ShipToAddr3>
          <ShipToCity>$p[ship_city]</ShipToCity>
          <ShipToState>$p[ship_state]</ShipToState>
          <ShipToZip>$p[ship_postal_code]</ShipToZip>
          <ShipToCountry>$p[ship_country]</ShipToCountry>
          <ShipToTelephone>$contact[phone]</ShipToTelephone>
          <ShipToFax></ShipToFax>
          <CustPoNum></CustPoNum>
          <BillToPoNum></BillToPoNum>
          <RecipientEmailAddress>$contact[email_address]</RecipientEmailAddress>
          <CustomData1></CustomData1>
          <CustomData2></CustomData2>
          <CustomData3></CustomData3>
          <DropShip>R</DropShip>
          <ResidentialFlag>T</ResidentialFlag>
          <PackSlipRequired>T</PackSlipRequired>
          <OrderDetails>
            $order_items
          </OrderDetails>
          <OrderNotes>
            <OrderNote>
                <NoteType>71</NoteType>
                <NoteText>$p[notes]</NoteText>
            </OrderNote>
          </OrderNotes>
        </Order>
      </Orders>
</OrderImport>
EOT;

$client->setHeaders($headers);

$result = $client->call('OrderImport', $xml_body);

// Check for a fault
if($client->fault) {
    echo '{ status: \'fault\', message: \'';
    print_r($result);
    echo '\'}';
} else {
    $response = array(
        'status' => 'success',
        'message' => $result
    );
    echo json_encode($response);
}
