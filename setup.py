from setuptools import setup, find_packages

setup(
    name="crossmarket",
    version="0.1",
    description="",
    author="Aman Kumar Jain",
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'setuptools',
    ],
)
