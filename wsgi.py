import os
import sys
import site

PATH = '/var/www/vhosts/jamberry'

os.environ['PYTHON_EGG_CACHE'] = '/var/www/vhosts/jamberry/env/eggs'

# Virtualenv
site.addsitedir('%s/env/lib/python2.6/site-packages' % PATH)

# Django settings
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

# Project Trunk
sys.path.append('%s/src/crossmarket' % PATH)
sys.path.append('%s/src' % PATH)

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
