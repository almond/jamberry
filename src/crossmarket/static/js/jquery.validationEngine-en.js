

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"none",
						"alertText":"* This field is required",
						"alertTextCheckboxMultiple":"* Please select an option",
						"alertTextCheckboxe":"* This checkbox is required"},
					"length":{
						"regex":"none",
						"alertText":"*Between ",
						"alertText2":" and ",
						"alertText3": " characters allowed"},
					"maxCheckbox":{
						"regex":"none",
						"alertText":"* Checks allowed Exceeded"},	
					"minCheckbox":{
						"regex":"none",
						"alertText":"* Please select ",
						"alertText2":" options"},	
					"confirm":{
						"regex":"none",
						"alertText":"* Fields do not match"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Invalid phone number"},
		            "addressNoPOBox": {
		                "regex": "/^(?!([pP]{1}[.]*[oO]{1}[.]*[ ]*[bB]{1}[oO]{1}[xX]{1}))/",
		                "alertText": "* Please enter a valid UPS shipping address. P.O. Boxes are not accepted."
		            },
                     "email": {
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":"* Invalid email address"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Invalid date, must be in YYYY-MM-DD format"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Numbers only"},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* No special caracters allowed"},	
					"ajaxUser":{
					    "file": "../service/Account.asmx/ValidateUser",
						"extraData":"",
						"alertTextOk":"* This user is available",	
						"alertTextLoad":"* Checking, please wait",
						"alertText": "* This username is not available."
		                },
		           "ajaxUser2": {
		                    "file": "../../service/Account.asmx/ValidateUser",
		                    "extraData": "",
		                    "alertTextOk": "* This user is available",
		                    "alertTextLoad": "* Checking, please wait",
		                    "alertText": "* This username is not available."
		                },
		            "ajaxWebAlias": {
		                    "file": "../service/Account.asmx/ValidateUser",
		                    "extraData": "",
		                    "alertTextOk": "* This web alias is available",
		                    "alertTextLoad": "* Checking, please wait",
		                    "alertText": "* This web alias is not available."
		                },
		                "ajaxWebAlias2": {
		                    "file": "../../service/Account.asmx/ValidateUser",
		                    "extraData": "",
		                    "alertTextOk": "* This web alias is available",
		                    "alertTextLoad": "* Checking, please wait",
		                    "alertText": "* This web alias is not available."
		                },
		            "ajaxEmail": {
		                "file": "../service/Account.asmx/ValidateUser",
		                "extraData": "",
		                "alertTextOk": "",
		                "alertTextLoad": "* Checking, please wait",
		                "alertText": "* This email address is already used by another account."
		            },
		            "ajaxEmail2": {
		                "file": "../../service/Account.asmx/ValidateUser",
		                "extraData": "",
		                "alertTextOk": "",
		                "alertTextLoad": "* Checking, please wait",
		                "alertText": "* This email address is already used by another account."
		            },
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText": "* Letters only"
		        },
		"validateSSN": {
                        "nname": "validateSSN",
                        "alertText": "* Please enter a valid social security number."
                    }
				}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});