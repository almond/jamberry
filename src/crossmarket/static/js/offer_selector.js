  //offerless_products_slug

  /*check if a value exists in an array*/
  Array.prototype.exists = function(search){
    for(var i=0; i<this.length; i++){
      if (this[i] == search) return true;
    }
    return false;
  }

  /*function offer in sidecart*/
  function show_offer_in_cart(){
      offerable_quantity=0;
      price_per_item=0
      cart_sub_total=0
      for(item in item_in_cart){
          if(!offerless_products_slug.exists(item_in_cart[item].slug)){
              offerable_quantity=offerable_quantity+item_in_cart[item].quantity
	      price_per_item=item_in_cart[item].unit_price //bad bad way! but will work as of now
          }
	  cart_sub_total=cart_sub_total+(item_in_cart[item].unit_price*item_in_cart[item].quantity)
      }
      ao=applicable_offers()
      if(ao.length>0){
	  ao=ao[0]
	  set_unit=parseInt(ao.offer.buy)+parseInt(ao.offer.free)
	  free_units=parseInt(offerable_quantity/set_unit)
	  if(free_units>0){
	      $('#total-of-cart').html('Sub T. $'+cart_sub_total.toFixed(2)+'<br/>Disc. $'+((free_units*price_per_item).toFixed(2))+'<br/>Total $'+(cart_sub_total-(free_units*price_per_item)).toFixed(2))
	  }
      }
  }


  /* Remove a product fron the cart */
  function rem_product(pid){
      url=remove_from_cart_url
      $.ajax({
	  url:url,
	  type:'POST',
	  dataType:'json',
	  data:{'product_id':pid, 'csrfmiddlewaretoken':csrfmiddlewaretoken},
	  success:function(data){
	      if (data.status.success) {
		  $('td.count').html('')
                  for (product in data.item_in_cart) {
                      $('td.count[data-id=' + product + ']').html((data.item_in_cart[product]['quantity']) + ' item(s) in cart. <a class="cross" href="javascript:void(0)" onClick="rem_product('+product+')"><span>x</span></a>')
                  }
                  item_in_cart = data.item_in_cart
                  total_items = 0
                  total_cart = 0
                  for (i in item_in_cart) {
                      total_items = total_items + item_in_cart[i]['quantity']
                      total_cart = total_cart + (item_in_cart[i]['quantity'] * item_in_cart[i]['unit_price'])
                  }
                  $('#count-of-items').html(total_items)
                  $('#total-of-cart').html('$'+total_cart.toFixed(2))
              }
	      
	  }
      })
    
  }

  /* Dropdown to select an offer if there are more than one */
  function offer_selector(){
    var empty_cart=true
    for(i in item_in_cart){
      empty_cart=false
    }
    if(empty_cart){
      $.fallr({
        content : "Your cart is empty. <br/>Please select some products to checkout."
      });
      return true
    }

    if(credit.available==false || parseFloat(credit.amount)<0){
      /* No offers or credits, go checkout */
      window.location=$('#checkout_link').attr('data-orig-url') 
    }else{
	fetch_credit_amount()
    }
      
    /*
    else if(offers.length==0 && (credit.available==true && parseFloat(credit.amount)>0)){
      // Has credits, fetch the max credit amount 
      }else if(offers.length>0 && (credit.available==false || parseFloat(credit.amount)<0)){
      // Has offers, expecting it will always be only one 
      //selecting_offer({% for o in offers %}{% if forloop.first %}{{o.pk}}{% endif %}{% endfor %})
      applicable=applicable_offers()
      show_select_offer_box(applicable)
    }else{ 
      // Has offers and credit 
      applicable=applicable_offers()
      show_select_offer_box(applicable)											
    }
    */
  }

  /* check if offers are applicable */
  function applicable_offers(){
    var applicable_offers=[]
    var cart_itm_total=0
    var offerable_quantity=0
    for(item in item_in_cart){
      cart_itm_total=cart_itm_total+item_in_cart[item].quantity
	if(!offerless_products_slug.exists(item_in_cart[item].slug)){
	    offerable_quantity=offerable_quantity+item_in_cart[item].quantity
	}
    }

      for(offer=0; offer<offers.length; offer++){
	buy=parseInt(offers[offer].buy)
	free=parseInt(offers[offer].free)
	set_count=buy+free

	if(offerable_quantity%set_count==0){
	    applicable_offers.push({offer:offers[offer], need_more:0})
	}else if( (offerable_quantity+free)%set_count==0 ){
	    applicable_offers.push({offer:offers[offer], need_more:free})
	}else{
	    units=parseInt(offerable_quantity/(buy+free))
	    if(units>0){
		applicable_offer=true
		applicable_offers.push({offer:offers[offer], need_more:0})	
	    }
	}
    }
    return applicable_offers
  }

  /* select offer box */
  function show_select_offer_box(offers){
      var offer_html=''
      var offer_count=0;
      var first_offer_id;
      for(o=0; o<offers.length; o++){
        offer=offers[o]['offer']
        offer_html=offer_html+'<option value="'+(offer.id)+'">'+(offer.description)+'</option>'  
        if(offer_count==0){
          first_offer_id=offer.id
        }
        offer_count=offer_count+1
      }
      if(credit.available && parseFloat(credit.amount)>0){
        if(offer_html==''){
          fetch_credit_amount()
          return true;
        }
        offer_html=offer_html+'<option value="product-credit">Use Product Credit</option>'  
      }else{
        if(offer_html!='' && offer_count==1){
          selecting_offer(first_offer_id)
	  return true;
        }
      }

      if(offer_html==''){
        window.location=$('#checkout_link').attr('data-orig-url')
        return true;
      }
      $.fallr('show', {
        icon        : 'form',
        content     : ''
          + '<form>'
          +     'What kind of disount do you want?'
          +     '<select id="type_of_credit">'
          +           offer_html
          +     '</select>'
          + '</form>',
        buttons : {
          button1 : {text: 'Submit', onclick: select_offer},
          button4 : {text: 'Cancel'}
        }	
      }); 

  }
  /* action when an offer is selected via dropdown */
  function select_offer(){
    data=$('#type_of_credit').val()
    $.fallr('hide', function(){
      if(data=='product-credit'){
        //fetch the max credit amount 
        fetch_credit_amount()     
      }else if(data!=''){
        selecting_offer(data)  
      }
    })
  }

  /* setting the offer in backend, it is not the credits. action for credits below */ 
function selecting_offer(offer, donot_check_offer_items){

    if(!donot_check_offer_items){
	avail_offers=applicable_offers()
	need_more=0;
	for(o=0; o<avail_offers.length; o++){
	    cur_offer=avail_offers[o]
	    if(cur_offer.offer.id==offer){
		need_more=cur_offer.need_more
	    }
	}

	if(need_more){
	    $.fallr({
		content : "Please select "+need_more+" free item(s).",
		buttons : {
		    button1 : {text: 'Okay',},
		    button2 : {text: 'No, please checkout!', onclick:function(){selecting_offer(offer, true)}}
		}
	    });
	    return false;
	}
    }
    $.ajax({
      url:set_offer_url,
      type:'POST',
       dataType:'json',
       data:{offer:offer, csrfmiddlewaretoken:csrfmiddlewaretoken},
       success:function(data){
         if(data.status.success){
           window.location=$('#checkout_link').attr('data-orig-url')
         }
       }
    })
  }

  /* fetching the max. credit amount */
  function fetch_credit_amount(){
    $.ajax({
      url:credit.amt_url,
      success:function(data){
        if(parseInt(data)>0){
          credit.max=data
          ask_amount()
        }else{
          window.location=$('#checkout_link').attr('data-orig-url')
        }
      }
    })
  }  

  /* ask amount of credit to be applied */
  function ask_amount(){
    $.fallr(
      'show',
      {
        buttons : {
          button1 : {text: 'Submit', onclick:apply_credit},
            button2 : {text: 'Cancel',  onclick:function(){ window.location=$('#checkout_link').attr('data-orig-url') }}
      },
      content : '<p>You can apply upto '+(credit.amt_prefix)+((credit.max < credit.amount)?credit.max:credit.amount)+' '+(credit.label)+'.</p>'
               +'How much should I apply? &nbsp;&nbsp;<img src="/media/manage/img/ajax.gif" id="credit_apply" style="vertical-align:middle; display:none">'
               +'<input type="text" id="credit_value" /'+'><p></p>',
      icon    : 'form'
    });
  }
  
  /* apply credit */
  function apply_credit(){
    $('#fallr-buttons span').remove()
    var amount = $('#credit_value').val();
    if(amount <= parseFloat((credit.max < credit.amount)?credit.max:credit.amount)){
	$('#credit_apply').show();
	$.ajax({
	  url:credit.apply_url,
	  type:'POST',
	  dataType:'json',
	  data:{amount:amount, csrfmiddlewaretoken:csrfmiddlewaretoken},
	  success:function(data){
	    $('#credit_apply').hide();
	    if(data.status.success){
	      $.fallr('hide');
	      window.location=$('#checkout_link').attr('data-orig-url')
	    }else{
	      $('#fallr-buttons').prepend('<span>'+(data.status.reason)+'</span>')
	    }
	  }
	})
    }else{
      $('#fallr-buttons').prepend('<span>Max '+(credit.amt_prefix)+(((credit.max < credit.amount)?credit.max:credit.amount))+' allowed.</span>')		
    }
  }
