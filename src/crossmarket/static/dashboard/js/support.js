/* Specific Functions */
update_credits=function(data){
    //console.log(data)
    $('#bsc_amt').html(data.bsc)
    $('#pc_amt').html(data.pc)
}
/* Functions End */


jQuery.fn.serializeObject = function() {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function() {
	var value;

	if (this.value != null) {
	    value = this.value;
	} else {
	    value = '';
	}

	if (objectData[this.name] != null) {
	    if (!objectData[this.name].push) {
		objectData[this.name] = [objectData[this.name]];
	    }

	    objectData[this.name].push(value);
	} else {
	    objectData[this.name] = value;
	}
    });

    return objectData;
};

attach_ajax=function(){
    $('.ajaxForm').each(function(form){	   
	$(this).click(function(e){
	    e.preventDefault()
	})

	$(this).find('.save.add').click(function(e){
	    e.preventDefault()
	    forms=$(this).closest("form")
	    $(forms[0]).submit()
	})

	$(this).submit(function(e){
	    e.preventDefault()
	    data=$(this).serializeObject()
	    data['csrfmiddlewaretoken']=csrf_token
	    data['action_url']=$(this).attr('action')
	    if($(this).attr('data-callback')){
		data['action_callback']=$(this).attr('data-callback')
	    }

	    $.ajax({
		data:data,
		url:$(this).attr('action'),
		type:"POST",
		dataType:'json',
		success: function(data){
		    if(data.status.success){
			if(data.status.clear && data.status.url){
			    $('form[action='+(data.status.url)+'] input').val('')
			    $('form[action='+(data.status.url)+'] select').val('')
			}

			if(data.status.callback && data.data){
			    window[data.status.callback](data.data)
			}
			
		    }
		}
	    })
	})

    })
}

var call_ongoing=false;
attach_del=function(){
    $(document).ready(function(){
        $('.del_data').click(function(e){
	    console.log(1)
	    e.stopPropagation() 
            if(call_ongoing){
		alert('Please wait, untill the other address is deleted.')
		return
            }
            del_type=$(this).attr('data-type')
            del_id=$(this).attr('data-id')
            del_url=$(this).attr('data-url')
            call_ongoing=true
            $.ajax({
		'url':del_url,
		'type':'POST',
		'data':{'id':del_id, 'csrfmiddlewaretoken':csrf_token},
		'dataType':'json',
		'success':function(d){
		    $('#del_data_'+del_type+'_'+d.data).remove()
		},

		'complete':function(){
		    call_ongoing=false
		}
            })	    
        })
    })
}

$(document).ready(function(){
    attach_ajax()	
    attach_del()
});
