# Django settings for crossmarket project.
import os


# IntegraCore API configuration. This will be done though PHP anyways
API_INTEGRACORE = {
    "username": "Jamb3rry",
    "password": "5reca8hu",
}


# AvaTax API configuration
API_AVATAX = {
    "username": "admin@jamberrynails.com",
    "password": "O8E48F78",
    "account_number": "1100079965",
    "licence_key": "B0CBB45BB9E1F4B8",
    "version": "1.0",
}

API_MAILCHIMP = {
    "username":"jamberrynails",
    "password":"37eb0abf0a61dc137990af20999bcf42-us2",
    "consultant_list_id":"dad786f1dd",
    "retail_list_id":"5abaa1a393",
}


API_PROPAY = {
    "signup_credentials":"DubNutritionCertString0000001",
    "commission_distribution_credentials":"DubNutritionCertString0000002",
    "auth_token":"b93c7f14-f9ab-4a06-a692-aa52a15d0f47",
    "biller_id":"3478230392483070",
    
}

# End IntegraCore API configuration

DEFAULT_FROM_EMAIL="crossmarketsoftware@gmail.com"

CHECKOUT_PAGE_NAME='opc_checkout'

"""Shop Configurations"""
OFFERLESS_PRODUCTS_SLUG=['jamberry-nail-care-healing-oil-1', 'jamberry-nail-care-healing-oil']

SIGNUP_PRODUCT_SLUG='signup'

BSC_PRODUCT_SLUG='business-supply-credit'
BSC_AMT_SESSION_SLUG='business-supply-credit'

PC_PRODUCT_SLUG='product-credit'
PC_AMT_SESSION_SLUG='product-credit-units'

OFFER_PRODUCT_SLUG='offer' # DO NOT CHANGE. YOU'LL PROVE THE MAYANS RIGHT.
OFFER_AMT_SESSION_SLUG='offer-amount'

DISC_PRODUCT_SLUGS=[PC_PRODUCT_SLUG, BSC_PRODUCT_SLUG, OFFER_PRODUCT_SLUG]
DISC_PRODUCT_SESSION_SLUGS=[BSC_AMT_SESSION_SLUG, PC_AMT_SESSION_SLUG, OFFER_AMT_SESSION_SLUG]

MAIN_PRODUCT_CATEGORY_SLUG='product'
MARKETING_PRODUCT_CATEGORY_SLUG='marketing'
OTHER_PRODUCT_CATEGORY_SLUG='other'

PARTY_WHOLESALE_DISCOUNT_SESSION_KEY='discount_party'
PARTY_WHOLESALE_DISCOUNT_SLUG='Party'
PARTY_ORDERDETAILS_SESSION_KEY='party_order_details'
PARTY_SHIPDETAILS_SESSION_KEY='party_ship_details'

WHOLESALE_DISCOUNT_SESSION_KEY='wholesale_discount'
WHOLESALE_DISCOUNT_TEXT='Wholesale Discount'

WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG='Marketing Materials'
WHOLESALE_DISCOUNT_REGULAR_ORDER_SLUG='Regular Order'

FRONTEND_PARTY_SESSION_SELECT='froentend-party-session'

DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG
DIRNAME = os.path.abspath(os.path.dirname(__file__))

BASE_URL = 'demosite.crossmarketsoftware.com'

ADMINS = (
    ('Aman Kumar Jain', 'mail@amanjain.com'),
)

MANAGERS = ADMINS
DATE_INPUT_FORMATS=(('%m/%d/%Y %I:%M %p'), )

# MRVG: This is a little hackish but until I get a better idea of the
# System I'll rather not move too much around
if os.getenv("LOGNAME") == "axolote":
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'jamberry',
            'USER': 'root',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'jamberry',
            'USER': 'root',
            'PASSWORD': 'sqlt00Rw0rd',
            'HOST': '',
            'PORT': '',
        }
    }


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'crossmarketsoftware@gmail.com'
EMAIL_HOST_PASSWORD = 'ou8124u2'
EMAIL_PORT = 587


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: '/home/media/media.lawrence.com/media/'
MEDIA_ROOT = os.path.join(DIRNAME, 'static/')
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: 'http://media.lawrence.com/media/', 'http://example.com/media/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(DIRNAME, 'static/')
# Example: 'http://media.lawrence.com/static/'
STATIC_URL = '/static/'
ADMIN_MEDIA_ROOT = os.path.join(DIRNAME, 'static/admin/')
# Make sure to use a trailing slash.
# Examples: 'http://foo.com/static/admin/', '/static/admin/'.
ADMIN_MEDIA_PREFIX = '/static-admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    ('styles', os.path.join(STATIC_ROOT, 'styles')),
    ('js', os.path.join(STATIC_ROOT, 'js')),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'uj139v3r!+d5y&-@%za%7w2%($0111-uln97iqrvkf9u3@d!ti'
ADMIN_HASH_SECRET='uj139v3r!+d5y&-@%za%kj098912!@#asd^SD!2$#$d!ti'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TINYMCE_DEFAULT_CONFIG = {
    'theme': 'advanced',
    'valid_elements' : '*[*]'
}

DASHBOARD_DOCUMENTS_PAGINATION = 25

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'threaded_multihost.middleware.ThreadLocalMiddleware',
    'satchmo_store.shop.SSLMiddleware.SSLRedirect',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'apps.account.middleware.DynamicSiteMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware'
)

ROOT_URLCONF = 'crossmarket.urls'

TEMPLATE_DIRS = (os.path.join(DIRNAME, 'templates'))

TEMPLATE_CONTEXT_PROCESSORS = (
    'satchmo_store.shop.context_processors.settings',
    'django.core.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'crossmarket.apps.account.context_processor.referred_by',
    'crossmarket.apps.events.context_processor.has_parties',
    'crossmarket.apps.manage.context_processor.discount_product_slug',
    'crossmarket.apps.manage.context_processor.no_of_items_in_cart',
)

INSTALLED_APPS = (
    # Django Builtins
    'django.contrib.sites',
    'django.contrib.messages',
    'satchmo_store.shop',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.admindocs',
    'django.contrib.contenttypes',
    'django.contrib.comments',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.flatpages',


    # Third Party
    # 'compressor',
    'tinymce',
    'flatpages_tinymce',
    'registration',
    'keyedcache',
    'livesettings',
    'l10n',
    'sorl.thumbnail',
    'satchmo_store.contact',
    'tax',
    'apps.percent',
    #'tax.modules.no',
    #'tax.modules.area',
    #'tax.modules.percent',
    'shipping',
    'shipping.modules.tiered',
    'product',
    'product.modules.configurable',
    'payment',
    'payment.modules.authorizenet',
    #'payment.modules.autosuccess',
    #'payment.modules.giftcertificate',

    'satchmo_utils',
    'satchmo_utils.thumbnail',
    'app_plugins',

    'apps.home',
    'apps.account',
    'apps.geo',
    'apps.compensation',
    'apps.manage',
    'apps.commands',
    'apps.dashboard',
    'apps.events',
    #'apps.messages',
    #'apps.payment_processors.cod',
    'apps.archive',
    'apps.pricing',
    'apps.notice',
    'apps.tools',
    'apps.utils',
    'apps.support',
    'apps.report_builder',
    'apps.one_page_checkout',
    'apps.bus_tour',
)

if os.getenv("LOGNAME") == "axolote":
    INSTALLED_APPS += ("django_extensions",)

AUTHENTICATION_BACKENDS = (
    'satchmo_store.accounts.email-auth.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'apps.dashboard.authenticator.LoginAsUserBackend',
)

from satchmo_settings import *

# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

try:
    from local_settings import *
except:
    SESSION_COOKIE_DOMAIN='.%s'%(BASE_URL)

AUTH_PROFILE_MODULE = 'apps.account.Profile'
