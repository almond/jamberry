# this is an extremely simple Satchmo standalone store.

import logging
import os, os.path

SATCHMO_SETTINGS = {
                    'SHOP_BASE' : '/shop',
                    'MULTISHOP' : True,
                    'SSL': False,
                    #'SHOP_URLS' : patterns('satchmo_store.shop.views',)
                    }

DIRNAME = os.path.dirname(os.path.abspath(__file__))

# trick to get the two-levels up directory, which for the "simple" project should be the satchmo dir
# for most "normal" projects, you should directly set the SATCHMO_DIRNAME, and skip the trick
_parent = lambda x: os.path.normpath(os.path.join(x, '..'))
SATCHMO_DIRNAME = _parent(_parent(DIRNAME))


gettext_noop = lambda s:s

LANGUAGE_CODE = 'en-us'
LANGUAGES = (
   ('en', gettext_noop('English')),
)

# Only set these if Satchmo is part of another Django project
#These are used when loading the test data
SITE_NAME = "simple"
#MEDIA_ROOT = os.path.join(SATCHMO_DIRNAME, 'static/')
DJANGO_PROJECT = 'simple'
DJANGO_SETTINGS_MODULE = 'simple.settings'



# "simple" doesn't have any custom templates, usually you'd have one here for your site.
#TEMPLATE_DIRS = (
#    os.path.join(DIRNAME, "templates"),
#)                                                                                 

SECRET_KEY = 'EXAMPLE SECRET KEY'

##### For Email ########
# If this isn't set in your settings file, you can set these here
#EMAIL_HOST = 'host here'
#EMAIL_PORT = 587
#EMAIL_HOST_USER = 'your user here'
#EMAIL_HOST_PASSWORD = 'your password'
#EMAIL_USE_TLS = True

#These are used when loading the test data
SITE_DOMAIN = "localhost"
SITE_NAME = "Simple Satchmo"

#These are used when loading the test data
SITE_DOMAIN = "localhost"
SITE_NAME = "Simple Satchmo"

# not suitable for deployment, for testing only, for deployment strongly consider memcached.
xCACHE_BACKEND = "locmem:///"
CACHE_TIMEOUT = 60*5
CACHE_PREFIX = "Z"

ACCOUNT_ACTIVATION_DAYS = 7

#Configure logging
LOGFILE = "satchmo.log"
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S')

#fileLog = logging.FileHandler(os.path.join(DIRNAME, LOGFILE), 'w')
#fileLog.setLevel(logging.DEBUG)
# add the handler to the root logger
#logging.getLogger('').addHandler(fileLog)
logging.getLogger('django.db.backends').setLevel(logging.INFO)
logging.getLogger('keyedcache').setLevel(logging.INFO)
logging.getLogger('l10n').setLevel(logging.INFO)
logging.getLogger('suds').setLevel(logging.INFO)
logging.info("Satchmo Started")
