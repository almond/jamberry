from django import forms
from apps.tour.models import Tour


class TourForm(forms.ModelForm):

    class Meta:
        model = Tour