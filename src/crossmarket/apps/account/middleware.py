# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.sites.models import Site
from django.http import HttpResponse
from models import UserWebsite

class DynamicSiteMiddleware(object):
    def process_request(self, request):
        host=request.get_host()
        shost=host.rsplit(':', 1)[0] # only host, without port
    
        site=Site.objects.all()

        if len(site)==0:
            site[0]=Site(domain=shost, name=shost)
            site[0].save()
        
        if site[0].domain=='example.com':
            site[0].domain=shost
            site[0].name=shost
            site[0].save()

        site=site[0]
        if shost==site.domain:
            request.referred_by=None
            request.requested_host=shost
            return None

        subdomain=shost[:-(len(site.domain)+1)]

        try:
            up=UserWebsite.objects.get(alias=subdomain, user__status__type__action__in=['nothing', 'pause'], user__is_active=True)
            
            #if (up.user.is_superuser or up.user.is_staff) and up.user!=request.user:
            #    return HttpResponse(status=404)
            request.referred_by=up.user
            request.requested_host=shost
            return None
        except:
            pass

        return HttpResponse(status=404)
