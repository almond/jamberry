from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db.models import Q 

from crossmarket.apps.compensation.models import UserHistory

import datetime

class Command(BaseCommand):
    help='In order for a consultant\'s account to remain in active status, they must generate 300 points or more in PCV in a rolling three-calendar-month period'
    
    def handle(self, *args, **options):
        

        now=datetime.datetime.now()
        year_months=[
            {'year':now.year if now.month!=1 else now.year-1, 'month':now.month-1 if now.month!=1 else 12},
        ]
        q=Q(year=year_months[0]['year'], month=year_months[0]['month'])
        for counter in range(0, 2):
            year_months.append({'year':year_months[counter]['year'] if year_months[counter]['month']!=1 else year_months[counter]['year']-1, 'month':year_months[counter]['month']-1 if year_months[counter]['month']!=1 else 12})
            q=q | Q(year=year_months[counter+1]['year'], month=year_months[counter+1]['month'])

        active_users=UserHistory.objects.values_list('user__id', flat=True).filter(q, pcv__gte=300).distinct()
        staff=User.objects.values_list('id', flat=True,).filter(Q(is_staff=True)|Q(is_superuser=True) ).distinct()
        deactivate_users=User.objects.filter(~Q(id__in=active_users), ~Q(id__in=staff), is_active=True).count()

        #just write command to deactivate
