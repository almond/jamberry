from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.account.models import Profile, UserWebsite, UserSponsor
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from l10n.models import Country



class Command(BaseCommand):
    help='Import Users from Old System\'s CSV.'

    def handle(self, *args, **options):
        user_check=True
        if user_check:
            user_types={}
            user_type_2_cache=[]

        filename=os.path.abspath('%s/../../resources/jamberry_db_usertypes.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue
            user_types[row[0]]=row

        filename=os.path.abspath('%s/../../resources/jamberry_db_users.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        
        if user_check:
            uc_data={'u_types':{}, 'unique_user_type_count':0}

        for row in data:
            if first:
                first=False
                continue

            try:
                user=User.objects.get(pk=row[0])
            except User.DoesNotExist:
                user=User(id=row[0])


            if user_check:
                if row[2] not in uc_data['u_types']:
                    uc_data['u_types'][row[2]]=0
                uc_data['u_types'][row[2]]=uc_data['u_types'][row[2]]+1

                if row[2]=='2':
                    if row[15].lower() not in user_type_2_cache:
                        user_type_2_cache.append(row[15].lower())
                        uc_data['unique_user_type_count']=uc_data['unique_user_type_count']+1

                continue

            if row[2]!='4':
                continue

            user.username=row[4]
            user.email=row[15]

            name=row[12]
            name=name.split(' ', 1)

            user.first_name=name[0]
            user.last_name="%s %s"%(name[1], row[13]) if 1 in name else row[13]
            dj=row[35].split('/')
            user.date_joined="%s-%s-%s"%(dj[2], dj[1], dj[0])
            ll=row[37].split('/')
            user.last_login="%s-%s-%s"%(ll[2], ll[1], ll[0]) if len(ll)==3 else "%s-%s-%s"%(dj[2], dj[1], dj[0])
            try:
                user.save()
                user.set_password('password')
                user.save()

            except:
                print 'id:', str(row[0])
                continue
            try:
                profile=Profile.objects.get(user=user)
            except Profile.DoesNotExist:
                profile=Profile(user=user)
            profile.phone_number=row[28]
            profile.save()


            try:
                sponsor=UserSponsor.objects.get(user=user)
            except UserSponsor.DoesNotExist:
                sponsor=UserSponsor(user=user)

            if row[6]=='':
                row[6]=1

            try:
                u=User.objects.get(pk=row[6])
            except User.DoesNotExist:
                u=User.objects.get(pk=1)

            sponsor.sponsor=u
            sponsor.save()


            try:
                website=UserWebsite.objects.get(user=user)
            except UserWebsite.DoesNotExist:
                website=UserWebsite(user=user)

            if row[9]=='':
                try:
                    wb=UserWebsite.objects.get(alias__exact=row[4])
                    if wb.user.id!=user.id:
                        row[9]=row[4]+'-'+(str(random.random())[2:5])
                        website.alias=row[9]
                except UserWebsite.DoesNotExist:
                    row[9]=row[4]
                    website.alias=row[9]
            else:
                try:
                    wb=UserWebsite.objects.get(alias__exact=row[9])
                    wb.alias=(wb.user.username)+'-'+(str(random.random())[2:5])
                    wb.save()
                except UserWebsite.DoesNotExist:
                    pass

                website.alias=row[9]

            website.name=row[10]
            website.save()


            """
            try:
                contact=Contact.objects.get(user=user)
            except Contact.DoesNotExist:
                contact=Contact(user=user)
            """

            contact=Contact.objects.filter(user=user)
            if len(contact)>0:
                contact=contact[0]
            else:
                contact=Contact(user=user)
            contact.first_name=user.first_name
            contact.last_name=user.last_name
            contact.role=ContactRole.objects.get(name='Customer')
            contact.email=user.email
            contact.save()

            """
            try:
                ab=AddressBook.objects.get(contact=contact)
            except AddressBook.DoesNotExist:
                ab=AddressBook(contact=contact)
            """
            ab=AddressBook.objects.filter(contact=contact)
            if len(ab)>0:
                ab=ab[0]
            else:
                ab=AddressBook(contact=contact)

            ab.description=''
            ab.addressee=user.get_full_name()
            ab.street1=row[16]
            ab.street2=row[17]
            ab.state=row[19]
            ab.city=row[18]
            ab.postal_code=row[20]
            try:
                country=Country.objects.get(printable_name=row[21])
            except Country.DoesNotExist:
                print contact.id, row[21]
                country=Country.objects.get(printable_name='United States')
            ab.country=Country.objects.get(printable_name=country)
            ab.is_default_shipping=True
            ab.is_default_billing=True
            try:
                ab.save()
            except:
                ab.postal_code



        if user_check:
            print uc_data
