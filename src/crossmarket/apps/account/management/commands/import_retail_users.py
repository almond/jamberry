from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.account.models import Profile, UserWebsite, UserSponsor
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from l10n.models import Country



class Command(BaseCommand):
    help='Import Users from Old System\'s CSV.'

    def handle(self, *args, **options):
        user_check=False
        user_types={}
        if user_check:
            user_type_2_cache=[]

        filename=os.path.abspath('%s/../../resources/jamberry_db_usertypes.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue
            user_types[row[0]]=row

        filename=os.path.abspath('%s/../../resources/jamberry_db_users.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        
        if user_check:
            uc_data={'u_types':{}, 'unique_user_type_count':0}

        for row in data:
            if first:
                first=False
                continue

            try:
                user=User.objects.get(pk=row[0])
            except User.DoesNotExist:
                user=User(id=row[0])


            if user_check:
                if row[2] not in uc_data['u_types']:
                    uc_data['u_types'][row[2]]=0
                uc_data['u_types'][row[2]]=uc_data['u_types'][row[2]]+1

                if row[2]=='2':
                    if row[15].lower() not in user_type_2_cache:
                        user_type_2_cache.append(row[15].lower())
                        uc_data['unique_user_type_count']=uc_data['unique_user_type_count']+1

                continue

            if row[2]!='2':
                continue


            contact=Contact.objects.filter(email=row[15])
            if len(contact)>0:
                contact=contact[0]
            else:
                contact=Contact(email=row[15])
            name=row[12]
            name=name.split(' ', 1)
            contact.first_name=name[0]
            contact.last_name="%s %s"%(name[1], row[13]) if 1 in name else row[13]
            contact.role=ContactRole.objects.get(name='Customer')
            contact.save()
            

            ab=AddressBook.objects.filter(contact=contact)
            if len(ab)>0:
                ab=ab[0]
            else:
                ab=AddressBook(contact=contact)

            ab.description=''
            ab.addressee=contact.full_name
            ab.street1=row[16]
            ab.street2=row[17]
            ab.state=row[19]
            ab.city=row[18]
            ab.postal_code=row[20]
            try:
                country=Country.objects.get(printable_name=row[21])
            except Country.DoesNotExist:
                print contact.id, row[21]
                country=Country.objects.get(printable_name='United States')
            ab.country=Country.objects.get(printable_name=country)
            ab.is_default_shipping=True
            ab.is_default_billing=True
            try:
                ab.save()
            except:
                ab.postal_code



        if user_check:
            print uc_data
