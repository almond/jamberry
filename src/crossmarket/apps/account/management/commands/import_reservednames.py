from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.account.models import *

class Command(BaseCommand):
    help='Import Reserved Names from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_reserved_username.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue
            

            if len(row)>0:
                try:
                    r=ReservedUsernameAndDomain.objects.get(name=row[0])
                except ReservedUsernameAndDomain.DoesNotExist:
                    r=ReservedUsernameAndDomain(name=row[0])
                
                r.save()
