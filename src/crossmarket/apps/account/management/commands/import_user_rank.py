from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random
from django.contrib.auth.models import User
from crossmarket.apps.account.models import UserRank
from crossmarket.apps.compensation.models import Rank

class Command(BaseCommand):
    help='Import Users from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_user_ranks.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                user=User.objects.get(pk=row[0])
            except User.DoesNotExist:
                continue

            try:
                us=UserRank.objects.get(user=user)
            except UserRank.DoesNotExist:
                us=UserRank(user=user)

            try:
                us.rank=Rank.objects.get(name=row[5].strip())
                us.save()
            except Rank.DoesNotExist:
                print 'Error in finding rank: %s'%(row[5])