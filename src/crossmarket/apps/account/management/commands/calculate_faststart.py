from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db.models import Q, Sum 

from apps.compensation.models import UserHistory, FastStart, CommissionableOrder
from apps.account.utils import get_commissionable_order
from apps.events.models import Event

import datetime
from decimal import *

class Command(BaseCommand):
    help='In order for a consultant\'s account to remain in active status, they must generate 300 points or more in PCV in a rolling three-calendar-month period'
    
    def handle(self, *args, **options):
        
        days_before_faststart_starts=10
        fast_start_config={
            1:{'enrolled':30, 'rules':[{'start':1, 'end':30, 'parties':2, 'pcv':400, 'active_consultant':0, 'update_data':True}]},
            2:{'enrolled':60, 'rules':[{'start':31, 'end':60, 'parties':3, 'pcv':600, 'active_consultant':0, 'update_data':True}]},
            3:{'enrolled':90, 'rules':[{'start':61, 'end':90, 'parties':5, 'pcv':1000, 'active_consultant':0, 'update_data':True}, {'start':1, 'end':90, 'parties':0, 'pcv':0, 'active_consultant':2, 'update_data':False}]}
        }
        
        now=datetime.datetime.now()
        #now=datetime.datetime(year=2012, month=5, day=14)
        for fs_level, all_rules in fast_start_config.items():
            enrolled_on=now-datetime.timedelta(days=days_before_faststart_starts+all_rules['enrolled'])
            fstarts=FastStart.objects.filter(enroll_date__year=enrolled_on.year, enroll_date__month=enrolled_on.month, enroll_date__day=enrolled_on.day)
            for fs in fstarts:
                meet_criteria=True
                for rule in all_rules['rules']:
                    start=rule['start']
                    end=rule['end']

                    """We are substracting, so start_date should come less than end date"""
                    volumes_check_starts=datetime.datetime(now.year, now.month, now.day, 23, 59, 59)-datetime.timedelta(days=end)
                    volumes_check_ends=datetime.datetime(now.year, now.month, now.day, 0, 0, 0)-datetime.timedelta(days=start)
                    
                    sales=get_commissionable_order(from_datetime=volumes_check_start, to_datetime=volumes_check_end, user_in=[fs.user], remarks_in=['self', 'web'])

                    
                    if len(sales)>0 and 'discounted_total__sum' in sales[0]:
                        sales=float(sales[0]['discounted_total__sum'])
                    else:
                        sales=float(0.00)

                    pcv=float(0.65)*sales #got the pcv here
                    parties=Event.objects.filter(owner=fs.user, starts_at__gte=volumes_check_start, starts_at__lte=volumes_check_end, status=4).count() #parties done

                    active=0
                    for u in fs.user.frontline:
                        if u.is_active==True:
                            active += 1

                    if pcv<rule['pcv'] or parties<rule['parties'] or active<rule['active_consultant']:
                        meet_criteria=False

                    if rule['update_data']:
                        setattr(fs, 'pv_month_%d'%fs_level, pcv)
                        setattr(fs, 'parties_month_%d'%fs_level, pcv)

                if meet_criteria:
                    setattr(fs, 'achieved_month_%d_date'%fs_level, now)
                setattr(fs, 'achieved_month_%d'%fs_level, meet_criteria)
                setattr(fs, 'ended_month_%d'%fs_level, True)
                fs.save()
