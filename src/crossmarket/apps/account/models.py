from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from satchmo_store.contact.models import Contact
from django.conf import settings
from django.db.models.signals import pre_save

from apps.compensation.models import Rank    
from apps.account.utils import (_get_sponsor, _get_downline, _get_upline,
    _get_five_level_downline, _get_frontline, _get_five_level_upline, _get_user_bsc, _get_user_productc,
    _get_user_network, _get_raw_frontline, _get_title, _get_join_date, _get_user_faststart,
    _get_director, _get_prv, _get_twv, _get_gwv, _get_group, _get_team, _get_user_history, _get_user_retail_contacts, _get_user_website,
    _get_active_frontline, _get_new_recruits, _get_user_current_month_volumes, _get_user_current_month_volumes_and_ranks, _get_user_previous_month_volumes_and_ranks,
    _get_first_generation_directors, _get_user_rank, _get_pcv, _get_gcv, _get_downline_rank_and_volume_for_month, _get_profile_phone_number, _get_user_scheduled_parties,
    _get_downline_dict)


GENDER = (
    ('', 'Don\'t Say'),
    ('m', 'Male'),
    ('f', 'Female'),
)

USERTYPE = (
    ('re', 'Retail'),
    ('cs', 'Consultant'),
)

class Type(models.Model):
    slug=models.CharField(unique=True, blank=False, null=False, max_length=32)
    name=models.CharField(max_length=128)
    action=models.CharField(max_length=36, choices=(('pause', 'Pause Commission'), ('disallow_and_compress', 'Pause Commission AND Compress'), ('nothing', 'Nothing')))
    def __unicode__(self):
        return self.name

class UserTypeLog(models.Model):
    user=models.ForeignKey(User)
    from_type=models.ForeignKey(Type, related_name='from_type')
    to_type=models.ForeignKey(Type, related_name='to_type')
    timestamp=models.DateTimeField()

class UserSponsorLog(models.Model):
    user=models.ForeignKey(User)
    from_sponsor=models.ForeignKey(User, related_name='from_sponsor')
    to_sponsor=models.ForeignKey(User, related_name='to_sponsor')
    timestamp=models.DateTimeField()

class UserType(models.Model):
    #from apps.account.models import UserType, Type;
    #from django.contrib.auth.models import User;
    #for u in User.objects.all(): UserType(user=u, type=Type.objects.get(slug='active')).save()
    user=models.OneToOneField(User, related_name='status')
    type=models.ForeignKey(Type)

    def clean(self):
        if self.pk:
            orignal=self.__class__.objects.get(pk=self.pk)
            if orignal.type!=self.type and orignal.type.action=="disallow_and_compress":
                pass #raise ValidationError('You cannot make any further changes to the user type.')

from django.core.exceptions import ValidationError
import datetime
from django.db.models import Max
def change_user_type(sender, instance, using, **kwargs):
    if instance.pk:
        orignal=instance.__class__.objects.get(pk=instance.pk)
        if orignal.type!=instance.type:
            now=datetime.datetime.now()
            UserTypeLog(user=instance.user, from_type=orignal.type, to_type=instance.type, timestamp=now).save()

            if orignal.type.action=='disallow_and_compress':
                uslog=UserSponsorLog.objects.filter(from_sponsor=instance.user).aggregate(max=Max('timestamp'))
                user=instance.user
                user.is_active=True
                user.save()

                if 'max' in uslog:
                    last_changed=uslog['max']
                    old_logs=UserSponsorLog.objects.filter(timestamp=last_changed)
                    for log in old_logs:
                        UserSponsorLog(user=log.user, from_sponsor=log.to_sponsor, to_sponsor=instance.user, timestamp=now).save()
                        UserSponsor.objects.filter(user=log.user).update(sponsor=instance.user)

            #disallow user account
            if instance.type.action=='disallow_and_compress':
                user=instance.user
                user.is_active=False
                user.save()

            if instance.type.action=='compression' or instance.type.action=='disallow_and_compress':
                sponsor=instance.user.sponsor
                if sponsor and sponsor!=instance.user:
                    frontline=UserSponsor.objects.filter(sponsor=instance.user)
                    for f in frontline:
                        UserSponsorLog(user=f.user, from_sponsor=instance.user, to_sponsor=sponsor, timestamp=now).save()
                        f.sponsor=sponsor
                        f.save()

pre_save.connect(change_user_type, sender=UserType)

class ReservedUsernameAndDomain(models.Model):
    name = models.CharField(unique=True, blank=True, null=False, max_length=64)
    def __unicode__(self):
        return self.name


class Network(models.Model):
    slug = models.CharField(unique=True, blank=False, null=False, max_length=32)
    name = models.CharField(max_length=128)
    prepend_text = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

class Propay(models.Model):
    user=models.OneToOneField(User, related_name='propay')
    account=models.CharField('Propay Account', max_length=128, )
    def __unicode__(self):
        return self.user.username

class Profile(models.Model):
    user = models.OneToOneField(User)
    language = models.CharField(max_length=10, choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)
    gender = models.CharField(max_length=1, choices=GENDER, blank=True, null=True)
    personal_photo = models.ImageField(upload_to='profiles/', blank=True, null=True)
    dob = models.DateField(blank=True, null=True, default=None)
    ssn = models.CharField(max_length=9, blank=True, null=True, default=None)
    receive_email = models.BooleanField(default=False)
    phone_number = models.CharField(max_length=40, blank=True, null=True)
    promo_page = models.TextField(blank=True, null=True)

    # MRVG: Adding more properties to the Profile
    # ALTER TABLE `jamberry`.`account_profile` ADD COLUMN `phone_number` VARCHAR(40) NULL
    # ALTER TABLE `jamberry`.`account_profile` ADD COLUMN `promo_page` TEXT NULL

    #ALTER TABLE `account_profile` ADD COLUMN `dob` DATE NULL  , ADD COLUMN `ssn` VARCHAR(128) NULL   ;
    #ALTER TABLE `account_profile` ADD COLUMN `receive_email` TINYINT(1) NULL DEFAULT 0 ;


class UserRank(models.Model):
    user = models.OneToOneField(User)
    rank = models.ForeignKey(Rank, related_name='rank')
    last_updated = models.DateField(auto_now=True)
    def __unicode__(self):
        return 'User %s: %s' % (self.user.username, self.rank)


class UserSponsor(models.Model):
    user = models.OneToOneField(User)
    sponsor = models.ForeignKey(User, related_name='sponsor')


class UserWebsite(models.Model):
    user = models.OneToOneField(User)
    alias = models.CharField(unique=True, blank=False, null=False, max_length=32) #Subdomain of the user
    name = models.CharField("Website", max_length=200) #Title shown on the website

    def domain(self):
        site = Site.objects.get(pk=settings.SITE_ID)
        return '%s.%s' % (self.alias, site.domain)

    def __unicode__(self):
        site = Site.objects.get(pk=1)
        return 'User %s: %s.%s' % (self.user.username, self.alias, site.domain)


class UserNetwork(models.Model):
    user = models.ForeignKey(User)
    network = models.ForeignKey(Network)
    network_address = models.CharField(blank=False, null=False, max_length=128)

    def __unicode__(self):
        return "User %s: %s"%(self.user, self.network)

User.network=property(_get_user_network)
User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
User.sponsor = property(_get_sponsor)
User.website = property(_get_user_website)#property(lambda u: UserWebsite.objects.get_or_create(user=u)[0])
User.contact = property(lambda u: Contact.objects.get_or_create(user=u)[0])
User.downline = property(_get_downline)
User.upline = property(_get_upline)
User.raw_frontline = property(_get_raw_frontline)
User.five_level_upline = property(_get_five_level_upline)
User.five_level_downline = property(_get_five_level_downline)
User.frontline = property(_get_frontline)
User.title = property(_get_title)
User.join_date = property(_get_join_date)
User.director = property(_get_director)
User.prv = property(_get_prv)
User.pcv = property(_get_pcv)
User.gcv = property(_get_gcv)
User.twv = property(_get_twv)
User.gwv = property(_get_gwv)
User.group = property(_get_group)
User.team = property(_get_team)
User.active_frontline = property(_get_active_frontline)
User.new_recruits = property(_get_new_recruits)
User.rank = property(_get_user_rank)
User.first_generation_directors = property(_get_first_generation_directors)
User.current_month_volumes = property(_get_user_current_month_volumes)
User.current_month_details = property(_get_user_current_month_volumes_and_ranks)
User.previous_month_details = property(_get_user_previous_month_volumes_and_ranks)
User.history = property(_get_user_history)
User.retail = property(_get_user_retail_contacts)
User.faststart = property(_get_user_faststart)
User.bsc = property(_get_user_bsc)
User.pc = property(_get_user_productc)
User.phone_number = property(_get_profile_phone_number)
User.scheduled_parties = property(_get_user_scheduled_parties)
User.downline_dict = property(_get_downline_dict)
