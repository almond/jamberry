import datetime
from decimal import *

from django.contrib.auth.models import User
from django.db.models import Q, Sum 
from django.db import models
from django.contrib.sites.models import Site
from django.conf import settings

from satchmo_store.contact.models import Contact
from satchmo_store.contact.models import Contact, AddressBook, ContactRole

from apps.geo.utils import get_zips_in_radius
from apps.events.models import Event
from apps.compensation.models import Rank, RankRequirement, FastStart
from apps.geo.utils import get_zips_in_radius
from apps.compensation.models import Period, CommissionableOrder, UserHistory, Credit
#from apps.account.models import UserSponsor
#from apps.account.models import UserNetwork
from django.db import connection, transaction
from apps.utils import dictfetchall

def _get_user_credit(user, type):
    amount=Decimal(0.00)
    credit=Credit.objects.filter(user=user, credit_type=type)
    for c in credit:
        if c.transaction_type=='a':
            amount=amount+Decimal(c.amount)
        elif c.transaction_type=='s':
            amount=amount-Decimal(c.amount)
    return amount

def _get_user_bsc(user):
    return _get_user_credit(user, 'bsc')

def _get_user_productc(user):
    return _get_user_credit(user, 'pc')

def _get_user_website(user):
    from apps.account.models import UserWebsite
    try:
        return UserWebsite.objects.get(user=user)
    except:
        return None

def _get_user_faststart(user):
    try:
        return FastStart.objects.get(user=user)
    except:
        return None

def _get_user_retail_contacts(user):
    co=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None)
    details=[]
    for c in co:
        details.append({'name':c.order.contact.full_name, 'email':c.order.contact.email, 'date_joined':c.order.time_stamp})
    return details


def _get_user_current_month_volumes_and_ranks(user):
    """Getting details of current month"""
    date=datetime.datetime.now()
    return _get_user_volumes_and_ranks_for_month(user, date.month, date.year)

def _get_user_previous_month_volumes_and_ranks(user):
    """Getting details of current month"""
    date=datetime.datetime.now()

    try:
        return UserHistory.objects.get(user=user, month=((date.month-1) if (date.month!=1) else 12), year=(date.year if (date.month!=1) else date.year-1))
    except:
        return {}

    #return _get_user_volumes_and_ranks_for_month(user, ((date.month-1) if (month!=1) else 12), (date.year if (month!=1) else date.year-1))

def _get_user_overview(user, date, month, year):
    from apps.account.models import UserSponsor
    """Getting details"""
    month_start=datetime.datetime(year, month, 1, 0, 0, 0)
    month_end=datetime.datetime((year if (month<12) else year+1), ((month+1) if (month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)
    
    today_start=datetime.datetime(year, month, date, 0, 0, 0)
    today_end=datetime.datetime(year, month, date, 23, 59, 59)

    week_start=today_start-datetime.timedelta(today_start.weekday())
    week_end=today_end+datetime.timedelta(6-today_start.weekday())

    """Retail Signups: Orders on user's website having no user attached in the system"""
    r_total=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None).count()
    r_this_month=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None, timestamp__gte=month_start, timestamp__lte=month_end).count()
    r_this_week=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None, timestamp__gte=week_start, timestamp__lte=week_end).count()
    r_this_day=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None, timestamp__gte=today_start, timestamp__lte=today_end).count()

    """Consultant Signups"""
    c_total=UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__is_active=True).count()
    c_this_month=UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__is_active=True, user__date_joined__gte=month_start, user__date_joined__lte=month_end).count()
    c_this_week=UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__is_active=True, user__date_joined__gte=week_start, user__date_joined__lte=week_end).count()
    c_this_day=UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__is_active=True, user__date_joined__gte=today_start, user__date_joined__lte=today_end).count()

    
    data={
        'retail':{'total':r_total, 'month':r_this_month, 'week':r_this_week, 'day':r_this_day},
        'consultant':{'total':c_total, 'month':c_this_month, 'week':c_this_week, 'day':c_this_day},
        'total':{'total':c_total+r_total, 'month':c_this_month+r_this_month, 'week':c_this_week+r_this_week, 'day':c_this_day+r_this_day}
        }
    return data
    #data['total']:{'total':data[re], 'month':r_this_month.count(), 'week':r_this_week.count(), 'day':r_this_day.count()}
    
def _get_user_volumes_and_ranks_for_month(user, month, year, get_quarter_data=False, get_level_pcv=False):
    """Getting details of month"""
    start=datetime.datetime(year, month, 1, 0, 0, 0)
    end=datetime.datetime((year if (month<12) else year+1), ((month+1) if (month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)

    data={}
    fl=user.frontline
    fl.append(user)
    volumes=_get_multiple_users_volume_in_timeperiod(fl, start, end, get_quarter_data, True)
    if user.pk not in volumes:
        data['volume']={'prv':Decimal(0), 'pcv':Decimal(0), 'gcv':Decimal(0)}
        data['enrolles']={'active':Decimal(0)}
        data['bsc']=Decimal(0)
        data['payrank']=RankRequirement.objects.get(rank__sequence=2).rank
    else:
        data['volume']=volumes[user.pk]
        data['enrolles']={'active':Decimal(0)}
        
        del volumes[user.pk]
        
        payrule=None
        rules=RankRequirement.objects.filter(rank__sequence__gte=2).order_by('rank__sequence') 
        for rule in rules:
            if data['volume']['gcv']>=rule.min_gcv_for_month and data['volume']['pcv']>=rule.min_pcv_for_month:
                """Passed pcv and gcv rules, now checking active frontline consulants"""
                passed_min_active_rule=False
                passed_min_frstar_rule=False

                
                active=0
                for fluser, com in volumes.items():
                    #print fluser, com['pcv']
                    if com['pcv']>=rule.pcv_to_be_active:
                        active=active+1

                #print rule, rule.pcv_to_be_active, active, rule.min_frontline_active

                data['enrolles']['active']=active
                if active>=rule.min_frontline_active:
                    passed_min_active_rule=True
                
                    
                if passed_min_active_rule:
                    """Passed min. active frontline tool, now min. frontline star or better"""
                    if rule.min_frontline_star_or_above==0:
                        passed_min_frstar_rule=True
                    else:
                        if len(volumes)>=rule.min_frontline_star_or_above:
                            star_consultant_rule=RankRequirement.objects.get(rank__sequence=5)
                            count_star_and_above=0
                            for fluser, com in volumes.items():
                                if com['gcv']>=star_consultant_rule.min_gcv_for_month and com['pcv']>=star_consultant_rule.min_pcv_for_month:
                                    fluser=User.objects.get(pk=fluser)
                                    fl_frontlines=_get_multiple_users_volume_in_timeperiod(fluser.frontline, start, end)
                                    active_frontline_to_be_a_star=0
                                    for fl_frontline in fl_frontlines:
                                        if fl_frontline['pcv']>=star_consultant_rule.pcv_to_be_active:
                                            active_frontline_to_be_a_star=active_frontline_to_be_a_star+1
                                    
                                    if active_frontline_to_be_a_star>=star_consultant_rule.min_frontline_active:
                                        count_star_and_above=count_star_and_above+1
                                    
                            if count_star_and_above>=rule.min_frontline_star_or_above:
                                passed_min_frstar_rule=True

                if passed_min_active_rule and passed_min_frstar_rule:
                    payrule=rule
        
        comm=Decimal(0.30)*Decimal(data['volume']['prv'])
        comp=payrule.rank.commission

        rank_comp={}
        for level, sale in data['volume']['level_pcvs'].items():
            comm=comm+(sale*Decimal(getattr(comp, level))) 

        data['bsc']=Decimal(0) if data['volume']['pcv'] <= 300 else (data['volume']['pcv']*Decimal(payrule.rank.commission.business_supply_credit_for_300_pcv))
        data['payrank']=payrule.rank
        data['comm']=comm
    return data

def _get_user_current_month_volumes(user):
    """Getting details of current month"""
    date=datetime.datetime.now()
    start=datetime.datetime(date.year, date.month, 1, 0, 0, 0)
    end=datetime.datetime((date.year if (date.month<12) else date.year+1), ((date.month+1) if (date.month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)    
    details=_get_multiple_users_volume_in_timeperiod([user, ], start, end)
    return details[user.pk] if user.pk in details else {'prv':Decimal(0), 'pcv':Decimal(0), 'gcv':Decimal(0)}

def get_commissionable_order(**kwrds):
    keywords={
        'from_datetime':{'query':'', 'model_obj':False, 'field':'`compensation_commissionableorder`.`timestamp` >='},
        'to_datetime':{'query':'', 'model_obj':False, 'field':'`compensation_commissionableorder`.`timestamp` <='},
        'user_in':{'query':'', 'model_obj':True, 'field':'`compensation_commissionableorder`.`commission_to_id`'},
        'remarks_in':{'query':'', 'model_obj':False, 'field':'`compensation_commissionableorder`.`remarks`'}
    }
    for key, detail in keywords.items():
        if kwrds.get(key, None):
            data=kwrds[key]
            if isinstance(data, datetime.datetime):
                keywords[key]['query']='AND %s "%s"'%(detail['field'], data.strftime('%Y-%m-%d %H:%M:%S'))
            elif isinstance(data, basestring) or isinstance(data, int):
                keywords[key]['query']='AND %s IN ("%s")'%(detail['field'], str(data))
            elif isinstance(data, list):
                if keywords[key]['model_obj']:
                    in_data='", "'.join(map(str, [getattr(d, 'id') for d in data]))
                else:
                    in_data='", "'.join(map(str, data))

                keywords[key]['query']='AND %s IN ("%s")'%(detail['field'], in_data)
    #Cancelled
    query='SELECT `compensation_commissionableorder`.`commission_to_id` AS `commission_to`, SUM((`shop_order`.`sub_total`-`shop_order`.`discount`)) AS `discounted_total__sum` FROM `compensation_commissionableorder` LEFT OUTER JOIN `shop_order` ON (`compensation_commissionableorder`.`order_id` = `shop_order`.`id`) WHERE (`shop_order`.`status` not IN ("Cancelled") %(to_datetime)s  %(from_datetime)s %(user_in)s %(remarks_in)s) GROUP BY `compensation_commissionableorder`.`commission_to_id`, `compensation_commissionableorder`.`commission_to_id` ORDER BY NULL' % dict(from_datetime=keywords['from_datetime']['query'], to_datetime=keywords['to_datetime']['query'], user_in=keywords['user_in']['query'], remarks_in=keywords['remarks_in']['query'])

    
    #print ""
    #print query
    #print ""
    

    cursor = connection.cursor()
    cursor.execute(query)
    return dictfetchall(cursor)

def _get_multiple_users_volume_in_timeperiod(users_list, start, end, get_quarter_data=False, get_level_pcv=False):
    details={}

    users_personal_sales_details=get_commissionable_order(from_datetime=start, to_datetime=end, user_in=users_list, remarks_in=['self', 'web'])
    users_group_sales_details=get_commissionable_order(from_datetime=start, to_datetime=end, user_in=users_list)


    for d in users_personal_sales_details:
        if d['commission_to'] not in details:
            details[d['commission_to']]={'prv':Decimal(0), 'pcv':Decimal(0), 'gcv':Decimal(0)}
        details[d['commission_to']]['prv']=d['discounted_total__sum']
        details[d['commission_to']]['pcv']=Decimal('0.65')*d['discounted_total__sum']

    for d in users_group_sales_details:
        if d['commission_to'] not in details:
            details[d['commission_to']]={'prv':Decimal(0), 'pcv':Decimal(0), 'gcv':Decimal(0)}
        details[d['commission_to']]['gcv']=Decimal(0.65)*d['discounted_total__sum']

    if get_quarter_data:
        """Finding start of this Quarter"""
        random_day_in_quarter_start_month=end-datetime.timedelta(days=100)
        quarter_start=datetime.datetime(random_day_in_quarter_start_month.year, random_day_in_quarter_start_month.month, 1, 0, 0, 0)
        users_quarterly_personal_sales_details=get_commissionable_order(from_datetime=quarter_start, to_datetime=end, user_in=users_list, remarks_in=['self', 'web'])

        for d in users_quarterly_personal_sales_details:
            if d['commission_to'] not in details:
                details[d['commission_to']]={'prv':Decimal(0), 'pcv':Decimal(0), 'gcv':Decimal(0), 'pcv_quarter':Decimal(0)}
            details[d['commission_to']]['pcv_quarter']=d['discounted_total__sum']
            details[d['commission_to']]['pcv_quarter']=Decimal('0.65')*d['discounted_total__sum']
        for u, data in details.items():
            if 'pcv_quarter' not in data:
                details[u]['pcv_quarter']=Decimal(0)
            
    if get_level_pcv:
        levels=['l1', 'l2', 'l3', 'l4', 'l5']
        for u, data in details.items():
            if 'level_pcvs' not in data:
                details[u]['level_pcvs']={'l1':Decimal(0), 'l2':Decimal(0), 'l3':Decimal(0), 'l4':Decimal(0), 'l5':Decimal(0)}
        for level in levels:
            user_level_data=get_commissionable_order(from_datetime=start, to_datetime=end, user_in=users_list, remarks_in=level)
            for d in user_level_data:
                details[d['commission_to']]['level_pcvs'][level]=Decimal('0.65')*d['discounted_total__sum']

    return details


def _get_downline_rank_and_volume_for_month(user, month, year):
    #p=Period.objects.get(month=month, year=year)
    return UserHistory.objects.filter(month=month, year=year, user__in=user.frontline)

def _get_user_rank(user):
    from apps.account.models import UserRank
    try:
        return UserRank.objects.get(user=user)
    except UserRank.DoesNotExists:
        return None

def _get_user_network(user):
    from apps.account.models import UserNetwork
    networks={}
    un=UserNetwork.objects.filter(user=user)
    
    for n in un:
        if n.network.slug not in networks:
            networks[n.network.slug]=[]
        networks[n.network.slug].append(n)
    return networks
    

def _get_user_history(user):
    #now=datetime.datetime.now()
    #return UserHistory.objects.filter(user=user).exclude(month=now.month, year=now.year).order_by('-year', '-month')
    return UserHistory.objects.filter(user=user).order_by('-year', '-month')

def find_user_by_string(data, rank_and_above=False):
    data=data.split(' ', 1)
    if len(data)==1:
        criteria=Q(first_name__icontains=data[0]) | Q(last_name__icontains=data[0]) | Q(email__icontains=data[0])
    else:
        criteria=Q(first_name__icontains=data[0]) & Q(last_name__icontains=data[1])
        
    """
    criteria=Q(first_name__icontains=data) | Q(last_name__icontains=data) | Q(email__icontains=data)
    data=data.split(' ')
    if len(data)>1:
        for d in data:
            if d!='':
                criteria=criteria | Q(first_name__icontains=d) | Q(last_name__icontains=d) | Q(email__icontains=d)
    """
    users=User.objects.filter(criteria)

    if rank_and_above:
        from apps.account.models import UserRank
        user_ids=UserRank.objects.values_list('user__id', flat=True).filter(rank__sequence__gte=rank_and_above)
        users=users.filter(id__in=user_ids)

    return users

def find_user_by_zip(data, rank_and_above=False):
    user_ids_list=False
    if rank_and_above:
        from apps.account.models import UserRank
        user_ids_list=UserRank.objects.values_list('user__id', flat=True).filter(rank__sequence__gte=rank_and_above)
        

    """
    for mile in range(1, 200):
        zip_list=get_zips_in_radius(data, mile)
        if user_ids_list:
            user_ids=AddressBook.objects.values_list('contact__user__id', flat=True).filter(postal_code__in=zip_list, contact__user__id__in=user_ids_list)
        else:
            user_ids=AddressBook.objects.values_list('contact__user__id', flat=True).filter(postal_code__in=zip_list)
        if len(user_ids)>10:
            break
    """
    #directly search in 200 miles.
    zip_list=get_zips_in_radius(data, 200)
    if user_ids_list:
        user_ids=AddressBook.objects.values_list('contact__user__id', flat=True).filter(postal_code__in=zip_list, contact__user__id__in=user_ids_list)
    else:
        user_ids=AddressBook.objects.values_list('contact__user__id', flat=True).filter(postal_code__in=zip_list)

    return User.objects.filter(id__in=list(user_ids))



def _get_sponsor(user):
    from apps.account.models import UserSponsor
    try:
        tmp=UserSponsor.objects.get(~Q(sponsor=user), user=user)
        return tmp.sponsor
    except UserSponsor.DoesNotExist:
        return None

def _get_five_level_upline(user):
    from apps.account.models import UserSponsor
    up_for=user
    level={}
    for count in range(1, 6):
        if up_for is None:
            level[count]=None
            continue

        tmp=UserSponsor.objects.filter(~Q(sponsor=up_for), user=up_for)
        if len(tmp)==1:
            u=tmp[0].sponsor
        else:
            u=None
        level[count]=u
        up_for=u
    return level


def _get_upline(user):
    from apps.account.models import UserSponsor
    ul=[]
    upline_list=UserSponsor.objects.filter(~Q(sponsor=user), user=user)
    while len(upline_list)==1:
        ul.append(upline_list[0].sponsor)
        upline_list=UserSponsor.objects.filter(~Q(sponsor=upline_list[0].sponsor), user=upline_list[0].sponsor)
    return ul

def _get_frontline(user):
    from apps.account.models import UserSponsor
    users=[]
    temp=UserSponsor.objects.filter(~Q(user=user), sponsor=user)
    for u in temp:
        users.append(u.user)
    return users

def _get_raw_frontline(user):
    from apps.account.models import UserSponsor
    return UserSponsor.objects.filter(~Q(user=user), sponsor=user)

def _get_five_level_downline(user):
    from apps.account.models import UserSponsor
    sponsor_in=[user.id, ]
    users_in_level={}
    for count in range(1, 6):
        users_in_level[count]=[]
        tmp=UserSponsor.objects.filter(sponsor__id__in=sponsor_in)
        sponsor_in=[]
        for u in tmp:
            users_in_level[count].append(u.user)
            sponsor_in.append(u.user.id)
    return users_in_level

def _get_downline(user, dl=[], only_id=False):
    from apps.account.models import UserSponsor
    downline=UserSponsor.objects.filter(sponsor=user)
    for d in downline:
        if d.user not in dl:
            dl.append(d.user)
            dl=_get_downline(d.user, dl)
    return dl

def _get_downline_dict_old(user):
    from apps.account.models import UserSponsor
    data=[]
    frontline=UserSponsor.objects.filter(sponsor=user)
    for f in frontline:
        volumes=user.current_month_details
        tmp={"name":f.user.get_full_name(), 'user_id':f.user.id, 'data':{
                'username':f.user.username,
                'email':f.user.email,
                'last_login':str(f.user.last_login),
                'pcv':volumes['volume']['pcv'],
                'commissions':volumes['comm'] if 'comm' in volumes else 0.00,
                'active_frontline':f.user.active_frontline,
                'new_recruits':f.user.new_recruits
                }
            }
        children=_get_downline_dict(f.user)
        if len(children)>0:
            tmp['children']=children
        data.append(tmp)
    return data

def _get_downline_dict(user):
    from apps.account.models import UserSponsor
    data=[]
    frontline=UserSponsor.objects.filter(sponsor=user)
    for f in frontline:
        volumes=user.current_month_details
        tmp={"name":f.user.get_full_name(), 'user_id':f.user.id}
        children=_get_downline_dict(f.user)
        if len(children)>0:
            tmp['children']=children
        data.append(tmp)
    return data

def _get_title(user):
    """
    MRVG: I think I fixed this. It was not returning anything. Hope nothing else breaks.
    """
    return user.rank.rank.name

def _get_join_date(user):
    return user.date_joined

def _get_director(user):
    return 1234

def _get_prv(user):
    return 1234

def _get_gcv(user):
    return 1234

def _get_pcv(user):
    return 1234

def _get_twv(user):
    return 1234

def _get_gwv(user):
    return 1234

def _get_group(user):
    return 1234

def _get_team(user):
    return 1234

def _get_active_frontline(user):
    return len(user.frontline)

def _get_new_recruits(user):
    from apps.account.models import UserSponsor

    now=datetime.datetime.now()
    month_start=datetime.datetime(now.year, now.month, 1, 0, 0, 0)
    month_end=datetime.datetime((now.year if (now.month<12) else now.year+1), ((now.month+1) if (now.month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)
    c_this_month=UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__is_active=True, user__date_joined__gte=month_start, user__date_joined__lte=month_end).count()
    return c_this_month

def _get_first_generation_directors(user):
    return 1234

def _get_personally_enrolled_in_a_month(user, month, year):
    from apps.account.models import UserSponsor
    start=datetime.datetime(year, month, 1, 0, 0, 0)
    end=datetime.datetime((year if (month<12) else year+1), ((month+1) if (month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)

    return UserSponsor.objects.filter(~Q(user=user), sponsor=user, user__date_joined__gte=start, user__date_joined__lte=end).count()

def _get_profile_phone_number(user):
    """
    MRVG: Added this to be able to pull this field into the config.py of the quasi-cms
    """
    return user.profile.phone_number


def _get_user_scheduled_parties(user):
    now=datetime.datetime.now()
    return Event.objects.filter(owner=user, status__in=[2, 3], starts_at__gte=now)
