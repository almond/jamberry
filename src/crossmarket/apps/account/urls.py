from django.conf.urls.defaults import patterns, include, url

from apps.account.views import *


urlpatterns = patterns('',
    (r'^user-logout/$', accounts_logout, {}, "accounts_logout"),
    (r'^find/$', find, {}, "find_a_consultant"),
    (r'^frontline/$', frontline, {}, "frontline"),
    (r'^frontline_dir/$', frontline_dir, {}, "frontline_dir"),
    (r'^propay_test/$', propay_test, {}, "propay_test"),
    (r'^join/$', join),
    url(r'^host/$', host, name="account_host_a_party"),
)

#propay_test
