from apps.account.forms import *
from apps.account.models import *
from apps.account.utils import *
from apps.geo.utils import get_zips_in_radius
from apps.events.models import EventPhoto
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from satchmo_store.shop.models import Cart, Product
from apps.api_wrapper.wrapper import ProPayApi



def accounts_logout(request):
    logout(request)
    return HttpResponseRedirect('http://%s'%(settings.BASE_URL))

def frontline(request):
    if request.user.is_authenticated():
        downline_for=request.GET.get('for', None)
        if downline_for:
            try:
                downline_for=User.objects.get(id=downline_for)
            except User.DoesNotExist:
                return HttpResponse(simplejson.dumps({'status':{'success':False, 'reason':'Requested user doesnot exists.'}}))
                
            upline=downline_for.upline
            if request.user not in upline:
                return HttpResponse(simplejson.dumps({'status':{'success':False, 'reason':'You cannot see the frontline of this user.'}}))
                
        else:
            downline_for=request.user
        
        fl_data=[]
        frontline=downline_for.frontline
        for u in frontline:
            name=u.get_full_name()
            fl_data.append({'id':u.id, 'name':u.get_full_name(), 'child_count':u.raw_frontline.count()})

        return HttpResponse(simplejson.dumps({'status':{'success':True}, 'data':{'frontline':fl_data}}))
    else:
        return HttpResponse(simplejson.dumps({'status':{'success':False, 'reason':'Login Required.'}}))

def frontline_dir(request):
    if request.user.is_authenticated():
        downline_for=request.GET.get('for', None)
        if downline_for:
            try:
                downline_for=User.objects.get(id=downline_for)
            except User.DoesNotExist:
                return HttpResponse('[]')
                
            upline=downline_for.upline
            if request.user not in upline:
                return HttpResponse('[]')
                
        else:
            downline_for=request.user
        
        fl_data=[]
        frontline=downline_for.frontline
        for u in frontline:
            name=u.get_full_name()
            if name.strip()=='':
                name=u.username

            len_frontline=u.raw_frontline.count()
            if len_frontline:
                fl_data.append({'key':u.id, 'title':name, 'isFolder':True, 'isLazy':True})
            else:
                fl_data.append({'title':name})

        return HttpResponse(simplejson.dumps(fl_data))
    else:
        return HttpResponse('[]')

def find(request):
    accounts=None
    if request.method=='POST':
        if 'name_email' in request.POST:
            accounts=find_user_by_string(request.POST.get('name_email'))
        elif 'zip' in request.POST:
            accounts=find_user_by_zip(request.POST.get('zip'), 5)
            
    return render_to_response('account/find.html', {'action':'shop', 'accounts':accounts}, context_instance=RequestContext(request))

def host(request):
    accounts=None
    if request.referred_by:
        if request.method=='POST':
            host_form=HostForm(request.POST, prefix='host')
            party_form=PartyForm(request.POST, prefix='party')
            
            if host_form.is_valid() and party_form.is_valid():
                party=party_form.save(commit=False)
                party.owner=request.referred_by
                party.photo=EventPhoto.objects.all()[0]
                party.save()
                host=host_form.save(commit=False)
                host.has_accepted=True
                host.event=party
                host.save()
                party.host=host
                party.save()
                
                send_mail('Jamberry Party Request.', 'Please check your email, there is a party request.', settings.DEFAULT_FROM_EMAIL, [request.referred_by.email])
                
                return HttpResponseRedirect( ("%s?done=true"%(reverse('account_host_a_party'))) )
        else:
            for_whome=request.GET.get('for', None)
            from apps.events.models import Attendee
            try:
                a=Attendee.objects.get(pk=for_whome, event__owner=request.referred_by)
                initial={'first_name':a.first_name, 'last_name':a.last_name, 'street':a.street, 'street2':a.street2, 'city':a.city, 'state':a.state, 'postal_code':a.postal_code, 'phone':a.phone, 'email':a.email}
            except:
                initial={}

            host_form=HostForm(prefix='host', initial=initial)
            party_form=PartyForm(prefix='party')
        return render_to_response('account/host.html', {'host_form':host_form, 'party_form':party_form}, context_instance=RequestContext(request))

    if request.method=='POST':
        if 'name_email' in request.POST:
            accounts=find_user_by_string(request.POST.get('name_email'))
        elif 'zip' in request.POST:
            accounts=find_user_by_zip(request.POST.get('zip'), 5)
            
    return render_to_response('account/find.html', {'action':'host', 'accounts':accounts}, context_instance=RequestContext(request))


def propay_test(request):
    from django.template import Context, loader
    t = loader.get_template('propay/signup.html')
    c = Context({
            'certStr': settings.API_PROPAY['signup_credentials'],
            'sourceEmail':'someemail',
            'firstName':'fn',
            'lastName':'ln',
            'addr':'addr',
            'city':'',
            'state':'',
            'zip':'',
            'dayPhone':'',
            'evenPhone':'',
            'ssn':'',
            'dob':'', #12-15-1961 format
            'tier':'Premium',
            'ccNum':'',
            'expDate':'',
            'phonePin':'1234'
            })

    pp = ProPayApi()
    pp.signup(t.render(c))

    return HttpResponse(t.render(c))




def join(request):
    accounts=None
    if request.referred_by:
        if request.method=='POST':
            form=RegistrationForm(request.POST, request=request)
            if form.is_valid():
                request.session['registration-form']=True
                request.session['registration-form-data']=form.cleaned_data
                #form.save()
                cart=Cart.objects.from_request(request, create=True)
                for item in cart.cartitem_set.all():
                    item.delete()
                product=Product.objects.get(slug='signup')
                added_item=cart.add_item(chosen_item=product, number_added=1, details=[])
                #return HttpResponse(reverse('satchmo_checkout-step1'))
                #return HttpResponseRedirect(reverse('satchmo_checkout-step1'))
                return HttpResponseRedirect(reverse(settings.CHECKOUT_PAGE_NAME))
        else:
            form=RegistrationForm(initial={'referrer': request.referred_by.username})
        website=request.POST.get('website', False)
        return render_to_response('account/join.html', {'action':'join', 'accounts':accounts, 'form':form, 'website':website}, context_instance=RequestContext(request))

    if request.method=='POST':
        if 'name_email' in request.POST:
            accounts=find_user_by_string(request.POST.get('name_email'))
        elif 'zip' in request.POST:
            accounts=find_user_by_zip(request.POST.get('zip'), 5)
    return render_to_response('account/find.html', {'action':'join', 'accounts':accounts, 'name_email':request.POST.get('name_email', False), 'zip':request.POST.get('zip', False)}, context_instance=RequestContext(request))
