from django import forms
from models import *
import datetime
#from registration import signals
from registration.forms import RegistrationForm
from registration.models import RegistrationProfile, RegistrationManager

from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site

from satchmo_store.contact.models import Contact, AddressBook, ContactRole, PhoneNumber
from l10n.models import Country
from apps.account.models import UserType, Type, Propay
from apps.events.models import Event, Host
from apps.compensation.models import FastStart

import chimpy

class HostForm(forms.ModelForm):
    class Meta:
        model=Host
        exclude=('event', 'user', 'has_accepted')

class PartyForm(forms.ModelForm):
    class Meta:
        model=Event
        exclude=('owner', 'user', 'has_accepted', 'use_personal_website', 'use_evite', 'is_closed', 'closed_at', 'created_at', 'status', 'photo', 'redeemed_hostess_reward')

    def clean_starts_at(self):
        starts_at = self.cleaned_data['starts_at']
        now = datetime.datetime.now()
        if now > starts_at:
            raise forms.ValidationError("Please select a date.")
        return starts_at


class RegistrationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(RegistrationForm, self).__init__(*args, **kwargs)

    first_name=forms.CharField(max_length=80, label='First Name', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    last_name=forms.CharField(max_length=80, label='Last Name', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    company_name=forms.CharField(max_length=250, label='Company Name', widget=forms.TextInput(attrs={'class':''}), required=False)
    phone=forms.IntegerField(widget=forms.TextInput(attrs={'class':'validate[custom[phone]]'}))
    email=forms.EmailField(max_length=80, label='Email', widget=forms.TextInput(attrs={'class':'validate[required,custom[email]]'}))
    receive_email=forms.BooleanField(label="I wish to receive emails from Jamberry Nails (you'll receive a confirmation email).", required=False)

    referrer=forms.CharField(max_length=80, label='Referrer', widget=forms.TextInput(attrs={'class':'', 'disabled':'disabled'}), required=False)

    ssn_or_taxid=forms.CharField(max_length=9, min_length=9, label='SSN or Tax ID', widget=forms.TextInput(attrs={'class':''}), required=True)
    dob_day=forms.ChoiceField(choices=[(x, x) for x in range(1, 32)])
    dob_month=forms.ChoiceField(choices=[(1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')])
    dob_year=forms.ChoiceField(choices=[(x, x) for x in range(1920, 2013)])

    username=forms.CharField(max_length=20, label='Username', widget=forms.TextInput(attrs={'class':'validate[required,custom[onlyLetterNumber],length[0,20]]'}))
    password=forms.CharField(max_length=24, min_length=6, label='Password', widget=forms.PasswordInput(attrs={'class':'validate[required,length[6,24]]'}))
    c_password=forms.CharField(max_length=24, min_length=6, label='Confirm Password', widget=forms.PasswordInput(attrs={'class':'validate[required,confirm[id_password]]'}))

    website=forms.CharField(max_length=24, label='Website Alias', widget=forms.TextInput(attrs={'class':'validate[required,custom[onlyLetterNumber],length[0,24]]'}))
    website_name=forms.CharField(max_length=32, label='Website Name', widget=forms.TextInput(attrs={'class':'validate[required,length[0,32]]'}))

    has_propay=forms.BooleanField(required=False, label="I have a ProPay a/c", widget=forms.CheckboxInput(attrs={'style':"margin-top: 12px;"}))
    propay_account=forms.CharField(required=False)

    reviewed=forms.BooleanField(required=True, widget=forms.CheckboxInput(attrs={'class':"validate[required]"}))


    def clean_has_propay(self):
        if self.cleaned_data['has_propay'] and ( ('propay_account' not in self.cleaned_data) or (self.cleaned_data['propay_account']=='') ):
            raise forms.ValidationError("Please enter your ProPay a/c details if you have a propay account.")
        return self.cleaned_data['has_propay']

    def clean_referrer(self):
        self.cleaned_data['referrer']=self.request.referred_by.username
        return self.request.referred_by.username

    def clean_c_password(self):
        if 'password' in self.cleaned_data:
            if self.cleaned_data['c_password']!=self.cleaned_data['password']:
                raise forms.ValidationError("Passwords do not match.")
            else:
                return self.cleaned_data['c_password']

        self.cleaned_data['referrer']=self.request.referred_by.username
        return self.request.referred_by.username

    def clean_username(self):
        data = self.cleaned_data['username']

        try:
            ReservedUsernameAndDomain.objects.get(name=data)
            raise forms.ValidationError("This username is not available.")
        except ReservedUsernameAndDomain.DoesNotExist:
            pass

        try:
            User.objects.get(username=data)
            raise forms.ValidationError("This username is already taken.")
        except User.DoesNotExist:
            return data

    def clean_email(self):
        try:
            data = self.cleaned_data['email']
            User.objects.get(email=data)
            raise forms.ValidationError("This email is already taken.")
        except User.DoesNotExist:
            return data

    def clean_website(self):
        data = self.cleaned_data['website']
            
        try:
            ReservedUsernameAndDomain.objects.get(name=data)
            raise forms.ValidationError("This website is not available.")
        except ReservedUsernameAndDomain.DoesNotExist:
            pass


        try:
            UserWebsite.objects.get(alias=data)
            raise forms.ValidationError("This website is already taken.")
        except UserWebsite.DoesNotExist:
            return data

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()

        day=self.cleaned_data['dob_day']
        month=self.cleaned_data['dob_month']
        year=self.cleaned_data['dob_year']
        bd=datetime.datetime(int(year), int(month), int(day))
        min_age=datetime.timedelta(weeks=52*18)
        if datetime.datetime.now()-bd < min_age:
            self._errors["dob_year"] = self.error_class(['You Must be 18 or older. ', ])
            del cleaned_data["dob_year"]
        return cleaned_data

    def save(self):
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        username, email, password = self.cleaned_data['username'], self.cleaned_data['email'], self.cleaned_data['password']
        user=RegistrationProfile.objects.create_inactive_user(username=self.cleaned_data['username'], password=self.cleaned_data['password'], email=self.cleaned_data['email'], site=site)
        
        user.first_name=self.cleaned_data['first_name']
        user.last_name=self.cleaned_data['last_name']
        user.save()

        FastStart(user=user, enroll_date=now).save()

        p=Profile(user=user)
        p.receive_email=1 if ('receive_email' in self.cleaned_data and self.cleaned_data['receive_email']) else 0
        p.ssn=self.cleaned_data['ssn_or_taxid']
        p.dob=datetime.date(year=int(self.cleaned_data['dob_year']), month=int(self.cleaned_data['dob_month']), day=int(self.cleaned_data['dob_day']))
        p.save()

        UserSponsor(user=user, sponsor=self.request.referred_by).save()
        UserWebsite(user=user, alias=self.cleaned_data['website'], name=self.cleaned_data['website_name']).save()

        UserType(user=user, type=Type.objects.get(slug='active')).save()

        contact=Contact(user=user, first_name=self.cleaned_data['first_name'], last_name=self.cleaned_data['last_name'], email=self.cleaned_data['email'], role=ContactRole.objects.get(name='Customer'))
        contact.save()
        
        PhoneNumber(phone=self.cleaned_data['phone'], contact=contact, type='Home', primary=1).save()
        #UserType(user=user, type='cs').save()

        try:
            tmp_chimpy=chimpy.Connection(settings.API_MAILCHIMP['password'], data_center="us2")
            tmp_chimpy.list_subscribe(settings.API_MAILCHIMP['consultant_list_id'], user.email, {'FIRST': user.first_name, 'LAST': user.last_name})
        except:
            pass




        if self.cleaned_data['has_propay']:
            Propay(user=user, account=self.cleaned_data['propay_account']).save()

        return user
