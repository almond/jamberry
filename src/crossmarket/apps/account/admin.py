from django.contrib import admin
from django import forms
from django.contrib.auth.models import User
from satchmo_store.contact.models import Contact, PhoneNumber, AddressBook
from satchmo_utils.admin import AutocompleteAdmin

from apps.account.models import (Profile, UserWebsite, UserSponsor,
    ReservedUsernameAndDomain, UserNetwork, Network, UserRank, Type, UserType)


class ContactAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContactAdminForm, self).__init__(*args, **kwargs)
        self.fields['user'].label = 'Username'


class ContactInline(admin.TabularInline):
    model = Contact
    extra = 1


class PhoneNumberInline(admin.TabularInline):
    model = PhoneNumber
    extra = 1


class AddressBookInline(admin.TabularInline):
    model = AddressBook
    extra = 1


class ProfileAdmin(admin.ModelAdmin):
    list_display=('user', )
    raw_id_fields=['user']


class UserSponsorAdmin(admin.ModelAdmin):
    list_display=('user', 'sponsor')
    raw_id_fields=['user', 'sponsor']


class UserWebsiteAdmin(admin.ModelAdmin):
    list_display=('user', 'domain', 'name')
    raw_id_fields=['user']


class TypeAdmin(admin.ModelAdmin):
    list_display=('slug', 'name', 'action')

class UserTypeAdmin(admin.ModelAdmin):
    list_display=('user', 'type')
    raw_id_fields=['user']

class UserNetworkAdmin(admin.ModelAdmin):
    list_display=('user', 'network', 'network_address')
    raw_id_fields=['user']


class ContactAdmin(AutocompleteAdmin):
    form = ContactAdminForm
    def sponsor(self):
        return self.user.usersponsor.sponsor.username
        
    def username(self):
        return self.user

    list_display = (username, 'last_name', 'first_name',
                    'organization', sponsor, 'role')
    list_filter = ['create_date', 'role', 'organization']
    ordering = ['last_name']    
    fieldsets = (
        ("Contact Info", {
            'fields': ('title', 'first_name', 'last_name', 'role',
                       'user', 'organization', 'dob', 'email',
                       'notes', 'create_date')
        }),
    )
    search_fields = ('first_name', 'last_name', 'email')
    related_search_fields = {'user': ('username', 'first_name', 'last_name', 'email')}
    related_string_functions = {'user': lambda u: u"%s (%s)" % (u.username, u.get_full_name())}
    inlines = [PhoneNumberInline, AddressBookInline]

class UserRankAdmin(admin.ModelAdmin):
    list_display=('user', 'rank')
    raw_id_fields=['user']


admin.site.unregister(Contact)
admin.site.register(Contact, ContactAdmin)

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(UserType, UserTypeAdmin)
admin.site.register(UserSponsor, UserSponsorAdmin)
admin.site.register(UserWebsite, UserWebsiteAdmin)
admin.site.register(UserNetwork, UserNetworkAdmin)
admin.site.register(UserRank, UserRankAdmin)
admin.site.register(Network)
admin.site.register(ReservedUsernameAndDomain)


from models import UserTypeLog, UserSponsorLog
class UserTypeLogAdmin(admin.ModelAdmin):
    list_display=('user', 'from_type', 'to_type', 'timestamp')
    raw_id_fields=['user']

class UserSponsorLogAdmin(admin.ModelAdmin):
    list_display=('user', 'from_sponsor', 'to_sponsor', 'timestamp')
    raw_id_fields=['user', 'from_sponsor', 'to_sponsor']

admin.site.register(UserTypeLog, UserTypeLogAdmin)
admin.site.register(UserSponsorLog, UserSponsorLogAdmin)
