from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from apps.api_wrapper.wrapper import AvaTaxApi, IntegraCoreApi
from apps.geo.models import ZipCode
from apps.events.models import Attendee, Event
from django.conf import settings
from django.utils import simplejson
from satchmo_store.shop.models import Order


@login_required
def avatax_geographical_tax(request):
    avatax = settings.API_AVATAX
    api = AvaTaxApi(avatax["account_number"], avatax["licence_key"], version=avatax["version"])
    message = {}

    # Get the geographical thing going on 
    if request.is_ajax():
        #attendee_id = request.GET.get("attendee_id", None)
        price = request.GET.get("total", 10)
        party_id = request.GET.get("party_id", None)

        try:
            party = Event.objects.get(pk=party_id)
            geo_coordinates = ZipCode.objects.get(zip_code=party.postal_code)
            latlng = (geo_coordinates.latitude, geo_coordinates.longitude)
            # Get the avalara response thing
            header, response = api.geographical_tax(price, str(latlng[0]), str(latlng[1]))
            message = {
                "status": True,
                "response": response,
            }
        except (Attendee.DoesNotExist, ZipCode.DoesNotExist):
            message = {
                "status": False,
                "response": "Please enter an attendant ID",
            }

        return HttpResponse(simplejson.dumps(message, ensure_ascii=False), mimetype="application/json")
    else:
        return HttpResponse("This URL is only to be called via AJAX")


def avatax_get_tax(request):
    avatax = settings.API_AVATAX
    api = AvaTaxApi(avatax["account_number"], avatax["licence_key"], version=avatax["version"])
    response = api.get_tax()
    return HttpResponse(simplejson.dumps(response, ensure_ascii=False), mimetype="application/json")


def integracore_import_order(request):
    """
    Test the crap out of this.
    """
    integracore = settings.API_INTEGRACORE
    api = IntegraCoreApi(integracore['username'], integracore['password'])
    order = Order.objects.get(pk=10127)
    response, content = api.import_order(order)
    return HttpResponse(content)
