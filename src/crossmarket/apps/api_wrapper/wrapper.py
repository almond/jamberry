'''
Created on May 16, 2012
@author: Mario R. Vallejo

API Wrapper calls to all of the different services that Jamberry has to connect to
'''
import decimal
import urllib

import httplib2
from django.conf import settings
from django.utils import simplejson


# TODO: Extract some functionality into a master class that both these API's extend. 

class AvaTaxApi:
    # A dictionary because calls can be made to more than one url in case one goes down. I just don't know
    # them yet. Perhaps they don't even exist and I misread the docs.
    # When substituting an identifier add at the end of the string a "/" character. Being an optional
    # parameter it is defined as not having one so not to disturb url integrity
    resources = {            
        "avatax_primary": "https://rest.avalara.net/{version}/{resource}/{identifier}{operation}.{format}",
    }

    def __init__(self, *args, **kwargs):
        """
        Get the auth headers going on before the call to the
        desired webservice
        
        Takes 2 positional parameters
        - username (account number)
        - password (licence key)
        """
        if "version" in kwargs:
            self.version = kwargs["version"]
        else:
            self.version = "1.0"

        self.username = args[0]
        self.password = args[1]


    def geographical_tax(self, *args, **kwargs):
        """
        Get the tax of a set amount depending on the geographical preceding of the 
        transaction

        Takes 3 positional arguments:
        - The amount to calculate tax from
        - Latitude 
        - Longitude

        Return variable is meant to be unpacked in a tuple. Returns both the response headers
        and the content of the response
        """

        if settings.DEBUG and hasattr(settings, 'SYS_OWNER') and settings.SYS_OWNER=="Aman":
            return "header" , {"Tax":0.00, "Rate":0.00}

        http_conn = httplib2.Http(timeout=5)
        http_conn.add_credentials(self.username, self.password)

        amount = args[0] # Not necessary but just to make it clearer to someone reading
        latlng = (args[1], args[2])
        version = self.version

        url = self.resources["avatax_primary"].format(version=version, 
                resource="tax", identifier=",".join(latlng) + "/", operation="get", format="json")

        # Setup the HTML object with all of the data needed and then make the call.
        params = urllib.urlencode({"saleamount": amount})
        url = "?".join((url, params))

        # Make the GET request to the server
        try:
            response, content = http_conn.request(url, "GET",
                    headers={"Content-type": "text/json"})
        except (IOError, httplib2.ServerNotFoundError):
            response = ""
            content = {"Tax": 0.09, "Rate": 0.09}

        # Turn the content into a nice Python dict
        content = simplejson.loads(content)

        # Now format the response to return a python dictionary
        return (response, content)


    def get_tax(self, *args, **kwargs):
        """ 
        The same as above but for batch operations 

        Takes 3 positional arguments:
        - Order django object
        - Latitude
        - Longitude

        I'll get to this later. People want 'fast' rather than 'good' right now
        """
        pass


    def validate_address(self, *args, **kwargs):
        """ 
        This operation returns a canonical representation of a validated address... 
        I have no idea what that means
        """
        pass


class ProPayApi:
    """
    Call methods from ProPay system
    """
    resources = {
        "all":"http://protectpaytest.propay.com/API/SPS.svc?wsdl",
        "svc":"https://protectpaytest.propay.com/api/sps.svc "
    }

    def __init__(self, *args, **kwargs):
        """
        TODO: Aman you should check this out and move it 
        to each method according to what you need from your API. It keeps a connection open
        for x amount of time (Until garbage collected) which means that sometimes the object
        will not be available for subsequent calls because the destination server closed the connection.
        Learnt this the hard way.
        """
        self.http_conn = httplib2.Http()
        
    def signup(self, doc):
        self.http_conn.request("http://amanjain.com")
        response, content = self.http_conn.request(self.resources["svc"], "POST", body=doc)
        print response

class IntegraCoreApi:
    """
    Call several method of the IntegraCore system. This will be the wrapper necessary for the 
    fullfillment API. They don't offer a formal REST API so I had to make my own on PHP and calling
    it from here. It's not that hard.
    """
    # This is declared as a list because there are more than one URL's to call
    resources = {
        "inventory": "http://www.integracoreb2b.com/IntCore/Inventory.asmx?wsdl",
        "order_entry": "http://www.integracoreb2b.com/intCore/IncomingOrders.asmx?wsdl",
        "return_status":  "http://www.integracoreb2b.com/api/returns.asmx?wsdl",
        "edit_item_info": "http://www.integracoreb2b.com/API/ItemMaster.asmx?wsdl",
        "expected_receipts":  "http://www.integracoreb2b.com/API/ExpectedReceipts.asmx?wsdl",
        "confirmation": "http://www.integracoreb2b.com/API/ShipConfirm.asmx?wsdl",
    }
    
    # This is for localtest purposes
    resources_testing = {
        "order_entry": "http://localhost:8888/jamberry/integracore/import_order.php",
    }
    
    def __init__(self, *args, **kwargs):
        """
        Get the user credentials from settings.py and instantiate a REST client with it
        takes 2 positional parameters
        - username: (account number)
        - password: (licence key)
        """
        self.http_conn = httplib2.Http()
        self.username = args[0]
        self.password = args[1]
        
        
    def import_order(self, *args, **kwargs):
        """
        Gets a passed Order object. Converts to a flat dictionary and
        sends it as a json post value to the PHP REST API. Foken hate satchmo and soap.
        
        takes 1 positional parameter
        - order: The Order object that's going to be sent
        """
        order = args[0]
        
        try:
            phone_number = order.contact.phonenumber_set.all()[0]
        except IndexError:
            phone_number = ""
            
        contact = {
            'first_name': order.contact.first_name,
            'last_name': order.contact.last_name,
            'email': order.contact.email,
            'phone': phone_number,
        }
        
        orderitem_queryset = order.orderitem_set.all()
        orderitem = []
        for item in orderitem_queryset:
            orderitem.append({
                "sku": item.product.sku,
                "quantity": "%d" % item.quantity,
                "price": "%.2f" % item.unit_price
            })
        
        # Serialize all the things
        tmp_order = {}
        for field in order._meta.fields:
            value = getattr(order, field.name)
            if type(value) == decimal.Decimal:
                tmp_order[field.name] = "%.2f" % value
            else:
                tmp_order[field.name] = value
                
        # Now prepare the thing here
        order = tmp_order
        order['time_stamp'] = str(order['time_stamp'])
        del order['site']
        order['password'] = self.password
        order['username'] = self.username
        
        # Had to do this because of reasons
        #order['contact_firstname'] = contact['first_name']
        #order['contact_lastname'] = contact['first_name']kkk
        #order['contact_email'] = contact['email']
        #order['contact_phone'] = contact['phone']
        order["contact"] = contact
        order['order_items'] = orderitem
        #data_to_send = simplejson.dumps(order, ensure_ascii=False)
        # serialize it and send a POST request
        order = simplejson.dumps(order, ensure_ascii=False)

        #headers = {'Content-type': 'application/x-www-form-urlencoded'}
        headers = {'Content-type': 'application/json'}
        response, content = self.http_conn.request(
                uri=self.resources_testing["order_entry"], 
                method="POST", 
                body=order, 
                headers=headers)
        return (response, content)
