from django.conf.urls.defaults import patterns, include, url
from apps.api_wrapper.views import avatax_get_tax, avatax_geographical_tax, integracore_import_order

urlpatterns = patterns('',
 #   url(r'^$', index, name="api_wrapper_index"),
    #url(r'^order_entry/$', order_entry, name="api_wrapper_order_entry"),
    #url(r'^return_status/$', return_status, name="api_wrapper_return_status"),
    #url(r'^edit_item_info/$', edit_item_info, name="api_wrapper_edit_item_info"),
    #url(r'^expected_receipts/$', expected_receipts, name="api_wrapper_expected_receipts"),
    #url(r'^confirmation/$', confirmation, name="api_wrapper_confirmation"), 
    url(r'^get_geographical_tax/$', avatax_geographical_tax, name="apiwrapper_avatax_geographical_tax"),
    url(r'^get_get_tax/$', avatax_get_tax, name="apiwrapper_avatax_get_tax"),
    url(r'^import_order/$', integracore_import_order, name="apiwrapper_integracore_import_order"),
)
