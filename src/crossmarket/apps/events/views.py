from apps.compensation.models import *
from apps.events.forms import EventForm, HostForm, ShipToForm, AttendeeFormSet, \
    AttendeeForm, AttendeeStatusForm
from django.conf import settings, settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, send_mail, send_mail
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.utils import simplejson
import datetime
# from apps.events.models import Event, Attendee

# You've got a nameclash over here.
@login_required
def event_manager(request):
    now = datetime.datetime.today()

    """
    current = Event.objects.filter(starts_at__gte=now, owner=request.user)
    past = Event.objects.filter(starts_at__lt=now, owner=request.user, status=4)
    requested = Event.objects.filter(starts_at__gte=now, attendee__user=request.user, is_closed=False, status=1)
    """
    current = Event.objects.filter(owner=request.user, status=3)
    past = Event.objects.filter(owner=request.user, status=4)
    requested = Event.objects.filter(owner=request.user, starts_at__gte=now, is_closed=False, status__in=[1, 2])


    return render_to_response('events/manager.html', {
        'future_events': current, 'past_events': past, "requested": requested
    }, context_instance=RequestContext(request))



@login_required
def events_tool(request, event_id=None):
    try:
        event=Event.objects.get(pk=event_id, owner=request.user, starts_at__gte=(datetime.datetime.today()))
    except Event.DoesNotExist:
        if event_id:
            raise Http404
        event=None

    if event and request.method=='GET':
        event_form=EventForm(prefix='event', instance=event)
        if event.host:
            host_form=HostForm(prefix='host', instance=event.host)
        else:
            host_form=HostForm(prefix='host')

        if event.shipto:
            ship_form=ShipToForm(prefix='ship', instance=event.shipto)
        else:
            ship_form=ShipToForm(prefix='ship')
    elif event and request.method=='POST':
        event_form=EventForm(request.POST, prefix='event', instance=event)
        if event.host:
            host_form=HostForm(request.POST, prefix='host', instance=event.host)
        else:
            host_form=HostForm(request.POST, prefix='host')

        if event.shipto:
            ship_form=ShipToForm(request.POST, prefix='ship', instance=event.shipto)
        else:
            ship_form=ShipToForm(request.POST, prefix='ship')
    elif not event and request.method=='POST':
        event_form=EventForm(request.POST, prefix='event')
        host_form=HostForm(request.POST, prefix='host')
        ship_form=ShipToForm(request.POST, prefix='ship')        
    else:
        event_form=EventForm(prefix='event')
        host_form=HostForm(prefix='host')
        ship_form=ShipToForm(prefix='ship')

        
    if ( event_form.is_valid() and host_form.is_valid() and ship_form.is_valid()):
        event=event_form.save(commit=False)
        if not event.pk:
            event.owner=request.user
        event.save()

        host=host_form.save(commit=False)
        if not host.pk:
            host.event = event
            if request.POST.get('is_host', False):
                host.has_accepted=True
                host.user=request.user
        host.save()

        shipto=ship_form.save(commit=False)
        if not shipto.pk:
            shipto.event=event
            if request.POST.get('ship_to_consultant', False):
                shipto.has_accepted = True
                shipto.user=request.user
        shipto.save()
        
        return HttpResponseRedirect(reverse('events'))
        #return HttpResponseRedirect(reverse('events_tool_edit', args=[event.pk]))

    shipping_address=request.user.contact.shipping_address
    consultant = {
        'first_name': request.user.first_name,
        'last_name': request.user.last_name,
        'email': request.user.email,
        'phone': request.user.profile.phone_number,
        'street': shipping_address.street1,
        'street2': shipping_address.street2,
        'city': shipping_address.city,
        'state': shipping_address.state,
        'postal_code': shipping_address.postal_code,
    }


    return render_to_response('events/events_tool.html', {
        'event_form': event_form,
        'host_form': host_form,
        'ship_form': ship_form,
        'consultant': consultant,
    }, context_instance=RequestContext(request))

    

@login_required
def event_step1(request, event_id=None):
    try:
        event = Event.objects.get(pk=event_id, owner=request.user)
    except Event.DoesNotExist:
        event = None

        if event_id:
            raise Http404

    if request.method == "POST":
        if event:
            form = EventForm(request.POST, instance=event)
        else:
            form = EventForm(request.POST)

        if form.is_valid():
            event = form.save(commit=False)
            # ok this is lame but it will have to do!
            event.owner = request.user
            event.save()
            if 'ajax' in request.POST:
                return HttpResponse('{"status":{"success":true}}')
            else:
                return HttpResponseRedirect(reverse('events_creator2', args=[event.id]))
        else:
            if 'ajax' in request.POST:
                return HttpResponse(simplejson.dumps({'status':{'success':False, 'errors':form.errors}}))
            
    else:
        if event:
            form = EventForm(instance=event)
        else:
            form = EventForm()

    return render_to_response('events/event_step1.html', {
        'form': form
    }, context_instance=RequestContext(request))

@login_required
def event_step2(request, event_id):
    try:
        event = Event.objects.get(pk=event_id, owner=request.user)
    except Event.DoesNotExist:
        raise Http404

    if request.method == "POST":
        try:
            host = event.host
        except:
            host = None

        if host:
            form = HostForm(request.POST, instance=host)
        else:
            form = HostForm(request.POST)

        if form.is_valid():
            host = form.save(commit=False)
            # ok this is lame but it will have to do!
            host.event = event
            if request.POST.get('is_host', False):
                host.has_accepted = True
                host.user = request.user
            host.save()
            return HttpResponseRedirect(reverse('events_creator3', args=[event.id]))
    else:
        try:
            host = event.host
        except:
            host = None

        if host:
            form = HostForm(instance=host)
        else:
            form = HostForm()

    shipping_address=request.user.contact.shipping_address
    consultant = {
        'name': request.user.get_full_name(),
        'email': request.user.email,
        'phone': request.user.profile.phone_number,
        'street': shipping_address.street1,
        'street2': shipping_address.street2,
        'city': shipping_address.city,
        'state': shipping_address.state,
        'postal_code': shipping_address.postal_code,
    }
    return render_to_response('events/event_step2.html', {
        'form': form, 'consultant': consultant
    }, context_instance=RequestContext(request))

@login_required
def event_step3(request, event_id):
    try:
        event = Event.objects.get(pk=event_id, owner=request.user)
    except Event.DoesNotExist:
        raise Http404

    if request.method == "POST":
        try:
            shipto = event.shipto
        except:
            shipto = None

        if shipto:
            form = ShipToForm(request.POST, instance=shipto)
        else:
            form = ShipToForm(request.POST)
        if form.is_valid():
            shipto = form.save(commit=False)
            # ok this is lame but it will have to do!
            shipto.event = event
            if request.POST.get('ship_to_consultant', False):
                shipto.has_accepted = True
                shipto.user = request.user
            shipto.save()

            hostessportal_url="http://%s%s"%(request.user.website.domain(), reverse('event_hostessportal', args=[event.code]))
            send_mail('Your hostess portal.', hostessportal_url, settings.DEFAULT_FROM_EMAIL, [event.host.email])
            return HttpResponseRedirect(reverse('events'))
        
    else:
        try:
            shipto = event.shipto
        except:
            shipto = None

        if shipto:
            form = ShipToForm(instance=shipto)
        else:
            form = ShipToForm()


    shipping_address=request.user.contact.shipping_address
    consultant = {
        'name': request.user.get_full_name(),
        'email': request.user.email,
        'phone': request.user.profile.phone_number,
        'street': shipping_address.street1,
        'street2': shipping_address.street2,
        'city': shipping_address.city,
        'state': shipping_address.state,
        'postal_code': shipping_address.postal_code,
    }
    return render_to_response('events/event_step3.html', {
        'form': form, 'consultant': consultant
    }, context_instance=RequestContext(request))

# TODO local this down so that only the hostest or superuser can view.
def event_hostessportal(request, code):
    try:
        event = Event.objects.get(code=code)
    except Event.DoesNotExist:
        raise Http404

    attendees = Attendee.objects.filter(event=event)
    
    party_orders=PartyOrder.objects.filter(party=event)
    orders={}
    for po in party_orders:
        for item in po.order.orderitem_set.all():
            if item.product.slug not in settings.DISC_PRODUCT_SLUGS: 
                if item.product not in orders:
                    orders[item.product]=Decimal(0)
                orders[item.product]=orders[item.product]+item.quantity

    return render_to_response('events/hostestportal.html', {
        'event': event, 'attendees': attendees, 'orders':orders
    }, context_instance=RequestContext(request))

@login_required
def event_attendees_list(request, code):
    # Moved this around a little so code shouln't have to be repeated
    try:
        event = Event.objects.get(code=code, owner=request.user)
    except Event.DoesNotExist:
        raise Http404

    # If request is AJAX delte this user from the event
    # TODO: Move this to a separate method so that several ajax operations can be made to this resource
    if request.is_ajax():
        attendee_id = request.GET.get("resource_id", None)
        try:
            attendee = Attendee.objects.get(pk=attendee_id)
            attendee.delete()
            message = {"status": "Success", "message": "Attendee has been deleted successfully"}
        except Attendee.DoesNotExist:
            message = {"status": "Failure", "message": "Attendee ID does not exist. Please try again later."}

        response = simplejson.dumps(message, ensure_ascii=False)
        return HttpResponse(response, mimetype="application/json")
        

    return render_to_response('events/attendees_list.html', {
        'event': event,
    }, context_instance=RequestContext(request))
        

def event_attendees(request, code, is_admin=False):
    redirect_to = request.GET.get('next', False)
    ajax = request.POST.get('ajax', False)
    try:
        event = Event.objects.get(code=code)
    except Event.DoesNotExist:
        raise Http404

    if request.method == "POST":
        tmp_attendee=None
        if 'attendee_id' in request.POST:
            tmp_attendee=Attendee.objects.filter(pk=request.POST.get('attendee_id'))
            if tmp_attendee.count()>0:
                form = AttendeeForm(request.POST, instance=tmp_attendee[0])
            else:
                tmp_attendee=None

        if not tmp_attendee:
            form = AttendeeForm(request.POST)
        
        if form.is_valid():
            attend=None
            if not tmp_attendee:
                # Make sure the use has not been invited yet.
                try:
                    attend = Attendee.objects.get(event=event, email=request.POST['email'])
                except Attendee.DoesNotExist:
                    pass

            if not attend:
                attendee = form.save(commit=False)
                attendee.event = event
                # See if there is a valid user relating to the email that was given.
                try:
                    user = User.objects.get(email=request.POST['email'])
                except:
                    user = None
                if user:
                    attendee.user = user
                attendee.save()
                
                if ajax:
                    return HttpResponse(simplejson.dumps({'status':{'success':True}, 'data':{'name':attendee.name(), 'first_name':attendee.first_name, 'last_name':attendee.last_name, 'id':attendee.pk, 'email':attendee.email, 'phone':attendee.phone, 'street':attendee.street, 'street2':attendee.street2, 'zip':attendee.postal_code, 'city':attendee.city, 'state':(attendee.state if attendee.state else ''), 'status':attendee.status }}))
            elif ajax:
                return HttpResponse(simplejson.dumps({'status':{'success':False, 'errors':{'email':['This email is already registered']}}}))
                
            if is_admin:
                url = reverse('events_manager_detail', args=[event.code])
            else: 
                url = reverse('events_detail', args=[event.code])

            if redirect_to:
                return HttpResponseRedirect(redirect_to)
            else:
                return HttpResponseRedirect(url)

        elif ajax:
            return HttpResponse(simplejson.dumps({'status':{'success':False, 'errors':form.errors}}))

    else:
        form = AttendeeForm()

    if is_admin:
        template_name = 'events/attendees_manager.html'
    else:
        template_name = 'events/attendees.html'

    return render_to_response(template_name, {
        'form': form, 'event': event, 'is_admin': is_admin,
    }, context_instance=RequestContext(request))
    

@login_required    
def event_attendee_edit(request, code=None, attendee_id=None, is_admin=False):
    """
    MRVG: Did not really understood how the method above this (event_attendees) was supposed to behave on updates
    so I rolled out my own quickly.
    ALSO THIS PROBABLY SHOULD BE DELETED AS AMAN WAS ALREADY WORKING ON IT AS IT WAS REALLY MEANT TO BE, NOT THIS PATCHED-UP JOB.
    """
    # Make sure the event exists
    event = get_object_or_404(Event, code=code)
    attendee = get_object_or_404(Attendee, pk=attendee_id)
    
    # If request is post, process data and save
    if request.method == "POST":
        form = AttendeeForm(request.POST)
        if form.is_valid():
            # This is a weird way to do it. Not sure why the model won't automatically keep the reference
            form.cleaned_data["event_id"] = event.id
            form.cleaned_data["attendee_id"] = attendee.pk
            if attendee.user:
                form.cleaned_data["user_id"] = attendee.user.pk
                
            form.save()
            
            return redirect("events_attendee_list", code=event.code)
    else:
        form = AttendeeForm(instance=attendee)
        
    return render_to_response("events/attendees_manager.html", {'form': form, 'event': event, 'is_admin': is_admin, "edit": True}, context_instance=RequestContext(request))
    

def event_attendee_status(request, code, attendee_code, status=None, is_admin=False):
    try:
        event = Event.objects.get(code=code)
    except Event.DoesNotExist:
        raise Http404

    try:
        attendee = Attendee.objects.get(code=attendee_code)
    except Attendee.DoesNotExist:
        raise Http404

    if request.method == "POST":
        form = AttendeeStatusForm(request.POST)
        if form.is_valid():
            attendee.status = form.cleaned_data['attend']
            attendee.save()
            # TODO do something here redirect or display a message to the user.
            # TODO Fire off email if they respond with NO.
            return HttpResponseRedirect("?status=%s"%request.POST.get('attend'))
    else:
        attend=request.GET.get('attend', False)
        if attend and (attend=='1' or attend=='2'):
            form=AttendeeStatusForm(request.GET)
            if form.is_valid():
                attendee.status = form.cleaned_data['attend']
                attendee.save()
                return HttpResponseRedirect("?status=%s"%request.GET.get('attend'))
        else:
            form=AttendeeStatusForm()
        
    template_name = 'events/attendees_status.html'
    return render_to_response(template_name, {
        'form': form, 'event': event, 'is_admin': is_admin, 'is_attendee_status': True,
    }, context_instance=RequestContext(request))

def event_details(request, code, is_admin=False):
    try:
        event = Event.objects.get(code=code)
    except Event.DoesNotExist:
        raise Http404

    if is_admin:
        template_name = 'events/details_manage.html'
    else:
        template_name = 'events/details.html'

    return render_to_response(template_name, {
        'event': event, 'is_admin': is_admin,
    }, context_instance=RequestContext(request))
