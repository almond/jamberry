from django.contrib import admin

from apps.events.models import Event, Attendee, EventPhoto


class AttendeeInline(admin.StackedInline):
    model = Attendee


class EventAdmin(admin.ModelAdmin):
    inlines = [AttendeeInline]

class EventPhotoAdmin(admin.ModelAdmin):
    pass


admin.site.register(Event, EventAdmin)
admin.site.register(EventPhoto, EventPhotoAdmin)
