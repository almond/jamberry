import datetime

from django import forms
from django.forms import ModelForm
from django.forms.formsets import formset_factory
from django.conf import settings
from apps.events.models import Event, Host, ShipTo, Attendee, EventPhoto, ATTEND


class AttendeeStatusForm(forms.Form):
    attend = forms.ChoiceField(choices=ATTEND, help_text="Are you attending?")


class EventPhotoForm(ModelForm):
    class Meta:
        model = EventPhoto
        exclude = ('name', 'photo')


class EventForm(ModelForm):
    starts_at=forms.DateTimeField(widget=forms.DateTimeInput(format=settings.DATE_INPUT_FORMATS[0]), input_formats=settings.DATE_INPUT_FORMATS)
    def clean_starts_at(self):
        starts_at = self.cleaned_data['starts_at']

        now = datetime.datetime.now()
        if now > starts_at:
            raise forms.ValidationError("Please select a date.")
        return starts_at

    class Meta:
        model = Event
        exclude = ('owner', 'is_closed', 'created_at', 'closed_at', 'redeemed_hostess_reward')

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['state'].widget.attrs['class'] = 'exclude'
        self.fields['photo'].widget.attrs['class'] = 'exclude'
        self.fields['status'].choices=[(1, 'Requested'), (2, 'Scheduled'), (3, 'Open'), (5, 'Canceled')]

class HostForm(ModelForm):

    class Meta:
        model = Host
        exclude = ('event', 'user', 'has_accepted')

class ShipToForm(ModelForm):

    class Meta:
        model = ShipTo
        exclude = ('event', 'has_shipped', 'user')

class AttendeeForm(ModelForm):
    #exclude
    class Meta:
        model = Attendee
        exclude = ('event', 'user', 'is_attending', 'birthday')

    def __init__(self, *args, **kwargs):
        super(AttendeeForm, self).__init__(*args, **kwargs)
        self.fields['status'].widget.attrs['class'] = 'exclude'
        self.fields['state'].widget.attrs['class'] = 'exclude'

    def clean_street(self):
        if 'first_name' in self.cleaned_data:
            if self.cleaned_data['first_name']==self.cleaned_data['street']:
                raise forms.ValidationError("Cannot be same as first name.")
        return self.cleaned_data['street']

AttendeeFormSet = formset_factory(AttendeeForm, extra=2)
