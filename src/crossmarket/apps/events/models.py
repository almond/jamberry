import datetime
import uuid

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum, Q
from django.db.models.signals import post_save
from django.contrib.localflavor.us.models import (PhoneNumberField,
    USPostalCodeField, USStateField)
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from satchmo_store.shop.models import Order
from product.models import Product


ATTEND = (
    (1, 'Yes'),
    (2, 'No'),
    (3, 'Maybe'),
    (4, 'Pending'),
)

STATUS = (
    (1, "Requested"),
    (2, "Scheduled"),
    (3, "Open"),
    (4, "Closed"),
    (5, "Canceled"),
)


def code_maker(model=None, length=6, key_name='code'):
    """Random code generator."""
    params = {key_name: None}
    while True:
        key = uuid.uuid4().hex[:length]
        params[key_name] = key

        try:
            has_key = model.objects.get(**params)
        except model.DoesNotExist:
            break

    return key


class EventManager(object):
    def past_events(self):
        now = datetime.datetime.now()
        delta = datetime.timedelta(hours=1)
        return self.filter(start__lte=now-delta).order_by('-starts_at')
        
    def future_events(self):
        now = datetime.datetime.now()
        delta = datetime.timedelta(hours=1)
        return self.filter(start__gte=now-delta).order_by('-starts_at')
    
class Event(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=50)
    description = models.TextField()
    code = models.SlugField(max_length=50, editable=False, unique=True, default=lambda: code_maker(Event))
    starts_at = models.DateTimeField(verbose_name="Party Date")
    
    street = models.CharField(max_length=100)
    street2 = models.CharField(max_length=100, blank=True, null=True) 
    city = models.CharField(max_length=75)
    state = USStateField()
    postal_code = models.CharField("Zip/Postal", max_length=12)

    use_personal_website = models.BooleanField(default=False)
    use_evite = models.BooleanField(default=False)

    photo = models.ForeignKey("EventPhoto")

    is_closed = models.BooleanField(default=False)
    closed_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.datetime.now)
    status = models.IntegerField(choices=STATUS, default=1)
    
    #ALTER TABLE `events_event` ADD COLUMN `redeemed_hostess_reward` TINYINT(1)  NULL DEFAULT false;
    redeemed_hostess_reward = models.BooleanField(default=False)


    objects = EventManager()


    def attendee_cart(self):
        return EventAttendeeCart.objects.filter(event=self, active=True)

    def can_order(self):
        if self.status==3 and self.starts_at>(datetime.datetime.today()):
            return True
        return False
    
    def is_future(self):
        return self.starts_at >= datetime.datetime.now()

    def __unicode__(self):
        return "%s, %s" % (self.code, self.starts_at.date())

    class Meta:
        ordering = ('starts_at', 'name',)

    def attendee(self):
        return Attendee.objects.filter(event=self)

    def host(self):
        return Host.objects.get(event=self)

    def orders(self):
        from apps.compensation.models import PartyOrder
        orders=[]
        pos=PartyOrder.objects.filter(party=self)
        for po in pos:
            orders.append(po.order)
        return orders

    def orders_total(self):
        from apps.compensation.models import PartyOrder
        st=PartyOrder.objects.filter(party=self).aggregate(total=Sum('order__sub_total'))
        st=(st['total'] if st['total'] is not None else 0.00 )
        dt=PartyOrder.objects.filter(party=self).aggregate(total=Sum('order__discount'))
        dt=( dt['total'] if dt['total'] is not None else 0.00 )
        return st-dt

    def hostess_rewards(self):
        from apps.compensation.models import HostingReward
        total=self.orders_total()
        hr=HostingReward.objects.filter((Q(min_sale__lte=total)&Q(max_sale=0.00)) | Q(min_sale__lte=total)&Q(max_sale__gt=total) ).order_by('min_sale')
        if len(hr)>0:
            return hr[0]
        return False

class EventPhoto(models.Model):
    name = models.CharField(max_length=25)
    photo = models.ImageField(upload_to="events/photos/")
    is_enabled = models.BooleanField(default=True)
    
    def image_tag(self):
        return '<img src="%s" width="" height="" alt="">' % self.photo.url
    
    def __unicode__(self):
        return "%s" % (self.name)


class Host(models.Model):
    event = models.OneToOneField(Event)
    user = models.ForeignKey(User, blank=True, null=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    street2 = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=75)
    state = USStateField()
    postal_code = models.CharField("Zip/Postal", max_length=12)

    phone = PhoneNumberField()
    email = models.EmailField()

    has_accepted = models.BooleanField(default=False)

    def name(self):
        return "%s %s"%(self.first_name, self.last_name)


class HostReward(models.Model):
    host = models.ForeignKey(Host)
    order = models.ForeignKey(Order)
    personal_sales = models.DecimalField(max_digits=9, decimal_places=2)
    reward_credit = models.DecimalField(max_digits=9, decimal_places=2)
    enroll_credit = models.DecimalField(max_digits=9, decimal_places=2)
    enroll_credit_redeemed = models.BooleanField()
    is_half_off = models.BooleanField()


class ShipTo(models.Model):
    event = models.OneToOneField(Event)
    user = models.ForeignKey(User, blank=True, null=True)
    
    name = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    street2 = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=75)
    state = USStateField()
    postal_code = models.CharField("Zip/Postal", max_length=12)

    phone = PhoneNumberField()
    email = models.EmailField()

    has_shipped = models.BooleanField(default=False)

#drop events_attendee events_event events_eventattendeecart events_eventattendeecartdetail events_host
#then syncdb
#and then import parties
class Attendee(models.Model):
    event = models.ForeignKey(Event)
    code = models.SlugField(max_length=25, editable=False, unique=True,
        default=lambda: code_maker(Attendee))
    user = models.ForeignKey(User, blank=True, null=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    email = models.CharField(max_length=100)
    phone = PhoneNumberField(blank=True, null=True)
    
    street = models.CharField(max_length=100, blank=True, null=True)
    street2 = models.CharField(max_length=100, blank=True, null=True) 
    city = models.CharField(max_length=100, blank=True, null=True)
    state = USStateField(blank=True, null=True)
    postal_code = models.CharField("Zip/Postal", max_length=12,
        blank=True, null=True)

    birthday = models.DateField(blank=True, null=True)
    
    status = models.IntegerField(default=4, choices=ATTEND)

    def name(self):
        return "%s %s"%(self.first_name, self.last_name)

# Signal for emailing a newly added attendee.
def email_event_attendee(sender, **kwargs):
    instance = kwargs.get("instance", None)
    created = kwargs.get("created", False)

    if created:
        html = render_to_string('events/email_invite.html', {
            'event': instance.event, 'code': instance.code,
            'STATIC_URL': settings.STATIC_URL, "is_email":True})
        subject = 'Party invite: %s' % instance.event.name
        msg = EmailMessage(subject, html, settings.DEFAULT_FROM_EMAIL, [instance.email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()

post_save.connect(email_event_attendee, sender=Attendee)


class EventAttendeeCart(models.Model):
    event=models.ForeignKey(Event)
    attendee=models.ForeignKey(Attendee)
    direct_ship=models.BooleanField(default=False)
    active=models.BooleanField(default=False)
    def details(self):
        return EventAttendeeCartDetail.objects.filter(attendee_cart=self)
    
class EventAttendeeCartDetail(models.Model):
    attendee_cart=models.ForeignKey(EventAttendeeCart)
    product=models.ForeignKey(Product)
    quantity=models.IntegerField(default=0)
