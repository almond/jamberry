import os, csv, random, time, datetime

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User

from apps.events.models import Event, Host, ShipTo, EventPhoto


class Command(BaseCommand):
    """
    Example Data:
        'dateClose': '',
        'consultantId': '10004',
        'enrollCreditRedeemed': '0',
        'fastStartDateOLD': '2011-08-09',
        'partyDateTime': '8/9/2011',
        'fastStartDate': '2011-08-09',
        'enrollCreditGuid': '{FAC4D8EC-EFA3-48A6-8688-5C88C1C7B323}',
        'city': 'Orem',
        'zip': '84057',
        'partyGuid': '{E2862229-F9B1-430E-94F2-36585A8DC306}',
        'comments': 'This is a test comment',
        'state': 'UT',
        'closed': '-1',
        'enrollCredit': '$0.00 ',
        'hostId': '0',
        'personalSales': '$0.00 ',
        'dateCreated': '7/1/2011',
        'phone': '1234567890',
        'partyId': '3',
        'address': '123 Fake St.',
        'partyStatusId': '2',
        'name': 'Launch Party',
        'hostName': 'Christy',
        'hostessRewardHalfOff': '',
        'hostessRewardCredit': '',
        'hostessRewardOrderID': '',
        'hostEmail': 'christy@fakeemail.com'

    """
    help='Import Parties!'

    def handle(self, *args, **options):
        file_path = os.path.abspath('%s../../../resources/jamberry_db_parties.csv' % settings.DIRNAME)
        data = csv.DictReader(open(file_path))
        loaded = 0
        no_duplicate_keys = []
        event_photo = EventPhoto.objects.all()[0]

        for row in data:
            # Check to see if the party exists and if so skip
            try:
                event = Event.objects.get(code=row['partyGuid'])
                continue
            except:
                pass

            try:
                user = User.objects.get(id=row['consultantId'])
            except:
                print "UserID: %s could not be found" % row['consultantId']
                continue

            loaded += 1
            event = Event()
            event.owner = user
            event.name = row['name'][:50]
            event.description = 'Jamberry Nails Party'
            event.code = row['partyGuid']
            event.status = row['partyStatusId']

            if row['partyDateTime'] != '':
                event.starts_at = datetime.datetime(*time.strptime(row['partyDateTime'], "%m/%d/%Y")[0:5])
            else:
                event.starts_at = datetime.datetime(*time.strptime(row['dateCreated'], "%m/%d/%Y")[0:5])

            event.created_at = datetime.datetime(*time.strptime(row['dateCreated'], "%m/%d/%Y")[0:5])
            event.street = row['address']
            event.street2 = ''
            event.city = row['city']
            event.state = row['state']
            event.postal_code = row['zip'][:10]
            event.use_personal_website = True
            event.use_evite = True
            event.photo = event_photo

            event.is_closed = False
            if row['dateClose'] != '':
                event.is_closed = True
                event.closed_at = datetime.datetime(*time.strptime(row['dateClose'], "%m/%d/%Y")[0:5])

            if row['dateCreated'] != '':
                event.created_at = datetime.datetime(*time.strptime(row['dateCreated'], "%m/%d/%Y")[0:5])

            event.save()

            host = Host(user=user)
            #host.name = row['hostName'][:100]
            hostName=row['hostName'].split(" ", 1)
            host.first_name=hostName[0]
            host.last_name=hostName[1] if len(hostName)>1 else ''
            host.event = event
            host.email = row['hostEmail']
            host.has_accepted = False
            host.street = row['address']
            host.street2 = ''
            host.city = row['city']
            host.state = row['state'][:2]
            host.postal_code = row['zip'][:10]
            host.phone = row['phone'][:13]
            host.save()

            ship_to = ShipTo(user=user)
            ship_to.event = event
            ship_to.name = row['name'][:100]
            ship_to.street = row['address']
            ship_to.street2 = ''
            ship_to.city = row['city']
            ship_to.state = row['state']
            ship_to.postal_code = row['zip'][:10]
            ship_to.phone = row['phone'][:13]
            ship_to.email = row['hostEmail']
            
            ship_to.has_shipped = False
            if row['dateClose'] != '':
                ship_to.has_shipped = True

            ship_to.save()
        print "%s Parties loaded" % loaded
