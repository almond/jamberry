import datetime

from django.core.management.base import BaseCommand, CommandError

from apps.events.models import Event


class Command(BaseCommand):
    """
    Manager method to be ran as a cron to auto close a party that 
    has a status of 'scheduled' or 'open' and a party has alredy occured.
    """
    help='Auto Close Parties after they have happened.'

    def handle(self, *args, **options):
      three_days=datetime.datetime.now()-datetime.timedelta(days=2)
      events = Event.objects.filter(starts_at__lte=three_days, status__in=[2,3])
      events.update(status=4)
      
