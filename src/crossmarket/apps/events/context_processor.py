import datetime

from django.conf import settings

from apps.events.models import Event

def has_parties(request):
    now = datetime.datetime.now()
    if request.user.is_authenticated():
        has_parties = Event.objects.filter(starts_at__gte=now, owner=request.user).count()
        return {'has_parties': bool(has_parties)}
    else:
        return {'has_parties': False}