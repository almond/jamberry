import os, datetime, hashlib

from django.db import models


HOSTS=(('youtube', 'YouTube'),)


# Obfuscate the filenames.
def get_document_path(instance, filename):
    split = filename.rsplit('.', 1)
    date = datetime.datetime.now()
    new_filename = hashlib.md5("%s%s" % (split[0], date.ctime())).hexdigest()
    filename = "%s.%s" % (new_filename, split[1])
    return os.path.join('archive/document/', filename)

def get_photo_path(instance, filename):
    split = filename.rsplit('.', 1)
    date = datetime.datetime.now()
    new_filename = hashlib.md5("%s%s" % (split[0], date.ctime())).hexdigest()
    filename = "%s.%s" % (new_filename, split[1])
    return os.path.join('archive/picture/', filename)
    #ALTER TABLE `jamberry`.`archive_document` ADD COLUMN `type` VARCHAR(2) NULL  AFTER `document` ;


class Video(models.Model):
    host = models.CharField(max_length=30,choices=HOSTS)
    video_id = models.CharField(max_length=256)
    thumbnail = models.ImageField(upload_to=get_photo_path)
    title = models.CharField(max_length=256)
    description = models.TextField(blank=True)
    show_in_dashboard = models.BooleanField(default=False)

    # MRVG: I made these changes to the DB schema
    #ALTER TABLE `jamberry`.`archive_video` ADD COLUMN `thumbnail` VARCHAR(140) NULL AFTER `video_id`
    #ALTER TABLE `jamberry`.`archive_video` ADD COLUMN `show_in_dashboard` BOOL NULL AFTER `description`

    def __unicode__(self):
        return self.title

    def embed(self, height=480, width=390):
        if self.host=='youtube':
            return '<iframe title="YouTube video player" width="%s" height="%s" src="http://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>'%(self.video_id, width, height)
        elif self.host=='vimeo':
            return '<iframe src="http://player.vimeo.com/video/%s?title=0&amp;byline=0&amp;portrait=0" width="%s" height="%s" frameborder="0"></iframe>'%(self.video_id, width, height)
        else:
            return ''


class Document(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    document = models.FileField(upload_to=get_document_path)
    type = models.CharField(max_length=2, choices=(('tm', 'Training & Marketing'), ('ci', 'Consultant Information'), ('cf', 'Consultant Forms')), default='tm' )
    def __unicode__(self):
        return self.title


class Picture(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    image = models.FileField(upload_to=get_photo_path)

    def __unicode__(self):
        return self.title
