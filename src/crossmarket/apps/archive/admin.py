from django.contrib import admin

from apps.archive.models import *


admin.site.register(Video)
admin.site.register(Document)
admin.site.register(Picture)
