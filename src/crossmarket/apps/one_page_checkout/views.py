from apps.utils import ajax_render
from apps.pricing.models import WholesaleDiscount
from django import http
from django.core import urlresolvers
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from livesettings import config_get_group, config_value
from payment.config import gateway_live
from payment.forms import PaymentContactInfoForm
from payment.views.confirm import ConfirmController
from payment.views.payship import credit_pay_ship_process_form, \
    pay_ship_info_verify
from product.models import Product
from product.utils import find_best_auto_discount
from satchmo_store.contact import CUSTOMER_ID
from satchmo_store.contact.models import Contact
from satchmo_store.shop.models import Cart, Config, Order
from signals_ahoy.signals import form_initialdata
from django.conf import settings
from apps.events.models import Event
from apps.api_wrapper.wrapper import AvaTaxApi
import django.dispatch
import logging
from decimal import *
from apps.percent.processor import Processor
from apps.events.models import *
log = logging.getLogger('satchmo_store.contact.contact')


#from satchmo_store.shop.signals import * #satchmo_cart_changed, satchmo_cart_add_complete, satchmo_cart_details_query, satchmo_cart_view
satchmo_cart_view=django.dispatch.Signal()

def checkout(request, tour_slug=None):
    # Check if there's a bus tour or something in the cart
    if tour_slug:
        try:
            # Empty cart
            cart = Cart.objects.from_request(request, create=True)
            cart.empty()
            bus_tour = Product.objects.get(slug=tour_slug)
            cart.add_item(bus_tour, 1)
        except Product.DoesNotExist:
            bus_tour = "Bus tour link corrupted"
    else:
        bus_tour = tour_slug
            
    tempCart = Cart.objects.from_request(request)
    if tempCart.numItems == 0:
        return render_to_response('flatpages/default.html', 
                {'flatpage':{'content':'<h4>Your cart is empty.</h4>', 'safe':True}}, 
                context_instance = RequestContext(request))
        """Return Cart is Empty"""
        

    """Getting CART details"""
    default_view_tax = config_value('TAX', 'DEFAULT_VIEW_TAX')
    cart = Cart.objects.from_request(request)


    if cart.numItems > 0:
        products = [item.product for item in cart.cartitem_set.all()]
        sale = find_best_auto_discount(products)
    else:
        sale = None


    satchmo_cart_view.send(cart, cart=cart, request=request)

        
    cart_details={
        'cart':cart,
        'default_view_tax':default_view_tax,
        'sale':sale,
    }

    """Contact Details"""
    init_data={}
    if request.user.is_authenticated():
        if request.user.email:
            init_data['email'] = request.user.email
        if request.user.first_name:
            init_data['first_name'] = request.user.first_name
        if request.user.last_name:
            init_data['last_name'] = request.user.last_name
    try:
        contact = Contact.objects.from_request(request, create=False)
        for item in contact.__dict__.keys():
            init_data[item] = getattr(contact,item)
        if contact.shipping_address:
            for item in contact.shipping_address.__dict__.keys():
                init_data["ship_"+item] = getattr(contact.shipping_address,item)
        if contact.billing_address:
            for item in contact.billing_address.__dict__.keys():
                init_data[item] = getattr(contact.billing_address,item)
        if contact.primary_phone:
            init_data['phone'] = contact.primary_phone.phone
        
        # Fetch cart price and tax
        price_numbers = _get_total_and_subtotal(cart, init_data["postal_code"])
        cart_details['price_numbers'] = price_numbers
 
        
    except Contact.DoesNotExist:
        contact = None
        # Allow them to login from this page.
        request.session.set_test_cookie()
        cart_details['price_numbers'] = ('Enter zip-code'*3,)

    shop=Config.objects.get_current()
    #Request additional init_data
    form_initialdata.send(sender=PaymentContactInfoForm, initial=init_data, contact=contact, cart=tempCart, shop=shop)
    form = PaymentContactInfoForm(shop=shop, contact=contact, shippable=tempCart.is_shippable, initial=init_data, cart=tempCart)
    if shop.in_country_only:
        only_country = shop.sales_country
    else:
        only_country = None
        
    contact_details={'form':form, 'country':only_country, 'paymentmethod_ct':len(form.fields['paymentmethod'].choices)}

    """Credit Card Form"""

    form_handler=credit_pay_ship_process_form
    payment_module=config_get_group('PAYMENT_AUTHORIZENET')
    results=pay_ship_info_verify(request, payment_module)
    if not results[0]:
        """Need to do something here"""
        contact = None #results[1]
        working_cart = tempCart #results[2]
    else:
        contact = results[1]
        working_cart = results[2]

    results = form_handler(request, contact, working_cart, payment_module)
    if results[0]:
        """Need to do something here too"""
        pass

    form = results[1]
    card_details={
        'form': form,
        'PAYMENT_LIVE': gateway_live(payment_module),
        'cart':working_cart
    }
    
    return render_to_response('one_page_checkout/form.html', {"selected_frontend_party":request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT, None), "bus_tour": bus_tour,'cart_details':cart_details, 'contact_details':contact_details, 'card_details':card_details}, context_instance = RequestContext(request))


def verify(request):
    #View which collects demographic information from customer.
    
    if request.method!='POST':
        return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'__all__':['This URL supports POST request only.',]}}})
    
    if 'is_party_order' in request.POST and ('party_id' not in request.POST or (not request.POST['party_id'].isdigit())):
        return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'party_id':['Please select a party.', ]}}})
    elif 'is_party_order' in request.POST and 'party_id' in request.POST and request.POST['party_id'].isdigit():
        if not Event.objects.filter(status=3, pk=request.POST.get('party_id')).exists():
            return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'party_id':['This party is not available.', ]}}})
        else:
            if (not request.user.is_authenticated()):
                request.session[settings.FRONTEND_PARTY_SESSION_SELECT]=request.POST.get('party_id')
            else:
                tempCart = Cart.objects.from_request(request)
                if tempCart.numItems > 0:

                    if settings.FRONTEND_PARTY_SESSION_SELECT in request.session:
                        del request.session[settings.FRONTEND_PARTY_SESSION_SELECT]
                    slug = WholesaleDiscount.objects.get(slug=settings.PARTY_WHOLESALE_DISCOUNT_SLUG)
                    request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY]=slug
                    request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY]=request.POST.get('party_id')
                    
                    party=Event.objects.get(pk=request.session.get(settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY))
                    attendees=Attendee.objects.filter(event=party, email=request.user.email)
                    if len(attendees)>0:
                        attendee=attendees[0]
                    else:
                        attendee=Attendee(event=party, name=request.user.first_name, status=1, email=request.user.email)
                        attendee.save()

                    request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY]=[attendee.pk, ]
                    items=tempCart.cartitem_set.all()
                    order_details={attendee.pk:{}}
                    for i in items:
                        if i.product.slug not in settings.DISC_PRODUCT_SLUGS:
                            order_details[attendee.pk][i.product.pk]=i.quantity
                    request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]=order_details

    #First verify that the cart exists and has items
    tempCart = Cart.objects.from_request(request)
    if tempCart.numItems == 0:
        #Empty Cart
        return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'__all__':['Your cart is empty. Please add some items to your cart, then complete the checkout process.', ]}}})

    if not request.user.is_authenticated() and config_value('SHOP', 'AUTHENTICATION_REQUIRED'):
        #Login Reguired
        url=urlresolvers.reverse('satchmo_checkout_auth_required')
        opc_checkout_url=urlresolvers.reverse('opc_checkout')
        login_url=url+"?next="+opc_checkout_url
        return ajax_render({'status':{'success':False, 'code':'login_required', 'reason':{'__all__':['You need to login before checking out.', ]}, 'login_url':login_url}})

    init_data={}
    shop=Config.objects.get_current()
    if request.user.is_authenticated():
        if request.user.email:
            init_data['email'] = request.user.email
        if request.user.first_name:
            init_data['first_name'] = request.user.first_name
        if request.user.last_name:
            init_data['last_name'] = request.user.last_name
    try:
        contact = Contact.objects.from_request(request, create=False)
    except Contact.DoesNotExist:
        contact = None

    try:
        order = Order.objects.from_request(request)
        if order.discount_code:
            init_data['discount'] = order.discount_code
    except Order.DoesNotExist:
        pass

    new_data = request.POST.copy()
    if not tempCart.is_shippable:
        new_data['copy_address'] = True
    form = PaymentContactInfoForm(data=new_data, shop=shop, contact=contact, shippable=tempCart.is_shippable, initial=init_data, cart=tempCart)

    if form.is_valid():
        if contact is None and request.user and request.user.is_authenticated():
            contact = Contact(user=request.user)
        custID = form.save(request, cart=tempCart, contact=contact)
        request.session[CUSTOMER_ID] = custID

        modulename = new_data['paymentmethod']
        if not modulename.startswith('PAYMENT_'):
            modulename = 'PAYMENT_' + modulename
        paymentmodule = config_get_group(modulename)
        
        #return ajax_render({'status':{'success':True}})
        pass #for a while
    else:
        return ajax_render({'status':{'success':False, 'code':'general', 'reason':form.errors}})


    """Verify generic credit card data"""
    form_handler=credit_pay_ship_process_form
    #payment_module=config_get_group(request.POST.get('paymentmethod'))
    results=pay_ship_info_verify(request, paymentmodule)
    
    if not results[0]:
        """
        It should never enter this block, if the request has come all the way down till here.
        It check for a contact or cart in the session.
        """
        return results[1]

    contact = results[1]
    working_cart = results[2]
    results = form_handler(request, contact, working_cart, paymentmodule)
    if results[0]:
        """If form has no error.. result[1] has Http Redirection to confirmation URL"""
        #Processing credit card
        controller=ConfirmController(request, paymentmodule)
        if controller.confirm():
            return ajax_render({'status':{'success':True}})
        else:
            return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'__all__': [controller.processorMessage, ]}}})
    
    else:
        return ajax_render({'status':{'success':False, 'code':'general', 'reason':results[1].errors}})


def get_tax(request):
    """
    Return a calculated tax depending on sent zip_code via get parameters.
    Pretty much just a gateway to the utility method below
    """
    if request.is_ajax():
        cart = Cart.objects.from_request(request)
        result = _get_total_and_subtotal(cart, zip_code=request.GET.get("zip_code", "84101"))
        
        data_to_json = {
                'data': {'total': float(result[0]), 'subtotal': float(result[1]), 'tax_amount': float(result[2])}, 
                'message': "Success",
        }
        return http.HttpResponse(simplejson.dumps(data_to_json, ensure_ascii=False), mimetype="application/json")
    else:
        return http.HttpResponse("Only ajax calls please")


def _get_total_and_subtotal(cart, zip_code="84101"):
    """
    Return the total and subtotal of a cart passed as parameter

    TODO: I am pretty sure satchmo has a way of doing this but I am beyond pissed off at it 
    to look for it. Just writting everything myself. Sick of magic everywhere
    """
    total = Decimal(0)
    subtotal = Decimal(0)
    tax_amount = Decimal(0)
    discount = Decimal(0)
    tax_rate = Decimal(Processor(zip_code=zip_code).rate)

    for item in cart.cartitem_set.all():
        # Hate non-functional alternatives. But really want to finish this and do something fun
        quantity_price = item.get_qty_price(item.quantity) * item.quantity
        if quantity_price > 0:
            subtotal += quantity_price
        else:
            # Get the discounts
            discount = abs(quantity_price)

    subtotal = subtotal - discount
    tax_amount = subtotal * (tax_rate/100)
    total = subtotal + tax_amount
    return (total, subtotal, tax_amount)
