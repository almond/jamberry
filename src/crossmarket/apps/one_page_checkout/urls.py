from django.conf.urls.defaults import patterns, include, url

from views import verify, checkout, get_tax

urlpatterns = patterns('',
    url(r'^verify/$', verify, {}, name="opc_verify"), 
    url(r'^get_tax/$', get_tax, {}, name="opc_get_tax"), 
    url(r'^(?P<tour_slug>[-\w]+)/$', checkout, {}, name="opc_checkout_slug"),
    url(r'^$', checkout, {}, name="opc_checkout"), 
)
