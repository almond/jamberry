from django.db import models
from django.contrib.auth.models import User

class Email(models.Model):
    subject=models.CharField(max_length=256)
    body=models.TextField()
    by=models.ForeignKey(User, related_name='email_sender')
    sent_on=models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return "%s: %s"%(self.by.username, self.subject)
    
class EmailRecipients(models.Model):
    email_body=models.ForeignKey(Email)
    email_id=models.EmailField()
    receipient=models.CharField(max_length=256)
