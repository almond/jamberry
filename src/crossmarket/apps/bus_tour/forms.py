from django import forms
from django.utils.translation import ugettext_lazy as _


class BusRegistrationForm(forms.Form):
    name = forms.CharField(max_length=100, label=_(u"Name"))
    email = forms.CharField(max_length=100, label=_(u"Email"))
    primary_phone = forms.CharField(max_length=100, label=_(u"Primary Phone"))
    
        