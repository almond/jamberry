from apps.utils import get_items_in_cart
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template.context import RequestContext
from django.utils import simplejson as json
from product.models import Category, Product
from satchmo_store.shop.models import Cart
from apps.account.models import Profile


@login_required
def index(request):
    """Display a list of available bus tours that allows the selection of them and what not"""
    category = Category.objects.get(slug="bus-tours")
    products = Product.objects.filter(category__pk=category.pk, active=True)
    tours_taken = Product.objects.filter(orderitem__product__in=products, orderitem__order__contact=request.user.contact)
    return render_to_response("bus_tour/select-products.html", 
                              {"products": products,
                               "tours_taken": tours_taken, 
                               "item_in_cart": json.dumps(get_items_in_cart(request, all_data=True))}, 
                              context_instance=RequestContext(request))
    

@login_required    
def bus_signup_form(request, tour_id=0):
    """
    Just add to cart and redirect to one page checkout form.
    """
    bus_tour = get_object_or_404(Product, pk=tour_id)
    cart = Cart.objects.from_request(request, create=True)
    
    # Add product before redirecting    
    cart.add_item(bus_tour, 1)
    
    return redirect("opc_checkout_slug", tour_slug=bus_tour.slug)