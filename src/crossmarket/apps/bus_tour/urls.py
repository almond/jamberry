from django.conf.urls.defaults import patterns, url


urlpatterns = patterns('apps.bus_tour.views', 
    url(r'^$', 'index', {}, name="bus_tour_index"),
    url(r'^add-to-cart/(?P<tour_id>\d+)/$', 'bus_signup_form', name="bus_tour_addtocart"),
)
