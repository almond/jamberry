from models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
import datetime
from django.db.models import Q 
from django.http import HttpResponse

@login_required
def view_support_list(request):
    return render_to_response('manage/tools-support-list.html', {}, context_instance=RequestContext(request))


@login_required
def close_support(request, support_id):
    sr=SupportRecipients.objects.filter(user=request.user, request__id=support_id)
    if len(sr)<1:
        raise Http404
    else:
        srequest=sr[0].request
        srequest.status='closed'
        srequest.save()
    return HttpResponseRedirect(reverse('manager_tools_view_support', args=[support_id]))

@login_required
def view_support(request, support_id):
    sr=SupportRecipients.objects.filter(user=request.user, request__id=support_id)
    if len(sr)<1:
        raise Http404
    else:
        if request.method=='POST':
            body=request.POST.get('body', None)
            if body and sr[0].request.status != 'closed':
                SupportRequest(body=body, by=request.user, category=sr[0].request.category, reply_to=sr[0].request).save()
                SupportRecipients.objects.filter(~Q(user=request.user), request=sr[0].request).update(read_on=None)

        sr=sr[0]
        if sr.read_on is None:
            sr.read_on=datetime.datetime.now()
            sr.save()

        
        return render_to_response('manage/tools-support-read.html', {'requested':sr}, context_instance=RequestContext(request))


@login_required
def tools_support(request):
    error=False
    if request.method=='POST':
        cat=request.POST.get('cat', None)
        subject=request.POST.get('subject', None)
        body=request.POST.get('body', None)

        if cat is None:
            error='Please select a category.'
        if subject is None or body is None:
            error='Please write both subject and body.'

        if not error:
            if "subcat_%s"%(cat) in request.POST and request.POST["subcat_%s"%(cat)] !='':
                cat=request.POST.get("subcat_%s"%(cat))
            cat=SupportCategory.objects.get(pk=cat)
                
            sr=SupportRequest(subject=subject, body=body, by=request.user, category=cat)
            sr.save()
            
            recepient_groups=cat.parent.support_manager.all() if cat.parent else cat.support_manager.all()
            recepients=User.objects.filter(groups__in=recepient_groups).distinct()
            for r in recepients:
                SupportRecipients(request=sr, user=r).save()

            try:
                r=SupportRecipients.objects.get(request=sr, user=request.user)
                r.read_on=datetime.datetime.now()
                r.save()
            except SupportRecipients.DoesNotExist:
                SupportRecipients(request=sr, user=request.user, read_on=datetime.datetime.now()).save()
                
    categories=SupportCategory.objects.filter(parent=None)
    return render_to_response('manage/tools-support.html', {'supportcategories':categories, 'error':error}, context_instance=RequestContext(request))
