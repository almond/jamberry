from django.db import models
from django.contrib.auth.models import User, Group

SUPPORT_REQUEST_STATUS_CHOICES = (
    ('unread', 'Unread'),
    ('open', 'Open'),
    ('closed', 'Closed'),
    ('input', 'Waiting for input'),
    ('pending', 'Pending'),
)

class SupportCategoryManager(models.Manager):
    def children(self, support_cat_obj):
        return self.filter(parent=support_cat_obj, is_enabled=True)

class SupportCategory(models.Model):
    parent=models.ForeignKey('self', null=True, blank=True, related_name='children')
    name=models.CharField(max_length=50, help_text="Category Name", unique=True)
    ordering=models.IntegerField(blank=True, null=True, help_text="Display Ordering")
    is_enabled=models.BooleanField(default=True, help_text="Enable Category")
    support_manager=models.ManyToManyField(Group)

    objects=SupportCategoryManager()

    class Meta:
        ordering = ['ordering', 'name']

    def has_children(self):
        return bool(SupportCategory.objects.children(self).count())

    def __str__(self):
        return self.name

class SupportRequest(models.Model):
    subject=models.CharField(max_length=256, blank=True, default='')
    body=models.TextField()
    by=models.ForeignKey(User, related_name='support_requestor')
    category=models.ForeignKey(SupportCategory, related_name='support_category')
    sent_on=models.DateTimeField(auto_now_add=True)
    reply_to=models.ForeignKey('self',  blank=True, null=True, default=None)
    
    # Added this field to control certain part of the dashboard. Will be clear later on
    status = models.CharField(max_length=20, choices=SUPPORT_REQUEST_STATUS_CHOICES, default='unread')
    
    def __unicode__(self):
        return "%s: %s"%(self.by.username, self.subject)

    def replies(self):
        return SupportRequest.objects.filter(reply_to=self).order_by('-sent_on')

class SupportRecipients(models.Model):
    request=models.ForeignKey(SupportRequest, related_name='support_request')
    user=models.ForeignKey(User)
    read_on=models.DateTimeField(null=True, default=None)
    unique=(('request', 'user'), )
