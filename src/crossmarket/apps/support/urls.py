from django.conf.urls.defaults import patterns, include, url

from apps.support.views import *

urlpatterns = patterns('',
    url(r'^$', tools_support, {}, name="manager_tools_support"), 
    url(r'^list/$', view_support_list, {}, name="manager_tools_view_supportlist"), 
    url(r'^view/(?P<support_id>[\d_]+)/$', view_support, {}, name="manager_tools_view_support"), 
    url(r'^close/(?P<support_id>[\d_]+)/$', close_support, {}, name="manager_tools_close_support"), 
)
