from django.contrib import admin
from models import *

class SubCategoryInline(admin.StackedInline):
    verbose_name = "Sub Categories"
    model = SupportCategory
    extra = 0
    fieldsets = (
        ("Sub Categories", {
            'fields': ('name', 'ordering', 'is_enabled',)
        }),
    )


class SupportCategoryAdmin(admin.ModelAdmin):
    list_display=('name',)
    inlines = [SubCategoryInline]

    fieldsets = (
        ("Section", {
            'fields': ('name', 'support_manager', 'is_enabled',)
        }),
    )

    def queryset(self, request):
        """
        Only show top level category items.
        """
        qs = super(SupportCategoryAdmin, self).queryset(request)
        return qs.filter(parent__isnull=True)

class SupportRequestAdminInline(admin.StackedInline):
    verbose_name = "Replies"
    model = SupportRequest
    extra = 0
    fieldsets = (
        ("Reply", {
            'fields': ('body', 'by', )
        }),
    )
    
    def save_model(self, request, obj, form, change):
        orignal_request=obj.reply_to
        SupportRecipients.objects.filter(request=orignal_request).update(read_on=None)
        
class SupportRequestAdmin(admin.ModelAdmin):
    list_display=('category', 'subject', 'by', 'sent_on')
    inlines = [SupportRequestAdminInline]
    fieldsets = (
        ("Request", {
            'fields': ('category', 'by', 'subject', 'body', 'status')
        }),
    )

    def queryset(self, request):
        qs = super(SupportRequestAdmin, self).queryset(request)
        return qs.filter(reply_to__isnull=True)

    class Meta:
        model = SupportRequest
    
    

admin.site.register(SupportCategory, SupportCategoryAdmin)
admin.site.register(SupportRequest, SupportRequestAdmin)
#admin.site.register(SupportRecipients)
