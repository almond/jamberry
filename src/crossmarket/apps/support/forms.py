from django.forms import ModelForm
from apps.support.models import SupportRequest, SupportCategory


class SupportRequestForm(ModelForm):
    class Meta:
        model = SupportRequest


class SupportCategoryForm(object):
    class Meta:
        model = SupportCategory
