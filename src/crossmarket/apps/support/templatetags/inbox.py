from django.template import Library, Node
from apps.support.models import *
class InboxOutput(Node):
    def render(self, context):
        try:
            user=context['user']
            count=SupportRecipients.objects.filter(user=user, read_on__isnull=True).count()
        except (KeyError, AttributeError):
            count = 0
        return "%s" % (count)        
        
def inbox_count(parser, token):
    return InboxOutput()


class SupportRequestReceived(Node):
    def __init__(self, variable_name):
        self.variable_name=variable_name

    def render(self, context):
        try:
            user=context['user']
            context[self.variable_name]=SupportRecipients.objects.filter(user=user, request__reply_to=None)
        except:
            pass
        return ''

def support_requests(parser, token):
    try:
        tag_name, tmp, variable_name=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag is used improperly." % token.contents.split()[0])
    return SupportRequestReceived(variable_name)


register = Library()     
register.tag('inbox_count', inbox_count)
register.tag('support_requests', support_requests)
