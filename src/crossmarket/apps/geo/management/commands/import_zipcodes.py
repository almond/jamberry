from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random
from crossmarket.apps.geo.models import *

class Command(BaseCommand):
    help='Import ZipCodes from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_zipcodes.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                zip=ZipCode.objects.get(zip_code=row[0])
            except ZipCode.DoesNotExist:
                zip=ZipCode(zip_code=row[0])
                
            #zip.zip_code=row[0]
            zip.city=row[4]
            zip.state_code=row[1]
            zip.state=row[5]
            zip.latitude=row[2]
            zip.longitude=row[3]
            zip.save()
