from models import *
from django.contrib import admin

class ZipCodeAdmin(admin.ModelAdmin):
    list_display=('zip_code', 'state_code', 'state', 'city', 'longitude', 'latitude')

admin.site.register(ZipCode, ZipCodeAdmin)

