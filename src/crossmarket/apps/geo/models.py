from django.db import models
#from django.contrib.gis.db import models

class ZipCode(models.Model):
    zip_code=models.CharField(unique=True, blank=False, null=False, max_length=5)
    city=models.CharField(max_length=128)
    state_code=models.CharField(max_length=2)
    state=models.CharField(max_length=128)
    latitude=models.FloatField()
    longitude=models.FloatField()
    #objects=models.GeoManager()kk
