from django.db import connection, transaction
def get_zips_in_radius(center_zip, radius_mile):
    query = """
            SELECT
              one.zip_code
            FROM
              `geo_zipcode` as one,
              `geo_zipcode` as two
            WHERE
              two.zip_code like('%s') and
              ((ACOS(SIN(two.latitude * PI() / 180) * SIN(one.latitude * PI() / 180) + COS(two.latitude * PI() / 180) * COS(one.latitude * PI() / 180) * COS((two.longitude - one.longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) < %d
            """ % (center_zip, radius_mile)

    cursor = connection.cursor()
    cursor.execute(query)
    zips=cursor.fetchall()
    zip_list=[x[0] for x in zips]
    if center_zip not in zip_list:
        zip_list.append(center_zip)

    return zip_list
