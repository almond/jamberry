import os, csv, random
from l10n.models import Country

from django.core.management.base import BaseCommand, CommandError
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.conf import settings

from satchmo_store.shop.models import Order, OrderItem
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from apps.compensation.models import CommissionableOrder
from apps.dashboard.models import OrderWebsite, OrderType
from apps.pricing.models import WholesaleDiscount


class Command(BaseCommand):
    help='Import Orders from Old System\'s CSV.'

    def handle(self, *args, **options):
        orderitems={}
        filename=os.path.abspath('%s/../../resources/jamberry_db_orderitem.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                print "\n", str({key:data for key, data in enumerate(row)})
                first=False
                continue
            if row[1] not in orderitems:
                orderitems[row[1]]=[]
            orderitems[row[1]].append(row)


        filename=os.path.abspath('%s/../../resources/jamberry_db_orders.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                print "\n", str({key:data for key, data in enumerate(row)})
                first=False
                continue

            try:
                o=Order.objects.get(pk=row[0])
            except Order.DoesNotExist:
                o=Order(pk=row[0])
            
            try:
                user=User.objects.get(pk=row[1])
            except User.DoesNotExist:
                #print 'User: ', str(row[1])
                continue

            contact=Contact.objects.filter(user=user)
            if len(contact)>0:
                contact=contact[0]
            else:
                contact=False
                
            if contact:
                ts=row[12].split('/')
                o.time_stamp='%s-%s-%s'%(ts[2], ts[1], ts[0])
                o.site=Site.objects.get(pk=1)
                o.sub_total=row[15].strip().lstrip('$').replace(',', '')
                o.shipping_cost=row[17].strip().lstrip('$').replace(',', '')
                o.tax=row[16].strip().lstrip('$').replace(',', '')
                o.total=row[19].strip().lstrip('$').replace(',', '')
                o.contact=contact

                country=Country.objects.get(printable_name=row[11])

                o.ship_street1=row[6]
                o.ship_street2=row[7]
                o.ship_city=row[8]
                o.ship_postal_code=row[9]
                o.ship_state=row[10]
                o.ship_country=country.iso2_code
                
                o.save()
                
                try:
                    OrderWebsite.objects.get(order=o, website=contact.user.website)
                except OrderWebsite.DoesNotExist:
                    OrderWebsite(order=o, website=contact.user.website).save()

                try:
                    wd=WholesaleDiscount.objects.get(pk=row[33], site=o.site)
                except:
                    wd=WholesaleDiscount(pk=row[33], slug=row[33], site=o.site)
                    wd.save()

                try:
                    OrderType.objects.get(order=o, type=wd)
                except OrderType.DoesNotExist:
                    OrderType(order=o, type=wd).save()

                try:
                    CommissionableOrder.objects.get(commission_to=contact.user, order=o, timestamp=o.time_stamp, remarks='self')
                except CommissionableOrder.DoesNotExist:
                    CommissionableOrder(commission_to=contact.user, order=o, timestamp=o.time_stamp, remarks='self').save()
                    pay_upline_commission(contact.user, o, o.time_stamp)
                    
def pay_upline_commission(whose_upline, order, ts):
    upline=whose_upline.upline
    for (counter, user) in enumerate(upline):
        CommissionableOrder(commission_to=user, order=order, remarks='l%d'%(counter+1), timestamp=ts).save()

