from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random
from decimal import *
from apps.compensation.models import *
#/src/crossmarket/apps/compensation/models.py
class Command(BaseCommand):
    help='Import Dates from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_earnings.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        dne=[]
            
        for row in data:
            if first:
                first=False
                print row
                continue
            
            period=Period.objects.get(pk=row[3].strip())
            
            try:
                user=User.objects.get(pk=row[1])
            except:
                if row[1] not in dne:
                    dne.append(row[1])
                #print "%s does not exists"%row[1]
                #print row
                continue

            
            try:
                e=Earning.objects.get(user=user, month=period.month, year=period.year, pk=row[1])
                continue
            except Earning.DoesNotExist:
                e=Earning(user=user, month=period.month, year=period.year, pk=row[1])

            e.earning_type=row[5].strip().lstrip('$').strip().replace(',', '')
            e.earning_basis=row[6].strip().lstrip('$').strip().replace(',', '')
            e.earning_percentage=row[7].strip().lstrip('$').strip().replace(',', '')
            if len(row[8].strip())>0:
                e.earning_max=row[8].strip().lstrip('$').strip().replace(',', '')
            if len(row[9].strip())>0:
                e.earning_amount=row[9].strip().lstrip('$').strip().replace(',', '')
            if len(row[10].strip())>0:
                e.sourceid=row[10]
            if len(row[11].strip())>0:
                e.source_orderid=row[11]
            e.desc=row[13]

            e.save()
            """
            try:
                e.save()
            except:
                print e
                print e.earning_type
                print e.earning_basis
                print e.earning_percentage
                print e.earning_max
                print e.earning_amount
                print e.sourceid
                print e.source_orderid
                print e.desc
                exit(0)
            """
        print dne
