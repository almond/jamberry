from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.account.models import Profile, UserWebsite, UserSponsor
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from l10n.models import Country

from product.models import Product, ProductImage, Price
from django.contrib.sites.models import Site
from django.template.defaultfilters import slugify
from urllib import urlretrieve

from product.models import Category
from django.contrib.sites.models import Site
from django.template.defaultfilters import slugify

class Command(BaseCommand):
    help='Import Categories from Old System\'s CSV.'

    def handle(self, *args, **options):
        try:
            products=Category.objects.get(pk=1)
        except Category.DoesNotExist:
            products=Category(id=1)

        products.site=Site.objects.get(pk=1)
        products.name='Product'#row[1].upper()
        products.slug='product'
        products.is_active=True #if row[2]=='-1' else False
        products.save()



        filename=os.path.abspath('%s/../../resources/jamberry_db_category.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                category=Category.objects.get(pk=row[0])
            except Category.DoesNotExist:
                category=Category(id=row[0])

            category.site=Site.objects.get(pk=1)
            category.name=row[1].upper()
            slug=slugify(row[1])

            try:
                tmp=Category.objects.get(slug=slug)
                if tmp!=category:
                    slug="%s-123"%slug
            except:
                pass
            category.slug=slug
            category.parent=products
            category.is_active=True if row[2]=='-1' else False
            category.save()
