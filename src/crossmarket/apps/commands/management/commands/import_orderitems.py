import os, csv, random
from l10n.models import Country

from django.core.management.base import BaseCommand, CommandError
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.conf import settings

from satchmo_store.shop.models import Order, OrderItem
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from product.models import Product


class Command(BaseCommand):
    help='Import Orders from Old System\'s CSV.'

    def handle(self, *args, **options):        
        filename=os.path.abspath('%s/../../resources/jamberry_db_orderitem.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                print "\n", str({key:data for key, data in enumerate(row)})
                first=False
                continue

            try:
                oi=OrderItem.objects.get(pk=row[0])
            except OrderItem.DoesNotExist:
                oi=OrderItem(pk=row[0])

            product_exists=True
            try:
                p=Product.objects.get(id=row[2])
            except:
                #print row[2] 
                product_exists=False
                
            order_exists=True
            try:
                o=Order.objects.get(pk=row[1])
            except:
                order_exists=False
            if product_exists and order_exists:
                oi.order=o
                oi.quantity=row[5].strip().lstrip('$').replace(',', '').lstrip('(').rstrip(')').lstrip('$')
                oi.unit_price=row[3].strip().lstrip('$').replace(',', '').lstrip('(').rstrip(')').lstrip('$')
                oi.product=p
                oi.unit_tax=0
                oi.tax=0
                oi.line_item_price=row[6].strip().lstrip('$').replace(',', '').lstrip('(').rstrip(')').lstrip('$')
                #oi.save()
                
                try:
                    oi.save()
                except:
                    print oi.quantity
                    print oi.unit_price
                    print oi.product.id
                    print oi.unit_tax
                    print oi.line_item_price
                    exit()
                
