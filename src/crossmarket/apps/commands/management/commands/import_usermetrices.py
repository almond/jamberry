from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random
from decimal import *
from apps.compensation.models import *
#/src/crossmarket/apps/compensation/models.py
class Command(BaseCommand):
    help='Import Dates from Old System\'s CSV.'

    def handle(self, *args, **options):
        fast_start={}
        filename=os.path.abspath('%s/../../resources/jamberry_db_faststart.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        
        
        for row in data:
            if first:
                first=False
                continue
            month_1=not (False if row[26]=='-1' else True)
            month_2=not (False if row[27]=='-1' else True)
            month_3=not (False if row[28]=='-1' else True)
            if month_1 or month_2 or month_3:
                fast_start[row[0]]={}
                if month_1:
                    if row[38][3:] not in  fast_start[row[0]]:
                        fast_start[row[0]][row[38][3:]]=[]
                    fast_start[row[0]][row[38][3:]].append(1)
                     
                if month_2:
                    if row[39][3:] not in  fast_start[row[0]]:
                        fast_start[row[0]][row[39][3:]]=[]

                    fast_start[row[0]][row[39][3:]].append(2)

                if month_3:
                    if row[40][3:] not in  fast_start[row[0]]:
                        fast_start[row[0]][row[40][3:]]=[]

                    fast_start[row[0]][row[40][3:]].append(3)

        commisions={}
        com=RankCommission.objects.all()
        for c in com:
            commisions[c.rank]=c
        print commisions
            

        filename=os.path.abspath('%s/../../resources/jamberry_db_metrics.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                print row
                continue
            
            period=Period.objects.get(pk=row[2].strip())
            
            try:
                user=User.objects.get(pk=row[1])
            except:
                #print "%s does not exists"%row[1]
                continue

            try:
                uh=UserHistory.objects.get(user=user, month=period.month, year=period.year)
            except UserHistory.DoesNotExist:
                uh=UserHistory(user=user, month=period.month, year=period.year)
            
                     
            if row[1] in fast_start and ('%s/%d'%(str(period.month).zfill(2), period.year)) in fast_start[row[1]]:
                bonuses=fast_start[row[1]][('%s/%d'%(str(period.month).zfill(2), period.year))]
                if 1 in bonuses:
                    uh.fs_1=100.00
                if 2 in bonuses:
                    uh.fs_2=100.00
                if 3 in bonuses:
                    uh.fs_2=100.00
            
            uh.earned_rank=Rank.objects.get(pk=row[11].strip())
            retail_sales=row[12].strip().lstrip('$').strip().replace(',', '')
            wholesale_sales=row[13].strip().lstrip('$').strip().replace(',', '')
            l1=row[16].strip().lstrip('$').strip().replace(',', '')
            l2=row[17].strip().lstrip('$').strip().replace(',', '')
            l3=row[18].strip().lstrip('$').strip().replace(',', '')
            l4=row[19].strip().lstrip('$').strip().replace(',', '')
            l5=row[20].strip().lstrip('$').strip().replace(',', '')
            
            pcv_amt=Decimal(row[4].strip().lstrip('$').strip().replace(',', ''))
            complan=commisions[uh.earned_rank]

            rc=Decimal(0.30)*Decimal(retail_sales)
            vb=Decimal(0) if pcv_amt <= 500 else (pcv_amt*Decimal(complan.volume_bonus_for_500_pcv))
            bs=Decimal(0) if pcv_amt <= 500 else (pcv_amt*Decimal(complan.business_supply_credit_for_300_pcv))


            l1=Decimal(l1)*Decimal(complan.l1)
            l2=Decimal(l2)*Decimal(complan.l2)
            l3=Decimal(l3)*Decimal(complan.l3)
            l4=Decimal(l4)*Decimal(complan.l4)
            l5=Decimal(l5)*Decimal(complan.l5)
            #16
            uh.retail_comm=rc #row[12].strip().lstrip('$').strip().replace(',', '')
            uh.volume_bonus=vb #row[13].strip().lstrip('$').strip().replace(',', '')

            uh.business_supply_credit=bs
            uh.commissions_on_downline=l1+l2+l3+l4+l5
            uh.pay_rank_bonus=0
            
            uh.prv=0
            uh.pcv=row[4].strip()
            uh.pcv_quater=row[5].strip()
            uh.gcv=row[10].strip()
            uh.l1_pcv=row[16].strip()
            uh.l2_pcv=row[17].strip()
            uh.l3_pcv=row[18].strip()
            uh.l4_pcv=row[19].strip()
            uh.l5_pcv=row[20].strip()
            
            uh.personally_enrolled=row[6].strip()
            uh.personally_enrolled_active=row[7].strip()

            uh.personally_enrolled_star=row[8].strip()
            #uh.wholsesale_sales=row[13].strip().lstrip('$').strip().replace(',', '')#row[8].strip()
            uh.wholsesale_onretailsales=row[14].strip().lstrip('$').strip().replace(',', '')#row[8].strip()
            uh.retail_diff=row[15].strip().lstrip('$').strip().replace(',', '')#row[8].strip()

            

            uh.save()
