#jamberry/src/crossmarket/apps/pricing/__init__.py
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.account.models import Profile, UserWebsite, UserSponsor
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from l10n.models import Country

from product.models import Product, ProductImage, Price, Category
#from product.models import ProductCategory

from django.contrib.sites.models import Site
from django.template.defaultfilters import slugify
from urllib import urlretrieve

class Command(BaseCommand):
    help='Import Products from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_products.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                product=Product.objects.get(pk=row[0])
            except Product.DoesNotExist:
                product=Product(id=row[0])
       
            product.site=Site.objects.get(pk=1)
            product.name=row[3]

            slug=slugify(row[3])
            try:
                tmp=Product.objects.get(slug=slug)
                if tmp!=product:
                    slug="%s-%s"%(slug, slug)
            except:
                pass
            product.slug=slug

            product.description=row[4]
            sku=row[1]
            try:
                tmp=Product.objects.get(sku=sku)
                if tmp!=product:
                    sku='%s-%s'%(sku, sku)
                    try:
                        tmp=Product.objects.get(sku=sku)
                        if tmp!=product:
                            sku='%s-%s'%(sku, row[1])
                            try:
                                tmp=Product.objects.get(sku=sku)
                                sku='%s-%s'%(sku, row[1])
                            except:
                                pass
                    except:
                        pass
            except:
                pass
            product.sku=sku


            product.save()

            if row[9]!='':
                image_full_url='http://www.jamberrynails.net/assets/images/products/%s'%(row[9])
                urlretrieve(image_full_url, os.path.join(settings.DIRNAME, 'static', 'images', '%s-%s'%('productimage-picture', row[9])))
                try:
                    product_img=ProductImage.objects.get(product=product)
                except ProductImage.DoesNotExist:
                    product_img=ProductImage(product=product)

                product_img.picture=os.path.join('images', '%s-%s'%('productimage-picture', row[9]))
                product_img.sort=1
                product_img.save()

            try:
                product_price=Price.objects.get(product=product)
            except Price.DoesNotExist:
                product_price=Price(product=product)
            
            product_price.price=row[12].lstrip('(').rstrip(')').lstrip('$')
            try:
                product_price.save()
            except:
                print product_price.price

            group=row[21]
            groups=group.split(',')
            for group in groups:
                if group.strip()!='':
                    try:
                        cat=Category.objects.get(pk=int(group))
                        product.category.add(cat)
                    except Category.DoesNotExist:
                        print 'Category %s does not exists'%(group)
            product.save()
            
            
