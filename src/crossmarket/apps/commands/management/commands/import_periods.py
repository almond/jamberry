from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random

from apps.compensation.models import Period
#/src/crossmarket/apps/compensation/models.py
class Command(BaseCommand):
    help='Import Periods from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_periods.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                p=Period.objects.get(pk=row[0])
            except Period.DoesNotExist:
                p=Period(id=row[0])

            p.begin=row[1]
            p.end=row[2]
            p.year=row[3]
            p.quater=row[4]
            p.month=row[5]
            p.save()
