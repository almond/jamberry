from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random

from apps.dashboard.models import CmsContent
class Command(BaseCommand):
    help='Import CMS Content from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_cms.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            
            try:
                cms=CmsContent.objects.get(pk=row[0])
            except CmsContent.DoesNotExist:
                cms=CmsContent(id=row[0])
            
            cms.slug=row[2]
            cms.description=row[3]
            cms.heading=row[4]
            cms.sub_heading=row[5]
            cms.text=row[6]
            cms.save()
            
