import os, csv, random
from l10n.models import Country

from django.core.management.base import BaseCommand, CommandError
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.conf import settings

from satchmo_store.shop.models import Order, OrderItem
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from apps.compensation.models import CommissionableOrder
from apps.dashboard.models import OrderWebsite, OrderType
from apps.pricing.models import WholesaleDiscount


class Command(BaseCommand):
    help='Import Orders from Old System\'s CSV.'

    def handle(self, *args, **options):
        user_details={}
        filename=os.path.abspath('%s/../../resources/jamberry_db_users.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue
            user_details[row[0]]=row

        filename=os.path.abspath('%s/../../resources/jamberry_db_orders.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                print "\n", str({key:data for key, data in enumerate(row)})
                first=False
                continue

            try:
                o=Order.objects.get(pk=row[0])
            except Order.DoesNotExist:
                o=Order(pk=row[0])
            
            try:
                user=User.objects.get(pk=row[1])
                continue
            except User.DoesNotExist:
                pass

            if row[1] in user_details:
                d=user_details[row[1]]
                try:
                    contact=Contact.objects.get(email=d[15], user=None)
                except Contact.DoesNotExist:
                    contact=Contact(email=d[15], user=None)

                sponsor=User.objects.get(pk=d[6])
                
            else:
                continue


            ts=row[12].split('/')
            contact.first_name=d[12]#user.first_name
            contact.last_name=d[13]#user.last_name
            contact.role=ContactRole.objects.get(name='Customer')
            contact.create_date='%s-%s-%s'%(ts[2], ts[1], ts[0])
            contact.save()

            if contact:

                ab=AddressBook.objects.filter(contact=contact)
                if len(ab)>0:
                    ab=ab[0]
                else:
                    ab=AddressBook(contact=contact)

                ab.description=''
                ab.addressee=contact.full_name#user.get_full_name()
                ab.street1=d[16]
                ab.street2=d[17]
                ab.state=d[19]
                ab.city=d[18]
                ab.postal_code=d[20]
                try:
                    country=Country.objects.get(printable_name=d[21])
                except Country.DoesNotExist:
                    print contact.id, d[21]
                    country=Country.objects.get(printable_name='United States')
                ab.country=Country.objects.get(printable_name=country)
                ab.is_default_shipping=True
                ab.is_default_billing=True
                try:
                    ab.save()
                except:
                    ab.postal_code



                o.time_stamp='%s-%s-%s'%(ts[2], ts[1], ts[0])
                o.site=Site.objects.get(pk=1)
                o.sub_total=row[15].strip().lstrip('$').replace(',', '')
                o.shipping_cost=row[17].strip().lstrip('$').replace(',', '')
                o.tax=row[16].strip().lstrip('$').replace(',', '')
                o.total=row[19].strip().lstrip('$').replace(',', '')
                o.contact=contact

                country=Country.objects.get(printable_name=row[11])

                o.ship_street1=row[6]
                o.ship_street2=row[7]
                o.ship_city=row[8]
                o.ship_postal_code=row[9]
                o.ship_state=row[10]
                o.ship_country=country.iso2_code
                
                o.save()
                
                print sponsor
                try:
                    OrderWebsite.objects.get(order=o, website=sponsor.website)
                except OrderWebsite.DoesNotExist:
                    OrderWebsite(order=o, website=sponsor.website).save()

                try:
                    wd=WholesaleDiscount.objects.get(pk=row[33], site=o.site)
                except:
                    wd=WholesaleDiscount(pk=row[33], slug=row[33], site=o.site)
                    wd.save()

                try:
                    OrderType.objects.get(order=o, type=wd)
                except OrderType.DoesNotExist:
                    OrderType(order=o, type=wd).save()

                try:
                    CommissionableOrder.objects.get(commission_to=sponsor, order=o, timestamp=o.time_stamp, remarks='web')
                except CommissionableOrder.DoesNotExist:
                    CommissionableOrder(commission_to=sponsor, order=o, timestamp=o.time_stamp, remarks='web').save()
                    pay_upline_commission(sponsor, o, o.time_stamp)
                    
def pay_upline_commission(whose_upline, order, ts):
    upline=whose_upline.upline
    for (counter, user) in enumerate(upline):
        CommissionableOrder(commission_to=user, order=order, remarks='l%d'%(counter+1), timestamp=ts).save()

