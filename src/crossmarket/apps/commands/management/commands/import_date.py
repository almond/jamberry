from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, csv, random

from apps.compensation.models import DateDetails
#/src/crossmarket/apps/compensation/models.py
class Command(BaseCommand):
    help='Import Dates from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_dates.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                date=DateDetails.objects.get(pk=row[0])
            except DateDetails.DoesNotExist:
                date=DateDetails(id=row[0])

            date.date=row[1]
            date.dw=row[2]
            date.dm=row[3]
            date.dy=row[4]
            date.day_name=row[5]
            date.day_abbr=row[6]
            date.wk=row[7]
            date.mo=row[8]
            date.month_name=row[9]
            date.month_abbr=row[10]
            date.qtr=row[11]
            date.qtr_abbr=row[12]
            date.yr=row[13]
            date.week_begin=row[14]
            date.week_end=row[15]
            date.period=row[16]
            date.publish_bonuses=row[17]
            date.autoship_week=row[18]
            date.save()
