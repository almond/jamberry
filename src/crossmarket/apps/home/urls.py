from django.conf.urls.defaults import patterns, include, url
from views import *
urlpatterns = patterns('',
    url(r'^redirect/$', redirect),
    url(r'^set-party-purchase/$', set_party_purchase, name='set_party_purchase'),
    url(r'^parties/$', parties),
    url(r'^hostess-portal/(?P<code>[\{\}\w-]+)/$', 'apps.events.views.event_hostessportal', name='event_hostessportal'),
    url(r'^$', home),
  
)
