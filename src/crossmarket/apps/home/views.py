from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.http import Http404
from apps.events.models import Event
from django.conf import settings
import datetime

def parties(request):
    if not request.referred_by:
        raise Http404
    else:
        return render_to_response('parties.html', {}, context_instance=RequestContext(request))

def set_party_purchase(request):
    party=request.GET.get('party', -1)
    today=datetime.datetime.today()
    if Event.objects.filter(status=3, pk=party, starts_at__gte=today).exists():
        request.session[settings.FRONTEND_PARTY_SESSION_SELECT]=party
        return HttpResponseRedirect('/shop/')
    
    return render_to_response('unavailable.html', {}, context_instance=RequestContext(request))

def home(request):
    return render_to_response('index.html', {}, context_instance=RequestContext(request))

def redirect(request):
    if request.user.is_authenticated() and request.user.is_staff:
        return HttpResponseRedirect(reverse('apps.dashboard.views.section'))
    elif request.user.is_authenticated():
        return HttpResponseRedirect(reverse('apps.manage.views.dashboard'))
    else:
        return HttpResponseRedirect('/')

def dashboard(request):
    return render_to_response('dashboard.html', {}, context_instance=RequestContext(request))
