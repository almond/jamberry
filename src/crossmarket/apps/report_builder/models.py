from django.db import models
from apps.account.models import Profile
from django.contrib.auth.models import User
from django.utils import simplejson as json

class Filter(models.Model):
    user=models.ForeignKey(User)
    name = models.CharField(max_length=40, blank=False, null=False)
    def properties(self):
        return FilterDetail.objects.filter(filter=self)

class FilterDetail(models.Model):
    filter=models.ForeignKey(Filter)
    key=models.CharField(max_length=40, blank=False, null=False)
    value=models.TextField()

# Create your models here.
class Report(models.Model):
    """
    Store filtered list on a per user basis so that they can select this pre-fabricated ones instead of entering 
    one user address at a time. 
    
    serialized_filter has to take the following form:
    
    Filters
           |-[op] # operation that'll be done on the contained filter [equals, more_than, less_than, range]
               |-[numbered_index]
                               |-[keyHigh]
                                          |-[value]
                                          |-[type]
                               |-[keyLow] # This one's optional and only used on comparison and range filters
                                         |-[value]
                                         |-[type]
    
    """
    profile = models.ForeignKey(Profile)
    name = models.CharField(max_length=40, blank=False, null=False)
    is_corporate = models.BooleanField(default=False, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    serialized_filter = models.TextField(blank=True, null=True) # Store a json string with all of the query parameters        
    
    
    def save(self, *args, **kwargs):
        """
        Serialize everything!! All of the data passed in the list 'report_parameters' should be serialized to json before storing.
        The format is outlined in the comment above
        """
        self.serialized_filter = json.dumps(self.report_parameters, ensure_ascii=False)
        super(Report, self).save(*args, **kwargs)
        
