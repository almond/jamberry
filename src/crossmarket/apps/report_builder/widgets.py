from django.forms import widgets
from django.utils.translation import ugettext_lazy as _

"""
Multivalue widgets for the mail_filter form
"""

class RangeWidget(widgets.MultiWidget):
    def __init__(self, attrs=None, range_type="date"):
        """
        Create two datefields or decimalfields for Django that'll display as only one widget. This to make form rendering and adding new
        filters a breeze. Kewl huh?
        """
        if range_type == "date": # I'd rather simulate a switch or something with range_type but I'm in a hurry 
            _widgets = (
                widgets.DateInput(attrs=attrs),
                widgets.DateInput(attrs=attrs),
            )
        elif range_type == "decimal":
            _widgets = (
                widgets.TextInput(attrs=attrs),
                widgets.TextInput(attrs=attrs),
            )
            
        super(RangeWidget, self).__init__(_widgets, attrs)
        
    
    def decompress(self, value):
        """
        This is called when the field has to be pre-populated with data from the DB. I don't think that it'll happen
        in this case but it's better I do this in case it does so it doesn't break.
        """
        if value:
            return [value.data_from, value.data_to]
        else: 
            return []
            
        
    def value_from_datadict(self, data, files, name):
        """
        This is called after submission. This method then takes care of taking every value entered
        in the different widgets and concatenating / processing it in a way that it only returns
        one value. I'll separate the dates with a & as in the URL syntax scheme
        """
        # Extract data from POST or GET dict passed as data parameter
        from_data = data.get(name + "_0", None)
        to_data = data.get(name + "_1", None)
        
        range_string = "%s&%s" % (from_data, to_data)
        return range_string
    
    
    def format_output(self, rendered_widgets):
        """
        This method takes care of how to render the widgets in the front end. What we are gonna do is 
        add a 'to' between each field. They depend on each other and what not
        """
        return u"from" + u" " + u"".join(rendered_widgets[0]) + u" " + u"to" + u" " + u"".join(rendered_widgets[1])