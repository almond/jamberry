from apps.account.models import Profile
from apps.report_builder.forms import ReportForm
from apps.report_builder.models import Report
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render_to_response
from django.template.context import RequestContext
import datetime
from django.http import HttpResponse, HttpResponseRedirect

from apps.events.models import Event
from apps.compensation.models import Rank, CommissionableOrder, UserHistory
from models import Filter, FilterDetail
import base64
from satchmo_store.contact.models import Contact

"""Information about all the filters"""
filter_types={
    1:{'label':'Customers', 'key':'customer', 'width':200, 'values':[{'id':1, 'name':'Ordered in last 30 days'}, {'id':2, 'name':'Ordered in last 6 months'}, {'id':3, 'name':'New customers in last 30 days'}, {'id':4, 'name':'New customers in last 6 months'}, {'id':5, 'name':'Promoted within the last 30 days'}]},
    2:{'label':'Hostesses', 'key':'hostess', 'width':210, 'values':[{'id':1, 'name':'Hosted within the past 3 months'}, {'id':2, 'name':'Hosted within the past 6 months'}]},
    3:{'label':'Type', 'key':'type', 'width':150, 'values':Rank.objects.filter(id__gte=2)},
    4:{'label':'Genealogy', 'key':'gene', 'width':150, 'values':[{'id':1, 'name':'Frontline'}, {'id':2, 'name':'Downline'}, {'id':3, 'name':'Retails'}, {'id':4, 'name':'All'}]},
    5:{'label':'Status', 'key':'status', 'width':80, 'values':[{'id':1, 'name':'Active'}, {'id':2, 'name':'In Active'}]}    
}


def find_filter_and_people(data, user):
    filter_on={}
    people={'c':[], 'r':[]}
    """Filter and show data only of the get parameter is set, this makes the first page load faster"""
    if data.get('filter', False)=='true':
        for filter, details in filter_types.items():
            filter_on_value=data.get('f[%s]'%details['key'], None)
            if filter_on_value:
                filter_on[details['key']]=filter_on_value

        if 'gene' not in filter_on or int(filter_on['gene'])<1 or int(filter_on['gene'])>len(filter_types[4]['values']):
            filter_on['gene']='1'

        if filter_on['gene']=='1':
            people['c']=user.frontline
        elif filter_on['gene']=='2':
            people['c']=user.downline
        elif filter_on['gene']=='3':
            people['r']=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None)
        elif filter_on['gene']=='4':
            people['r']=CommissionableOrder.objects.filter(commission_to=user, remarks='web', order__contact__user=None)
            people['c']=user.downline

        people['c']=[c.pk for c in people['c']]
        people['r']=[r.order.contact.pk for r in people['r']]

    return filter_on, people

 

def get_filtered_people(filter_on, people, user):
    # Just to leave you a note Aman: This is impressive! Really nice job! Learned a lot.
    now=datetime.datetime.now()
    for key, f in filter_types.items():
        if f['key'] in filter_on:
            try:
                id=int(filter_on.get(f['key']))
            except:
                continue

            if f['key']=='customer':
                if id==1:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=30)
                    people['c']=CommissionableOrder.objects.values_list('commission_to__id', flat=True).filter(commission_to__id__in=people['c'], remarks='self', timestamp__gte=start)
                    people['r']=CommissionableOrder.objects.values_list('commission_to__id', flat=True).filter(order__contact__id__in=people['r'], commission_to__id=user.id, remarks='web', timestamp__gte=start)
                elif id==2:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=60)
                    people['c']=CommissionableOrder.objects.values_list('commission_to__id', flat=True).filter(commission_to__id__in=people['c'], remarks='self', timestamp__gte=start)
                    people['r']=CommissionableOrder.objects.values_list('commission_to__id', flat=True).filter(order__contact__id__in=people['r'], commission_to__id=user.id, remarks='web', timestamp__gte=start)

                elif id==3:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=30)
                    people['c']=User.objects.values_list('id', flat=True).filter(id__in=people['c'], date_joined__gte=start)
                    people['r']=Contact.objects.values_list('id', flat=True).filter(id__in=people['r'], create_date__gte=start)

                elif id==4:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=180)
                    people['c']=User.objects.values_list('id', flat=True).filter(id__in=people['c'], date_joined__gte=start)
                    people['r']=Contact.objects.values_list('id', flat=True).filter(id__in=people['r'], create_date__gte=start)
                
                elif id==5:
                    # Promoted in the last 30 days
                    now = datetime.date.today()
                    month_ago = now - datetime.timedelta(days=30)
                    people['c'] = User.objects.values_list('id', flat=True).filter(id__in=people['c'], userrank__last_updated__gte=month_ago)
                    people['r'] = Contact.objects.values_list('id', flat=True).filter(id__in=people['r'], user__userrank__last_updated__gte=month_ago)


            if f['key']=='hostess':
                if id==1:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=90)
                elif id==2:
                    start=datetime.datetime(year=now.year, month=now.month, day=now.day)-datetime.timedelta(days=180)
                people['c']=Event.objects.values_list('owner__id', flat=True).filter(owner__id__in=people['c'], starts_at__gte=start)

            if f['key']=='type':
                if id!=2:
                    people['c']=UserHistory.objects.values_list('user__id', flat=True).filter(earned_rank__id=id, user__id__in=people['c'], month=now.month, year=now.year)

            if f['key']=='status':
                people['c']=User.objects.values_list('id', flat=True).filter(id__in=people['c'], is_active=True if id==1 else False)

    people['c']=User.objects.filter(id__in=people['c'])
    people['r']=Contact.objects.filter(id__in=people['r'])

    return people

def get_selected_people(data):
    only={'c':data.get('o[c]', None), 'r':data.get('o[r]', None)}
    if only['c']:
        try:
            only['c']=base64.decodestring(only['c'])
            only['c']=only['c'].split(',')
        except:
            only['c']=None
    if only['r']:
        try:
            only['r']=base64.decodestring(only['r'])
            only['r']=only['r'].split(',')
        except:
            only['r']=None
    return only

def save_filter(filter_on, only, filter_name, user):
    if filter_name and (filter_on or only['c'] or only['r']):
        f=Filter(user=user, name=filter_name)
        f.save()
        for key, value in filter_on.items():
            FilterDetail(key="f[%s]"%key, value=value, filter=f).save()
        if only['c']:
            FilterDetail(key='o[c]', value=base64.encodestring(','.join(only['c'])), filter=f).save()
        if only['r']:
            FilterDetail(key='o[r]', value=base64.encodestring(','.join(only['r'])), filter=f).save()

def del_filter(id, user):    
    if id:
        try:
            Filter.objects.get(user=user, id=id).delete()
        except Filter.DoesNotExist:
            pass

            
@login_required
def index(request):
    if request.GET.get('delete_filter', None):
        del_filter(request.GET.get('delete_filter'), request.user)
        return HttpResponseRedirect('.')

    filter_on, people=find_filter_and_people(request.GET, request.user)
    only=get_selected_people(request.GET)
    

    if request.GET.get('save', False)=='true' and request.GET.get('name', None):
        save_filter(filter_on, only, request.GET.get('name'), request.user)
        return HttpResponse('true')

    if request.GET.get('filter', False)=='true':
        people=get_filtered_people(filter_on, people, request.user)

    filters=Filter.objects.filter(user=request.user)
    return render_to_response("report_builder/index.html", {"filter_types":filter_types, "filter_on":filter_on, "people":people, "only":only, "filters":filters}, context_instance=RequestContext(request))


    

def create(request):
    """
    Create a new report. Data should be passed as a POST and then it'll be serialized to JSON in the model by overriding the save method. 
    This 'blob' is stored in the model field 'serialized_filter' and the model itself will have a custom manager that returns a queryset of the 
    filtered users.
    """
    if request.method == "POST":
        report_form = ReportForm(request.POST, request.FILES)
        if report_form.is_valid():
            report_model = Report()
            report_model.profile = Profile.objects.get_or_create(user=request.user)
            if request.user.is_staff:
                report_model.is_corporate = True
            report_model.name = report_form.cleaned_data["report_name"]
            report_model.report_parameters = report_form.cleaned_data
            report_model.save()
    else:
        report_form = ReportForm()
    
    return render_to_response("report_builder/create.html", {'report_form': report_form}, context_instance=RequestContext(request))
            
