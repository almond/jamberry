from django import forms
from django.utils.translation import ugettext_lazy as _
from apps.compensation.models import Rank


TIME_SPAN_OPTIONS = [
    (0, "------"),
    (1, "month"),
    (3, "2 months"),
    (6, "6 months"),
    (12, "year"),
]


def __get_rank_options(ranks):
    options = [(0, "------")]
    for rank in ranks:
        options.append((rank.pk, rank.name))
    return options


RANK_OPTIONS = __get_rank_options(Rank.objects.all())


class ReportForm(forms.Form):
    """
    Form written to handle all of the filters for the mail_filter builder.
    Will receive data as normal. It'll be serialized in the model
    nicely serialized to json. 
    
    The format that the json string has to take is written down and defined in 
    this apps models.py
    
    TODO: Allow the superadmin the ability to create filters and stuff. Defining an operation, over what to operate
    and saving the results. 
    
    TODO: Port to formsets if necessary
    
    JSON should make everything easy to cache.
    
    FAQ: 
        How do I add more filters?
            You should let this job to professionals lest risk death. If you'd rather still try it is just thing of adding the fields here and
            defining how they'd work on your format_data method
    """
    # Start with filter METADATA
    report_name = forms.CharField(max_length=100, required=False, label=_(u"Report name"))
    
    # Start with the user information part
    user_first_name = forms.CharField(max_length=100, required=False, label=_(u"First Name Matches"))
    user_last_name = forms.CharField(max_length=100, required=False, label=_(u"Last Name Matches"))
    user_username = forms.CharField(max_length=100, required=False, label=_(u"Username Matches"))
    user_email = forms.CharField(max_length=100, required=False, label=_(u"Email Matches"))
    
    # Follow with order information part
    order_pvc_range_from = forms.DecimalField(max_digits=10, decimal_places=6, required=False, label=_(u"PCV Range From"))
    order_pcv_range_to = forms.DecimalField(max_digits=10, decimal_places=6, required=False, label=_(u"PCV Range To"))
    order_amount_from = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label=_(u"Order Amount From"))
    order_amount_to = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label=_(u"Order Amount To"))
    order_ordered_time_span = forms.IntegerField(widget=forms.Select(choices=TIME_SPAN_OPTIONS), required=False, label=_(u"Ordered In The Last"))
    order_shipped_to = forms.CharField(max_length=40, required=False, label=_(u"Shipped To"))
    
    # Now contact related stuff
    contact_location = forms.CharField(max_length=60, required=False, label=_(u"City or State"))
    contact_rank = forms.CharField(max_length=100, required=False, widget=forms.Select(choices=RANK_OPTIONS), label=_(u"Contact Rank"))
    
    # Now event related filters
    event_hosted_time_span = forms.IntegerField(widget=forms.Select(choices=TIME_SPAN_OPTIONS), required=False, label=_(u"Event Hosted In The Last"))
    event_location = forms.CharField(max_length=60, required=False, label=_("Event Location"))