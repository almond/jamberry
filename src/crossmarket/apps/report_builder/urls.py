from django.conf.urls.defaults import patterns, url

from apps.report_builder.views import create, index

urlpatterns = patterns('',
    url(r'^$', index, name="report_builder_index"),
    url(r'^create/', create, name="report_builder_create"),
)