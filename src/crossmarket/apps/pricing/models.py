from django.db import models
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from product.models import Category

from django.contrib.contenttypes import generic

class WholesaleDiscount(models.Model):
    site=models.ForeignKey(Site, verbose_name=_('site'))
    description=models.CharField(_("Description"), max_length=256)
    slug=models.CharField(_("Discount Type"), max_length=64, unique=True, help_text=_("Slug"))
    active=models.BooleanField(_("Active"))
    percentage=models.DecimalField(_("Discount Percentage"), decimal_places=2, max_digits=5, blank=True, null=True, help_text=_("Percents are given in whole numbers, and can be up to 100%."))
    show_in_frontend=models.BooleanField(default=False)
    def __unicode__(self):
        return "%s (%s)"%(self.slug, self.description)

class Offer(models.Model):
    site=models.ForeignKey(Site, verbose_name=_('site'))
    description=models.CharField(_("Description"), max_length=256)
    active=models.BooleanField(_("Active"))
    buy=models.IntegerField()
    free=models.IntegerField()
    def __unicode__(self):
        return "%s"%(self.description)
    



"""
Apply Offer Discounts
"""
"""
from decimal import Decimal
from product.models import Discount
from product import signals as product_signals
from satchmo_store.shop.models import OrderItem

def apply_offer(cls, discounted, amount):
    work = {}
    for lid, price in discounted.items():
        quantity_discount = OrderItem.objects.get(pk=lid).quantity * amount
        if price > quantity_discount:
            work[lid] = quantity_discount
        else:
            work[lid] = price
    return work

# Bind our custom classmethod to ``Discount``
Discount.apply_offer = classmethod(apply_offer)
"""
