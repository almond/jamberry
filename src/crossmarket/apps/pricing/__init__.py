from product.signals import satchmo_price_query
from product.prices import PriceAdjustment
from product.models import Category
from django.conf import settings
from decimal import *
def get_price(sender, slug=None, discountable=None, adjustment=None, **kwargs):
    from threaded_multihost import threadlocals 
    request=threadlocals.get_current_request() 
    product=adjustment.product
    product_category=Category.objects.get(slug=settings.MAIN_PRODUCT_CATEGORY_SLUG)

    if product.main_category and product.main_category.parent and product.main_category.parent==product_category:
        if settings.WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
            if request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY].percentage:
                item_price=product.price_set.get(quantity=1)
                adjustment+=PriceAdjustment('tieredpricing', settings.WHOLESALE_DISCOUNT_TEXT, (request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY].percentage/100)*item_price.price)


    price=Decimal(15.00)
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
        price=price*(100-request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY].percentage)/100

    if settings.BSC_AMT_SESSION_SLUG in request.session and product.slug==settings.BSC_PRODUCT_SLUG:
        adjustment+=PriceAdjustment('tieredpricing', 'BSC Price', request.session[settings.BSC_AMT_SESSION_SLUG])

    if settings.PC_AMT_SESSION_SLUG in request.session and product.slug==settings.PC_PRODUCT_SLUG:
        adjustment+=PriceAdjustment('tieredpricing', 'PC Price', request.session[settings.PC_AMT_SESSION_SLUG]*price)

    if settings.OFFER_AMT_SESSION_SLUG in request.session and product.slug==settings.OFFER_PRODUCT_SLUG:
        adjustment+=PriceAdjustment('tieredpricing', 'PC Price', request.session[settings.OFFER_AMT_SESSION_SLUG]*price)

satchmo_price_query.connect(get_price)
