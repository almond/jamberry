from django.db import models
from django.contrib.auth.models import User

class Notice(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    type = models.CharField(max_length=10, choices=(('info', 'Information'), ('warning', 'Warning'), ('error', 'Error'), ('success', 'Success')), default='info' )
    location = models.CharField(max_length=10, choices=(('ud', 'User Dashboard'), ), default='ud' )
    specific_users=models.ManyToManyField(User, blank=True)

    active=models.BooleanField()
    def __unicode__(self):
        return self.title
