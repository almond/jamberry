from django import template
register = template.Library()

from apps.utils import get_items_in_cart
import math

@register.tag
def cart_details(parser, token):
    try:
        tag_name, tmp, variable_name, variable_totals=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag is user improperly." % token.contents.split()[0])
    return CartItems(variable_name, variable_totals)

class CartItems(template.Node):
    def __init__(self, variable_name, variable_totals):
        self.variable_name=variable_name
        self.variable_totals=variable_totals

    def render(self, context):
        context[self.variable_name]=get_items_in_cart(context['request'])
        
        total_price=0.00
        total_quantity=0
        for i, value in get_items_in_cart(context['request'], True).items():
            total_price=total_price+(float(value['unit_price'])*float(value['quantity']))
            total_quantity=total_quantity+(int(value['quantity']))

        context[self.variable_totals]={'quantity':total_quantity, 'price':total_price}

        return ''

@register.filter
def getitem(item, string):
    return item.get(string, '')

@register.filter
def get_item_total(self, quantity):
    return float(self) * float(quantity)
