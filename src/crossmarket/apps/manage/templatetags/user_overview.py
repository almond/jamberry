from django import template
register = template.Library()

from apps.account.utils import _get_user_overview
import datetime

@register.tag
def user_overview(parser, token):
    try:
        tag_name, tmp, variable_name=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag is used improperly." % token.contents.split()[0])
    return UserOverview(variable_name)

class UserOverview(template.Node):
    def __init__(self, variable_name):
        self.variable_name=variable_name

    def render(self, context):
        date=datetime.datetime.now()
        context[self.variable_name]=_get_user_overview(context['request'].user, date.day, date.month, date.year)
        return ''


@register.filter
def getitem(item, string):
    return item.get(string, '')
