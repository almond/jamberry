from apps.account.models import UserWebsite, UserNetwork, Profile, \
    UserSponsor, Propay
from apps.account.utils import _get_downline_rank_and_volume_for_month
from apps.archive.models import * #@UnusedWildImport
from apps.compensation.models import * #@UnusedWildImport
from apps.manage.forms import * #@UnusedWildImport
from apps.notice.models import Notice
from apps.pricing.models import WholesaleDiscount, Offer
from apps.tools.forms import * #@UnusedWildImport
from apps.tools.models import * #@UnusedWildImport
from apps.utils import ajax_render, get_items_in_cart
from django import http
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages import constants, get_messages
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.core.xheaders import populate_xheaders
from django.db.models import Q
from django.forms.models import inlineformset_factory, modelformset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template, RequestContext, Context
from django.template.loader import get_template, select_template
from django.utils import simplejson as json
from livesettings import config_value
from product.models import Category, Price
from product.signals import index_prerender
from product.utils import find_best_auto_discount
from satchmo_store.contact.models import Contact, AddressBook
from satchmo_store.shop.models import Cart, NullCart, CartItem
from satchmo_store.shop.signals import satchmo_cart_details_query, \
    satchmo_cart_add_complete
from satchmo_utils.views import bad_or_missing
from shipping.modules.tiered.models import Carrier
from apps.events.forms import AttendeeForm, EventForm
from apps.events.models import EventAttendeeCart, EventAttendeeCartDetail, Event
from django.utils import simplejson
from apps.dashboard.templatetags.party_utils import party_order_details as party_order_info
from django.views.decorators.cache import cache_control, never_cache
from product.templatetags.satchmo_discounts import discount_price

import logging
import math
import base64


@login_required
def redeem_hostess_rewards(request, party_id):
    #no commissions on this sale

    try:
        party=Event.objects.get(pk=party_id, owner=request.user, redeemed_hostess_reward=False)
    except Event.DoesNotExist:
        raise Http404
    reward=party.hostess_rewards()
    if not reward:
        return render_to_response('manage/no-rewards.html', {}, context_instance=RequestContext(request))
    
    category = Category.objects.get_by_site(slug=settings.MAIN_PRODUCT_CATEGORY_SLUG)
    products = Product.objects.by_site(active=True, category__parent=category).distinct()
    product_list = list(products)
    sale = find_best_auto_discount(product_list)

    products = json.dumps({p.pk: {'sku': p.sku, 'slug': p.slug, 'name': p.translated_name(), 'price': float(discount_price(p, sale))} for p in products})
    return render_to_response('manage/redeem-hostess-reward.html', {'reward':reward, 'products':products, 'sale':sale}, context_instance=RequestContext(request))



@login_required
def get_cart_total_amt(request):
    cart = Cart.objects.from_request(request=request, create=True)
    items = cart.cartitem_set.all()
    for i in items:
        if i.product.slug in settings.DISC_PRODUCT_SLUGS:
            i.delete()

    return HttpResponse("%.2f" % cart.total)

@login_required
def offerable_amount_in_cart(request):
    cart = Cart.objects.from_request(request=request, create=True)
    items = cart.cartitem_set.all()
    amt = Decimal(0.00)
    for i in items:
        if i.product.slug in settings.DISC_PRODUCT_SLUGS:
            i.delete()
        else:
            if i.product.slug not in settings.OFFERLESS_PRODUCTS_SLUG:
                amt += (i.quantity*i.product.unit_price)

    return HttpResponse("%.2f" % amt)


@login_required
def get_cart_total_item(request):
    cart = Cart.objects.from_request(request=request, create=True)
    items = cart.cartitem_set.all()
    count = Decimal(0)
    for i in items:
        if i.product.slug in settings.DISC_PRODUCT_SLUGS:
            i.delete()
        else:
            count = count + i.quantity
    return HttpResponse("%.0f" % count)

@login_required
def offerable_items_in_cart(request):
    cart = Cart.objects.from_request(request=request, create=True)
    items = cart.cartitem_set.all()
    count = Decimal(0)
    for i in items:
        if i.product.slug in settings.DISC_PRODUCT_SLUGS:
            i.delete()
        else:
            if i.product.slug not in settings.OFFERLESS_PRODUCTS_SLUG:
                count = count + i.quantity
    return HttpResponse("%.0f" % count)

@login_required
def party_order_details(request, event_id, order_id):
    try:
        po = PartyOrder.objects.get(order=order_id, party=event_id, party__owner=request.user)
    except PartyOrder.DoesNotExist:
        raise Http404

    return render_to_response('manage/list-party-order.html', {'porder': po.details, 'order': po.order, 'direct_shippers':po.direct_shippers},
        context_instance=RequestContext(request))


@login_required
def user_propay_create(request):
    if request.method == 'POST':
        form = PropayForm(request.POST, request=request)
        if form.is_valid():
            pass
    else:
        form = PropayForm()
    return render_to_response('manage/propay-create.html', {'form': form}, context_instance=RequestContext(request))


def set_bsc(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('amount', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'},
                'request': {'amount': request.POST.get('amount')}}
        return HttpResponse(ajax_render(data))
    elif request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug != settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        data = {'status': {'success': False, 'reason': 'BSC can be applied to Marketing Materials Only.'},
                'request': {'amount': request.POST.get('amount')}}
        return HttpResponse(ajax_render(data))
    else:
        amount = Decimal(request.POST.get('amount'))
        if amount > request.user.bsc:
            data = {'status': {'success': False, 'reason': 'Available BSC is less.'},
                    'request': {'amount': request.POST.get('amount')}}
            return HttpResponse(ajax_render(data))
        else:
            cart = Cart.objects.from_request(request=request, create=True)

            items = cart.cartitem_set.all()
            for i in items:
                if i.product.slug == settings.BSC_PRODUCT_SLUG:
                    #i.quantity=0
                    i.delete()

            if amount > cart.total:
                data = {'status': {'success': False, 'reason': 'Cart total is only $%.2f.' % (cart.total)},
                        'request': {'amount': request.POST.get('amount')}}
                return HttpResponse(ajax_render(data))

            request.session[settings.BSC_AMT_SESSION_SLUG] = amount

            details = []
            formdata = request.POST

            quantity = 1
            product = Product.objects.get(slug=settings.BSC_PRODUCT_SLUG)
            add_prod_to_cart(cart, product, quantity, details, request, formdata)

            data = {'status': {'success': True}, 'request': {'amount': request.POST.get('amount')}}
            return HttpResponse(ajax_render(data))


def set_pc(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('amount', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'},
                'request': {'amount': request.POST.get('amount')}}
        return HttpResponse(ajax_render(data))
    elif request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        data = {'status': {'success': False, 'reason': 'PC cannnot be applied to Marketing Materials.'},
                'request': {'amount': request.POST.get('amount')}}
        return HttpResponse(ajax_render(data))
    else:
        amount = Decimal(request.POST.get('amount'))
        if amount > request.user.pc:
            data = {'status': {'success': False, 'reason': 'Available PC is less.'},
                    'request': {'amount': request.POST.get('amount')}}
            return HttpResponse(ajax_render(data))
        else:
            cart = Cart.objects.from_request(request=request, create=True)

            in_cart = offerable_items_in_cart(request)
            if amount > in_cart:
                data = {'status': {'success': False, 'reason': 'Cart only has %.0f creditable items.' % (in_cart)},
                        'request': {'amount': request.POST.get('amount')}}
                return HttpResponse(ajax_render(data))

            request.session[settings.PC_AMT_SESSION_SLUG] = amount

            details = []
            formdata = request.POST

            quantity = 1
            try:
                product = Product.objects.get(slug=settings.PC_PRODUCT_SLUG)
            except Product.DoesNotExist:
                product = Product(slug=settings.PC_PRODUCT_SLUG)
                product.description = 'Product Credit'
                product.site = Site.objects.get(pk=1)
                product.name = 'Product Credit'
                product.sku = settings.PC_PRODUCT_SLUG
                product.save()
                product_price = Price(product=product)
                product_price.price = 0.00
                product_price.save()

            add_prod_to_cart(cart, product, quantity, details, request, formdata)

            data = {'status': {'success': True}, 'request': {'amount': request.POST.get('amount')}}
            return HttpResponse(ajax_render(data))


def set_offer(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('offer', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'},
                'request': {'offer': request.POST.get('offer')}}
        return HttpResponse(ajax_render(data))
    elif request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        data = {'status': {'success': False, 'reason': 'Offers cannnot be applied to Marketing Materials.'},
                'request': {'offer': request.POST.get('offer')}}
        return HttpResponse(ajax_render(data))
    else:
        offer = Decimal(request.POST.get('offer'))
        try:
            offer = Offer.objects.get(pk=offer, active=True)
        except Offer.DoesNotExist:
            data = {'status': {'success': False, 'reason': 'Invalid offer applied.'},
                    'request': {'offer': request.POST.get('offer')}}
            return HttpResponse(ajax_render(data))

        cart = Cart.objects.from_request(request=request, create=True)
        items = cart.cartitem_set.all()
        total_quantity = 0
        for i in items:
            if i.product.slug in settings.DISC_PRODUCT_SLUGS:
                i.delete()
            else:
                total_quantity = total_quantity + i.quantity

        min_required = offer.buy + offer.free
        in_hand = total_quantity % min_required
        offerable_set = math.floor(total_quantity / min_required)

        if in_hand == offer.buy:
            items[0].quantity = items[0].quantity + (offer.free)
            items[0].save()
            offerable_set = offerable_set + 1

        num_discounts = offerable_set * offer.free
        if num_discounts > 0:
            request.session[settings.OFFER_AMT_SESSION_SLUG] = Decimal(num_discounts)

            try:
                product = Product.objects.get(slug=settings.OFFER_PRODUCT_SLUG)
            except Product.DoesNotExist:
                product = Product(slug=settings.OFFER_PRODUCT_SLUG)
                product.description = 'Offer'
                product.site = Site.objects.get(pk=1)
                product.name = 'Offer'
                product.sku = settings.OFFER_PRODUCT_SLUG
                product.save()
                product_price = Price(product=product)
                product_price.price = 0.00
                product_price.save()

            quantity = 1
            details = []
            formdata = request.POST
            add_prod_to_cart(cart, product, quantity, details, request, formdata)


        data = {'status': {'success': True}, 'request': {'offer': offer.pk}}
        return HttpResponse(ajax_render(data))

def get_order_details_from_request(post_arr):
    order_details = {}
    total_orders = {}
    purchasers = post_arr.getlist('purchasers[]')
    direct_ship = post_arr.getlist('direct_ship[]')

    for p in purchasers:
        if p not in order_details:
            order_details[p] = {}
        products = post_arr.getlist('products_%s[]' % p, )
        for product in products:
            order_details[p][product] = post_arr.get('quantity_%s_%s' % (p, product), '0')
            if product not in total_orders:
                total_orders[product] = int(0)
            total_orders[product] = total_orders[product] + int(order_details[p][product])
    return purchasers, order_details, direct_ship, total_orders

def store_party_order_details(event, order_details, direct_ship_details):
    attendees=[]
    for att_id, details in order_details.items():
        attendees.append(att_id)
        attendee_cart=EventAttendeeCart.objects.filter(event=event, attendee__id=att_id, active=True)
        if len(attendee_cart)==0:
            attendee_cart=EventAttendeeCart(event=event, attendee=Attendee.objects.get(pk=att_id), active=True, direct_ship=False)
        else:
            if len(attendee_cart)>1:
                EventAttendeeCart.objects.filter(~Q(pk=attendee_cart[0].pk), event=event, attendee__id=att_id, active=True).delete()

            attendee_cart=attendee_cart[0]
            attendee_cart.direct_ship=False


        if att_id in direct_ship_details:
            attendee_cart.direct_ship=True

        attendee_cart.save()

        attendee_cart.details().delete()
        for prod_id, quant in details.items():
            EventAttendeeCartDetail(attendee_cart=attendee_cart, quantity=quant, product=Product.objects.get(pk=prod_id)).save()

    EventAttendeeCart.objects.filter(~Q(attendee__id__in=attendees), event=event).delete() #if there are people with older carts delete 'em
        
def get_party_order_details(event):
    #Will return as str so that it matches the list generated by post variables
    order_details={}
    direct_ship=[]
    attendee_carts=EventAttendeeCart.objects.filter(event=event, active=True)
    #print attendee_carts
    for attendee_cart in attendee_carts:
        if attendee_cart.direct_ship:
            direct_ship.append(str(attendee_cart.attendee.id))
        order_details[str(attendee_cart.attendee.id)]={}
        for detail in attendee_cart.details():
            order_details[str(attendee_cart.attendee.id)][str(detail.product.id)]=str(detail.quantity)
    
    return order_details, direct_ship

def save_party_cart(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('purchasers[]', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'}, 'request': {}}
        return HttpResponse(ajax_render(data))
    elif settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        data = {'status': {'success': False, 'reason': 'Please select a party first.'}, 'request': {}}
        return HttpResponse(ajax_render(data))
    else:
        purchasers, order_details, direct_ship, total_orders = get_order_details_from_request(request.POST)
        event=Event.objects.get(pk=request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY])
        store_party_order_details(event, order_details, direct_ship)
        data = {'status': {'success': True}}
        return HttpResponse(ajax_render(data))

def get_product_price(pk):
    """Products Data"""
    products = {}
    products = Product.objects.by_site(active=True, id=pk).distinct()
    product_list = list(products)
    sale=find_best_auto_discount(product_list)
    return float(discount_price(products[0], sale))


def add_prod_to_cart(cart, product, quantity, details, request, formdata):
    quantity=int(quantity)
    satchmo_cart_details_query.send(
        cart,
        product=product,
        quantity=quantity,
        details=details,
        request=request,
        form=formdata
    )
    added_item = cart.add_item(product, quantity, details)
    satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item, product=product, request=request, form=formdata)


from apps.compensation.models import PartyOrder

def order_tracking(request, order_id):
    order = None
    try:
        contact = Contact.objects.from_request(request, create=False)
        try:
            order = Order.objects.get(id__exact=order_id, contact=contact)
        except Order.DoesNotExist:
            pass
    except Contact.DoesNotExist:
        contact = None


    try:
        po=PartyOrder.objects.get(party__owner=request.user, order__id__exact=order_id)
        order=po.order
        contact=order.contact
    except:
        pass


    if order is None:
        return bad_or_missing(request, "The order you have requested doesn't exist, or you don't have access to it.")

    ctx = RequestContext(request, {
        'default_view_tax': config_value('TAX', 'DEFAULT_VIEW_TAX'),
        'contact' : contact,
        'order' : order})

    return render_to_response('shop/order_tracking.html', context_instance=ctx)

order_tracking = login_required(order_tracking)


from satchmo_store.shop.models import Cart, Config, Order
from payment.forms import PaymentContactInfoForm
from payment.views.confirm import ConfirmController
from payment.views.payship import credit_pay_ship_process_form, pay_ship_info_verify
from satchmo_store.contact import CUSTOMER_ID
from livesettings import config_get_group, config_value
from signals_ahoy.signals import form_initialdata

@login_required
def process_party_payment(request):
    """Getting order details"""
    event = Event.objects.get(pk=request.POST.get('event'), starts_at__gte=datetime.datetime.today())
    order_details, direct_ship=get_party_order_details(event)
    order_items={}
    

    for_whome=request.POST.get('for', None)

    import pdb; pdb.set_trace()

    if for_whome != 'party':
        order_items=order_details[for_whome]
    else:
        for u, details in order_details.items():
            if u not in direct_ship:
                for item, quantity in details.items():
                    if item not in order_items:
                        order_items[item]=Decimal(0)

                    order_items[item]+=Decimal(quantity)
    

    if len(order_items)>0:
        request.session['whose_party_cart']=for_whome
        tempCart = Cart.objects.from_request(request)
        tempCart.empty()

        offerable_quant_total=0
        cart_total=Decimal(0.00)
        details = []
        formdata = request.POST


        for item, quantity in order_items.items():
            item=Product.objects.get(pk=item)
            if item.slug not in settings.OFFERLESS_PRODUCTS_SLUG and item.slug not in settings.DISC_PRODUCT_SLUGS:
                offerable_quant_total=offerable_quant_total+int(quantity)

            if item.slug not in settings.DISC_PRODUCT_SLUGS:
                cart_total=item.unit_price*int(quantity)
                add_prod_to_cart(tempCart, item, quantity, details, request, formdata)
                
        offers=Offer.objects.filter(active=True)
        offer_quantity=0
        apply_offer=False
        applicable_offers=[]
        for offer in offers:
            if offerable_quant_total%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':0, 'offer':offer})
            elif (offerable_quant_total+offer.free)%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':offer.free, 'offer':offer})
            else:
                units=int(offerable_quant_total/(offer.buy+offer.free))
                if units>0:
                    apply_offer=True
                    applicable_offers.append({'need_more':0, 'offer':offer}) 

        if apply_offer:
            apply_offer=applicable_offers[0]    
            offer_quantity=(offerable_quant_total/(apply_offer['offer'].buy+apply_offer['offer'].free))*apply_offer['offer'].free
            request.session[settings.OFFER_AMT_SESSION_SLUG] = Decimal(offer_quantity)
            add_prod_to_cart(tempCart, Product.objects.get(slug=settings.OFFER_PRODUCT_SLUG), offer_quantity, details, request, formdata)




    
        """ Paymnet processing stuff from here """

        init_data={}
        shop=Config.objects.get_current()
        if request.user.is_authenticated():
            if request.user.email:
                init_data['email'] = request.user.email
            if request.user.first_name:
                init_data['first_name'] = request.user.first_name
            if request.user.last_name:
                init_data['last_name'] = request.user.last_name

        try:
            order = Order.objects.from_request(request)
            if order.discount_code:
                init_data['discount'] = order.discount_code
        except Order.DoesNotExist:
            pass


        if for_whome=='party':
            contact = request.user.contact
            email = contact.email
            if contact.primary_phone:
                phone = contact.primary_phone.phone
            else:
                phone = request.user.profile.phone_number
        else:
            a=Attendee.objects.get(pk=for_whome)
            email=a.email
            try:
                contact = Contact.objects.filter(email=email)[0]
                phone = contact.primary_phone.phone
            except Contact.DoesNotExist:
                contact = None
            phone=a.phone


        name=request.POST.get('add_addressee', ' ').split(" ", 1)

        data={
          'paymentmethod':'AUTHORIZENET',
          'first_name':name[0],
          'last_name':name[1] if ( len(name)==2 and len(name[1])>0 ) else " ",
          'addressee':name,
          'email':email,
          'street1':request.POST.get('add_street1', ''),
          'street2':request.POST.get('add_street2', ''),
          'city':request.POST.get('add_city', ''),
          'state':request.POST.get('add_state', ''),
          'postal_code':request.POST.get('add_postal_code', ''),
          'phone':phone,
          'copy_address':True
        }

        data['credit_type']=request.POST.get('credit_type')
        data['credit_number']=request.POST.get('credit_number')
        data['month_expires']=request.POST.get('month_expires')
        data['year_expires']=request.POST.get('year_expires')
        data['ccv']=request.POST.get('ccv')

        new_data=data
        form = PaymentContactInfoForm(data=new_data, shop=shop, contact=contact, shippable=tempCart.is_shippable, initial=init_data, cart=tempCart)

        if form.is_valid():
            if contact is None and request.user and request.user.is_authenticated():
                contact = Contact(user=request.user)
            custID = form.save(request, cart=tempCart, contact=contact)
            request.session[CUSTOMER_ID] = custID

            modulename = new_data['paymentmethod']
            if not modulename.startswith('PAYMENT_'):
                modulename = 'PAYMENT_' + modulename
            paymentmodule = config_get_group(modulename)

            #return ajax_render({'status':{'success':True}})
            pass #for a while
        else:
            #print form.errors
            return ajax_render({'status':{'success':False, 'code':'general', 'reason':form.errors}})


        """Verify generic credit card data"""
        form_handler=credit_pay_ship_process_form
        #payment_module=config_get_group(request.POST.get('paymentmethod'))
        results=pay_ship_info_verify(request, paymentmodule)

        if not results[0]:
            """
            It should never enter this block, if the request has come all the way down till here.
            It check for a contact or cart in the session.
            """
            return results[1]

        contact = results[1]
        working_cart = results[2]
        results = form_handler(request, contact, working_cart, paymentmodule)
        if results[0]:
            """If form has no error.. result[1] has Http Redirection to confirmation URL"""
            #Processing credit card
            controller=ConfirmController(request, paymentmodule)
            if controller.confirm():
                return ajax_render({'status':{'success':True}, 'data':{'for':for_whome}})
            else:
                return ajax_render({'status':{'success':False, 'code':'general', 'reason':{'__all__': [controller.processorMessage, ]}}})

        else:
            return ajax_render({'status':{'success':False, 'code':'general', 'reason':results[1].errors}})
    return HttpResponse(str(request.POST))


def party_payment(request):
    event = Event.objects.get(pk=request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY], starts_at__gte=datetime.datetime.today())
    tempCart = Cart.objects.from_request(request)


    """Credit Card Form"""
    from payment.views.payship import credit_pay_ship_process_form, pay_ship_info_verify
    from livesettings import config_get_group, config_value
    from payment.config import gateway_live

    form_handler=credit_pay_ship_process_form
    payment_module=config_get_group('PAYMENT_AUTHORIZENET')
    #print payment_module
    results=pay_ship_info_verify(request, payment_module)
    if not results[0]:
        """Need to do something here"""
        contact = None #results[1]
        working_cart = tempCart #results[2]
    else:
        contact = results[1]
        working_cart = results[2]

    results = form_handler(request, contact, working_cart, payment_module)
    if results[0]:
        """Need to do something here too"""
        pass

    form = results[1]
    card_details={
        'form': form,
        'PAYMENT_LIVE': gateway_live(payment_module),
        'cart':working_cart
    }

    

    """Calculate Cart Details"""
    total_cost={'party':Decimal(0.00)}
    offers=Offer.objects.filter(active=True)
    total_sale=Decimal(0.00)
    attendee=event.attendee()
    attendee={a.pk:a for a in attendee}
    unofferable_product_ids=Product.objects.values_list('id', flat=True).filter(slug__in=settings.OFFERLESS_PRODUCTS_SLUG)
    order_details, direct_ship=get_party_order_details(event)
    for att, details in order_details.items():
        amount=Decimal(0.00)
        no_of_items=0
        offerable_quant_total=0
        for item, quantity in details.items():
            no_of_items +=  int(quantity)
            amount += Decimal(quantity)*Decimal(get_product_price(int(item)))
            if int(item) not in unofferable_product_ids:
                offerable_quant_total += int(quantity)

        attendee[int(att)].amount=amount
        attendee[int(att)].no_of_items=no_of_items
        attendee[int(att)].offerable_quant_total=offerable_quant_total
        attendee[int(att)].offered_quantity=0
        attendee[int(att)].shipping_cost=0
        attendee[int(att)].discount=0
        attendee[int(att)].final_amount=amount


        offer_quantity=0
        apply_offer=False
        applicable_offers=[]
        for offer in offers:
            if offerable_quant_total%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':0, 'offer':offer})
            elif (offerable_quant_total+offer.free)%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':offer.free, 'offer':offer})
            else:
                units=int(offerable_quant_total/(offer.buy+offer.free))
                if units>0:
                    apply_offer=True
                    applicable_offers.append({'need_more':0, 'offer':offer}) 

        if apply_offer:
            apply_offer=applicable_offers[0]    
            offer_quantity=(offerable_quant_total/(apply_offer['offer'].buy+apply_offer['offer'].free))*apply_offer['offer'].free
            attendee[int(att)].offered_quantity=offer_quantity
            attendee[int(att)].final_amount=attendee[int(att)].final_amount-(Decimal(15)*Decimal(offer_quantity))
            attendee[int(att)].discount=(Decimal(15)*Decimal(offer_quantity))

        total_sale+=attendee[int(att)].final_amount
    
        if att in direct_ship:
            total_cost[int(att)]=Decimal(attendee[int(att)].final_amount)
        else:
            total_cost['party']+=Decimal(attendee[int(att)].final_amount)

        
    pcv=total_sale*Decimal(0.65)
    shipping=Decimal(0.00)
    split_orders={}
    party_shipping=Decimal(0.00)
    """Calculating shipping details"""
    carrier = Carrier.objects.get(active=True, key='UPS')
    for whose, amt in total_cost.items():
        if amt > 0.00:
            charge=carrier.tiers.filter(min_total__lte=amt).order_by('-min_total')[:1]
            shipping+=Decimal(charge[0].price)
            if whose!='party':
                attendee[int(whose)].total_cost=Decimal(charge[0].price)
                attendee[int(whose)].final_amount+=attendee[int(whose)].total_cost
                split_orders[whose]={'amount':attendee[int(whose)].final_amount, 'for':whose, 'display':attendee[int(whose)].name()+" (Direct Ship party attendee)"}
            else:
                party_shipping=Decimal(charge[0].price)
                split_orders[whose]={'amount':amt+Decimal(charge[0].price), 'for':whose, 'display':'Party (Pay for entire party, Will not include Direct Ship)'}
            

    for user, details in split_orders.items():
        if user=='party':
            split_orders[user]['add_name']=request.user.get_full_name()
            split_orders[user]['add_street1']=event.street
            split_orders[user]['add_street2']=event.street2
            split_orders[user]['add_city']=event.city
            split_orders[user]['add_state']=event.state
            split_orders[user]['add_postal_code']=event.postal_code
        else:
            a=Attendee.objects.get(pk=user)
            split_orders[user]['add_name']=a.name
            split_orders[user]['add_street1']=a.street
            split_orders[user]['add_street2']=a.street2
            split_orders[user]['add_city']=a.city
            split_orders[user]['add_state']=a.state
            split_orders[user]['add_postal_code']=a.postal_code

    for a, a_data in attendee.items():
        if a not in split_orders:
            split_orders[a]={
                'amount':a_data.final_amount,
                'for':a,
                'display':a_data.name()+' (Pay Individual Order Amount as Party Order)',
                'add_name':request.user.get_full_name(),
                'add_street1':event.street,
                'add_street2':event.street2,
                'add_city':event.city,
                'add_state':event.state,
                'add_postal_code':event.postal_code,
                'not_implemented': False #GOD!!! This got me looking f$%$#ng everywhere.
            }

    shop=Config.objects.get_current()
    init_data={}

    form_initialdata.send(sender=PaymentContactInfoForm, initial=init_data, contact=contact, cart=tempCart, shop=shop)
    form = PaymentContactInfoForm(shop=shop, contact=contact, shippable=tempCart.is_shippable, initial=init_data, cart=tempCart)
    if shop.in_country_only:
        only_country = shop.sales_country
    else:
        only_country = None
        
    contact_details={'form':form, 'country':only_country, 'paymentmethod_ct':len(form.fields['paymentmethod'].choices)}

    
    

    """Setting context variables"""
    cxt={
        'contact_details':contact_details,
        'attendee':attendee,
        'card_details':card_details,
        'party':event,
        'pcv':pcv,
        'event':event,
        'total_sale':total_sale,
        'oeder_total':shipping+total_sale,
        'shipping':shipping,
        'previous_order_total':event.orders_total(),
        'total_cost':total_cost,
        'party_shipping':party_shipping,
        'split_orders':split_orders
    }
    return render_to_response('manage/party-payment.html', cxt, context_instance=RequestContext(request))


def add_party_products(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('purchasers[]', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'}, 'request': {}}
        return HttpResponse(ajax_render(data))
    elif settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        data = {'status': {'success': False, 'reason': 'Please select a party first.'}, 'request': {}}
        return HttpResponse(ajax_render(data))
    else:
        purchasers, order_details, direct_ship, total_orders = get_order_details_from_request(request.POST)
        event=Event.objects.get(pk=request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY])
        if event.attendee().count() > len(purchasers):
            data = {'status': {'success': False, 'reason': 'Please select items for all attendees.'}, 'request': {}}
            return HttpResponse(ajax_render(data))

        
        store_party_order_details(event, order_details, direct_ship)
        unofferable_product_ids=Product.objects.values_list('id', flat=True).filter(slug__in=settings.OFFERLESS_PRODUCTS_SLUG)

        offers=Offer.objects.filter(active=True)
        offer_quantity=0
        for pdetails in order_details:
            quant_total=0
            offerable_quant_total=0
            apply_offer=None
            applicable_offers=[]
            for pid in order_details[pdetails]:
                pcount=order_details[pdetails][pid]
                quant_total=quant_total+int(pcount)
                if int(pid) not in unofferable_product_ids:
                    offerable_quant_total=offerable_quant_total+int(pcount)



            for offer in offers:
                if offerable_quant_total%(offer.buy+offer.free)==0:
                    apply_offer=True
                    applicable_offers.append({'need_more':0, 'offer':offer})
                elif (offerable_quant_total+offer.free)%(offer.buy+offer.free)==0:
                    apply_offer=True
                    applicable_offers.append({'need_more':offer.free, 'offer':offer})
                else:
                    units=int(offerable_quant_total/(offer.buy+offer.free))
                    if units>0:
                        apply_offer=True
                        applicable_offers.append({'need_more':0, 'offer':offer}) #we will show need more only of that is the no of free products

            if apply_offer:
                apply_offer=applicable_offers[0]
                
	        offer_quantity=(offerable_quant_total/(apply_offer['offer'].buy+apply_offer['offer'].free))*apply_offer['offer'].free

                request.session[settings.OFFER_AMT_SESSION_SLUG] = Decimal(offer_quantity)
                offer_product=Product.objects.get(slug=settings.OFFER_PRODUCT_SLUG)
                total_orders[offer_product.pk]=1

        cart = Cart.objects.from_request(request=request, create=True)
        if not (cart.__class__ == NullCart or cart.is_empty):
            cart.empty()

        details = []
        formdata = request.POST

        for product, quantity in total_orders.items():
            product = Product.objects.get(pk=product)
            add_prod_to_cart(cart, product, quantity, details, request, formdata)

        request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY] = order_details
        request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY] = direct_ship
        return HttpResponse(ajax_render({'status': {'success': True}, }))



def del_products(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))

    try:
        pid = int(request.POST.get('product_id'))
    except (TypeError, ValueError):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'No/Invalid product_id.'}}))

    cart=Cart.objects.from_request(request, create=True)
    try:
        cartitem = CartItem.objects.get(product__id=pid, cart=cart)
        cartitem.delete()
    except CartItem.DoesNotExist:
        pass
    
    return HttpResponse(ajax_render({'status': {'success': True}, 'item_in_cart': get_items_in_cart(request, all_data=True)}))


def add_products(request):
    if request.method != 'POST':
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.POST.get('productslug_1', None) or not request.POST.get('quantity_1',
        None) or not request.POST.get('product_1', None):
        return HttpResponse(ajax_render({'status': {'success': False, 'reason': 'Invalid request.'}}))
    elif not request.user.is_authenticated:
        data = {'status': {'success': False, 'reason': 'User not loggedin.'},
                'request': {'quantity': request.POST.get('quantity_1'), 'productslug': request.POST.get('productslug_1')
                    , 'product': request.POST.get('product_1')}}
        return HttpResponse(ajax_render(data))
    else:
        try:
            product = Product.objects.get(slug=request.POST.get('productslug_1'))
        except:
            data = {'status': {'success': False, 'reason': 'This product doesnot exists.'},
                    'request': {'quantity': request.POST.get('quantity_1'),
                                'productslug': request.POST.get('productslug_1'),
                                'product': request.POST.get('product_1')}}
            return HttpResponse(ajax_render(data))

        error_in_quantity = False
        try:
            quantity = int(request.POST.get('quantity_1'))
            if quantity < 0:
                error_in_quantity = True
        except:
            error_in_quantity = True

        if error_in_quantity:
            data = {'status': {'success': False, 'reason': 'Please enter a valid quantity.'},
                    'request': {'quantity': request.POST.get('quantity_1'),
                                'productslug': request.POST.get('productslug_1'),
                                'product': request.POST.get('product_1')}}
            return HttpResponse(ajax_render(data))

        cart = Cart.objects.from_request(request, create=True)
        details = []
        formdata = request.POST
        add_prod_to_cart(cart, product, quantity, details, request, formdata)

        return HttpResponse(ajax_render({'status': {'success': True},
                                         'request': {'quantity': request.POST.get('quantity_1'),
                                                     'productslug': request.POST.get('productslug_1'),
                                                     'product': request.POST.get('product_1')},
                                         'item_in_cart': get_items_in_cart(request, all_data=True)}))


@login_required
def set_party(request):
    party = request.GET.get('party', None)
    try:
        today=datetime.datetime.today()
        Event.objects.filter(owner=request.user, status=3, pk=party, starts_at__gte=today)
        request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY] = party
        return HttpResponseRedirect(reverse('manager_select_products'))
    except:
        return HttpResponseRedirect(reverse('manager_shop'))


@login_required
@cache_control(must_revalidate=True, max_age=0, no_cache=True, no_store=True)
def select_party_products(request):
    
    today=datetime.datetime.today()
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        return HttpResponseRedirect(reverse('manager_shop'))
    
    if request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.PARTY_WHOLESALE_DISCOUNT_SLUG:
        #print settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY
        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
            parties = Event.objects.filter(owner=request.user, status=3, starts_at__gte=today)
            return render_to_response('manage/select-party.html', {'parties': parties},
                context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect(reverse('manager_select_products'))

    products = {}
    if request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        category = Category.objects.get_by_site(slug=settings.MARKETING_PRODUCT_CATEGORY_SLUG)
        products = Product.objects.by_site(active=True, category=category).distinct()
    else:
        category = Category.objects.get_by_site(slug=settings.MAIN_PRODUCT_CATEGORY_SLUG)
        products = Product.objects.by_site(active=True, category__parent=category).distinct()

    product_list = list(products)
    sale = find_best_auto_discount(product_list)
    
    event = Event.objects.get(pk=request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY], starts_at__gte=today)
    attendee = event.attendee()

    order_details, direct_ship=get_party_order_details(event)
    request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY]=direct_ship
    request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]=order_details


    products_data = {}
    for p in products:
        products_data[p.pk] = {'sku': p.sku, 'slug': p.slug, 'name': p.translated_name(),
                               'price': float(discount_price(p, sale))}
    #, 'categories':[c.slug for c in p.category.all()]

    carrier = Carrier.objects.get(active=True, key='UPS')
    #carrier_charges=json.dumps(carrier.tiers.values('min_total', 'price').all(), use_decimal=True)
    carrier_charges = []
    charges = carrier.tiers.values('min_total', 'price').all().order_by('min_total')
    for c in charges:
        carrier_charges.append({'min_total': "%.2f" % c['min_total'], 'price': "%.2f" % c['price']})
    carrier_charges = json.dumps(carrier_charges)
    attendee_form = AttendeeForm()
    event_form=EventForm(instance=event)

    po=party_order_info(None, request, True, True)
    attendee_info=[]
    direct_ship_info=request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY] if settings.PARTY_SHIPDETAILS_SESSION_KEY in request.session else []

    for a in attendee:
        if a in po:
            a.cart=po[a]
        else:
            a.cart=None
        a.direct_ship=True if str(a.id) in direct_ship_info else False
        #print a.direct_ship
        #print direct_ship_info
        attendee_info.append(a)

    rewards=[{'min':float(hr['min_sale']), 'max':float(hr['max_sale']) if hr['max_sale']>0.00 else False, 'desc':hr['desc']} for hr in HostingReward.objects.values('min_sale', 'max_sale', 'desc').order_by('min_sale').all()]
    offers = Offer.objects.filter(active=True)

    return render_to_response('manage/select-party-products.html',
            {'offers':offers, 'previous_order_total':event.orders_total(), 'rewards':simplejson.dumps(rewards), 'carrier_charges': carrier_charges, 'party': event, 'party_json':simplejson.dumps({'id':event.id, 'name':event.name, 'description':event.description, 'street':event.street, 'street2':event.street2, 'city':event.city, 'state':event.state, 'zip':event.postal_code}), 'products': products, 'sale': sale,
             'attendee': attendee_info, 'offerless_products_slug':json.dumps(settings.OFFERLESS_PRODUCTS_SLUG), 'event_form':event_form, 'attendee_form':attendee_form, 'products_data': json.dumps(products_data)},
        context_instance=RequestContext(request))


@login_required
def select_products(request):
    today=datetime.datetime.today()
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        return HttpResponseRedirect(reverse('manager_shop'))

    if request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.PARTY_WHOLESALE_DISCOUNT_SLUG:
        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
            parties = Event.objects.filter(owner=request.user, status=3, starts_at__gte=today)
            return render_to_response('manage/select-party.html', {'parties': parties},
                context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect(reverse('manager_select_party_products'))

    products = {}
    marketing_material = False
    if request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        category = Category.objects.get_by_site(slug=settings.MARKETING_PRODUCT_CATEGORY_SLUG)
        products = Product.objects.by_site(active=True, category=category).distinct()
        marketing_material = True
    else:
        category = Category.objects.get_by_site(slug=settings.MAIN_PRODUCT_CATEGORY_SLUG)
        products = Product.objects.by_site(active=True, category__parent=category).distinct()

    product_list = list(products)
    sale = find_best_auto_discount(product_list)
    offers = Offer.objects.filter(active=True)

    return render_to_response('manage/select-products.html',
            {'products': products, 'sale': sale, 'marketing_material': marketing_material, 'offers': offers, 'offerless_products_slug':json.dumps(settings.OFFERLESS_PRODUCTS_SLUG),
             'item_in_cart': json.dumps(get_items_in_cart(request, all_data=True))},
        context_instance=RequestContext(request))


@login_required
def dashboard(request):
    from apps.account.utils import _get_user_overview

    #print _get_user_overview(request.user, 14, 4, 2012)

    # Get the videos that'll be shown on this screeen then pass them as context
    videos = Video.objects.filter(show_in_dashboard=True)

    notices = Notice.objects.filter(Q(specific_users=None) | Q(specific_users__in=[request.user, ]), active=True,
        location='ud')
    return render_to_response('manage/dashboard.html', 
                              {'notices': notices,
                               'volumes': request.user.current_month_volumes, 
                               'videos': videos}, context_instance=RequestContext(request))


@login_required
def user_resources(request):
    videos = Video.objects.all()
    pictures = Picture.objects.all()
    documents = Document.objects.filter(type='tm')

    available = ['doc', 'jpg', 'pdf', 'txt', 'xls'] #icons

    cus_documents = Document.objects.filter(Q(type='ci') | Q(type='cf'))
    cus_media = {'ci': [], 'cf': []}

    for d in cus_documents:
        ext = d.document.path.rsplit('.')
        append_to = d.type
        if (len(ext) == 2) and (ext[1] in available):
            d.ext = ext[1]
        else:
            d.ext = 'file'
        cus_media[append_to].append(d)

    media = []
    for picture in pictures:
        picture.type = 'picture'
        media.append(picture)

    for video in videos:
        video.type = 'video'
        media.append(video)

    for doc in documents:
        ext = doc.document.path.rsplit('.')
        doc.type = 'doc'
        if (len(ext) == 2) and (ext[1] in available):
            doc.ext = ext[1]
        else:
            doc.ext = 'file'
        media.append(doc)

    return render_to_response('manage/user_resources.html', {'media': media, 'cus_media': cus_media},
        context_instance=RequestContext(request))


@login_required
def site(request):
    return render_to_response('manage/site.html', {}, context_instance=RequestContext(request))


@login_required
def financial(request):
    from satchmo_store.shop.models import Order
    # Return a json object
    if request.is_ajax():
        try:
            order = Order.objects.get(pk=request.GET.get("order_pk"))
            status = "Ok"
        except Order.DoesNotExist:
            order = None
            status = "Fail. Order does not exist."

        t = get_template("manage/order_detail.html")
        template = t.render(RequestContext(request, {"order": order}))
        json_dict = {"template": template,
                     "status": status}
        response = json.dumps(json_dict, ensure_ascii=False)
        return HttpResponse(response, mimetype="application/json")

    all_order = CommissionableOrder.objects.filter(commission_to=request.user, remarks__in=['self', 'web'], )
    personal_order = CommissionableOrder.objects.filter(commission_to=request.user, remarks='self', )
    retail_order = CommissionableOrder.objects.filter(commission_to=request.user, remarks__in=['self', 'web'], )
    enrolles_order = CommissionableOrder.objects.filter(commission_to=request.user, remarks__in=['l1'], )
    dl_rank = _get_downline_rank_and_volume_for_month(request.user, 9, 2011)

    party_orders = list(PartyOrder.objects.values_list('order__id', flat=True).filter(party__owner=request.user))
    party_orders = CommissionableOrder.objects.filter(commission_to=request.user, order__id__in=party_orders)

    return render_to_response('manage/financial.html',
            {'party_orders': party_orders, 'dl_rank': dl_rank, 'all_order': all_order, 'personal_order': personal_order,
             'retail_order': retail_order, 'enrolles_order': enrolles_order}, context_instance=RequestContext(request))


@login_required
def team(request):
    y = datetime.datetime.now() - datetime.timedelta(days=1)
    yesterday_start = datetime.datetime(y.year, y.month, y.day, 0, 0, 0)
    yesterday_end = datetime.datetime(y.year, y.month, y.day, 23, 59, 59)
    seven_days = yesterday_end - datetime.timedelta(days=7)
    thirty_days = yesterday_end - datetime.timedelta(days=30)

    frontline = request.user.frontline

    top_sponsors = {}
    week_top_sponsors = {}
    month_top_sponsors = {}
    enrolled_yesterday = []

    sponsored = UserSponsor.objects.filter(sponsor__in=frontline)
    #return HttpResponse(str(sponsored))
    for s in sponsored:
        if s.sponsor not in top_sponsors:
            top_sponsors[s.sponsor] = 0
        top_sponsors[s.sponsor] = top_sponsors[s.sponsor] + 1

        if s.user.date_joined < yesterday_end and s.user.date_joined > seven_days:
            if s.sponsor not in week_top_sponsors:
                week_top_sponsors[s.sponsor] = 0
            week_top_sponsors[s.sponsor] = week_top_sponsors[s.sponsor] + 1

        if s.user.date_joined < yesterday_end and s.user.date_joined > thirty_days:
            if s.sponsor not in month_top_sponsors:
                month_top_sponsors[s.sponsor] = 0
            month_top_sponsors[s.sponsor] = month_top_sponsors[s.sponsor] + 1

        if s.user.date_joined < yesterday_end and s.user.date_joined > yesterday_start:
            enrolled_yesterday.append(s)

    #top_sponsors=sorted(top_sponsors, key=top_sponsors.get)

    party_attendees=Attendee.objects.filter(event__owner=request.user)
    
    return render_to_response('manage/team.html',
            {'enrolled_yesterday': enrolled_yesterday, 'week_top_sponsors': week_top_sponsors,
             'month_top_sponsors': month_top_sponsors, 'top_sponsors': top_sponsors, 'party_attendees':party_attendees},
        context_instance=RequestContext(request))


@login_required
def tools_menu(request):
    return render_to_response('manage/tools-menu.html', {}, context_instance=RequestContext(request))

@login_required
def send_email(request):
    from django.conf import settings
    subject=request.POST.get('subject', '')
    if len(subject.strip()) < 3 or len(subject.strip()) > 256:
        return HttpResponse(json.dumps({'status':{'success':False, 'reason':'Subject should be 3 - 256 chars'}}))
        
    body=request.POST.get('body', '')
    if len(body.strip()) < 25 or len(body.strip()) > 2500:
        return HttpResponse(json.dumps({'status':{'success':False, 'reason':'Body should be 25 - 2500 chars'}}))
    else:
        body_content=body

    users=request.POST.getlist('selected_users[]')
    users=User.objects.filter(id__in=users)
    
    if settings.DEBUG:
        users=User.objects.filter(email__endswith='amanjain.com')

    m=Email(subject=subject, body=body_content, by=request.user)
    m.save()
 
    for u in users:
        body=body_content.replace('{', '{{').replace('}', '}}')
        t=Template(body)
        c=Context({
                'email': u.email,
                'name': u.get_full_name(),
                'promo_url': "http://%s" % (request.user.website.domain()),
         })
        message=t.render(c)
        
        msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [u.email, ], headers={'Reply-To': request.user.email})
        msg.content_subtype = "html"
        msg.send()
        EmailRecipients(email_body=m, email_id=u.email, receipient=u.get_full_name()).save()
    return HttpResponse(json.dumps({'status':{'success':True}}))

def get_selected_people(data):
    only={'c':data.get('o[c]', None), 'r':data.get('o[r]', None)}
    if only['c']:
        try:
            only['c']=base64.decodestring(only['c'])
            only['c']=only['c'].split(',')
            only['c']=[(int(id) if id.isdigit() else -1) for id in only['c']]
            only['c']=User.objects.filter(id__in=only['c'])

        except:
            only['c']=None
    if only['r']:
        try:
            only['r']=base64.decodestring(only['r'])
            only['r']=only['r'].split(',')
            only['r']=[(int(id) if id.isdigit() else -1) for id in only['r']]
            only['r']=Contact.objects.filter(id__in=only['r'])

        except:
            only['r']=None

    return only


def send_user_email(email_obj, subject, body_content, receiver_email, receiver_name, sender_email, sender_domain):
    body=body_content.replace('{', '{{').replace('}', '}}')
    t=Template(body)
    c=Context({
            'email': receiver_email,
            'name': receiver_name,
            'promo_url': "http://%s" % (sender_domain),
            })
    message=t.render(c)

    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [receiver_email, ], headers={'Reply-To': sender_email})
    msg.content_subtype = "html"
    msg.send()
    EmailRecipients(email_body=email_obj, email_id=receiver_email, receipient=receiver_name).save()

@login_required
def tools_filter(request):
    done=False
    only=get_selected_people(request.GET)
    
    subject_err=False
    body_err=False
    if request.method == 'POST':
        subject = request.POST.get('subject', '')
        if len(subject.strip()) < 3 or len(subject.strip()) > 256:
            subject_err = True
        body = request.POST.get('body', '')
        if len(body.strip()) < 25 or len(body.strip()) > 2500:
            body_err = True
        else:
            body_content = body

        if not subject_err and not body_err and (only['c'] or only['r']):
            if settings.DEBUG:
                only['c']=User.objects.filter(email__endswith='amanjain.com')
                only['r']=Contact.objects.filter(email__endswith='amanjain.com')

            m=Email(subject=subject, body=body_content, by=request.user)
            m.save()

            for u in only['c']:
                send_user_email(email_obj=m, subject=subject, body_content=body_content, receiver_email=u.email, receiver_name=u.get_full_name(), sender_email=request.user.email, sender_domain=request.user.website.domain())
            for u in only['r']:
                send_user_email(email_obj=m, subject=subject, body_content=body_content, receiver_email=u.email, receiver_name=u.full_name, sender_email=request.user.email, sender_domain=request.user.website.domain())

            done=True

    return render_to_response('manage/tools_filter.html', {'only':only, 'subject_err':subject_err, 'body_err':body_err, 'done':done}, context_instance=RequestContext(request))

@login_required
def tools(request):
    """
    MRVG: Going to make small modifications to this.
    """
    from django.forms.formsets import formset_factory

    EmailRecipientFormSet = formset_factory(EmailRecipientForm, extra=5)
    recipient_err = False
    subject_err = False
    body_err = False
    body_content = False
    if request.method == 'POST':
        formset = EmailRecipientFormSet(request.POST, request.FILES)
        if not formset.is_valid():
            recipient_err = True
        subject = request.POST.get('subject', '')
        if len(subject.strip()) < 3 or len(subject.strip()) > 256:
            subject_err = True
        body = request.POST.get('body', '')
        if len(body.strip()) < 25 or len(body.strip()) > 2500:
            body_err = True
        else:
            body_content = body

        if not subject_err and not body_err and formset.is_valid:
            if len(formset) > 1:
                m = Email(subject=subject, body=body, by=request.user)
                m.save()

            for form in formset:
                if form.is_valid() and 'receipient' in form.cleaned_data:
                    subject, from_email, name, email = subject, request.user.email, form.cleaned_data['receipient'], form.cleaned_data['email_id']

                    body = body_content.replace('{', '{{').replace('}', '}}')
                    t = Template(body)
                    c = Context({
                        'email': email,
                        'name': name,
                        'promo_url': "http://%s" % (request.user.website.domain()),
                        })
                    message = t.render(c)

                    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [email, ],
                        headers={'Reply-To': request.user.email})
                    msg.content_subtype = "html"
                    msg.send()
                    EmailRecipients(body=m, email_id=email, receipient=name).save()

            formset = EmailRecipientFormSet()
    else:
        formset = EmailRecipientFormSet()

    return render_to_response('manage/tools.html',
            {'formset': formset, 'recipient_err': recipient_err, 'subject_err': subject_err, 'body_err': body_err,
             'body_content': body_content}, context_instance=RequestContext(request))


@login_required
def user_information(request):
    UserFormset = modelformset_factory(User, form=UserForm, can_delete=False, extra=0)
    ProPayFormset = inlineformset_factory(User, Propay, fields=('account', ), can_delete=False)
    WebsiteFormset = inlineformset_factory(User, UserWebsite, fields=('name', ), can_delete=False)
    NetworkFormset = inlineformset_factory(User, UserNetwork, extra=1)
    AddressFormset = inlineformset_factory(Contact, AddressBook, extra=1, exclude=('description', ))
    ProfileFormset = inlineformset_factory(User, Profile, can_delete=False)

    if request.method == 'POST':
        # user_form=UserForm(request.POST, request.FILES, instance=request.user)
        user_form = UserFormset(request.POST)
        propay_form = ProPayFormset(request.POST, request.FILES, instance=request.user)
        website_form = WebsiteFormset(request.POST, request.FILES, instance=request.user)
        network_form = NetworkFormset(request.POST, request.FILES, instance=request.user)
        profile_form = ProfileFormset(request.POST, request.FILES, instance=request.user)
        address_form = AddressFormset(request.POST, request.FILES, instance=Contact.objects.get(user=request.user))
        if user_form.is_valid():
            user_form.save()
        if website_form.is_valid():
            website_form.save()
        if network_form.is_valid():
            network_form.save()
        if profile_form.is_valid():
            profile_form.save()
        if address_form.is_valid():
            address_form.save()
        request.user = User.objects.get(pk=request.user.pk)
        return HttpResponseRedirect("/manage/user-info")
    else:
        # user_form=UserFormset(queryset=User.objects.get(pk=request.user.id))
        user_form = UserFormset(queryset=User.objects.filter(id=request.user.id))
        propay_form = ProPayFormset(instance=request.user)
        website_form = WebsiteFormset(instance=request.user)
        network_form = NetworkFormset(instance=request.user)
        profile_form = ProfileFormset(instance=request.user)
        address_form = AddressFormset(instance=Contact.objects.get(user=request.user))

    return render_to_response('manage/user_information.html',
            {'profile_form': profile_form, 'propay_form': propay_form, 'network_form': network_form,
             'website_form': website_form, 'address_form': address_form, 'user_form': user_form},
        context_instance=RequestContext(request))


@login_required
def parties(request):
    form = PartyForm()

    return render_to_response('manage/parties.html', {
        'form': form
    }, context_instance=RequestContext(request))


@login_required
def set_wholesale_discount(request):
    slug = request.GET.get('slug', None)
    try:
        slug = WholesaleDiscount.objects.get(active=True, slug=slug, show_in_frontend=True)
        request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY] = slug

        """Since the type of wholesale discount changed, clean the cart. As the discount amount may vary."""
        cart = Cart.objects.from_request(request=request, create=False)
        if not (cart.__class__ == NullCart or cart.is_empty):
            cart.empty()

        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
            del request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY]
        if settings.PARTY_ORDERDETAILS_SESSION_KEY in request.session:
            del request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]
        if settings.BSC_AMT_SESSION_SLUG in request.session:
            del request.session[settings.BSC_AMT_SESSION_SLUG]
        if settings.FRONTEND_PARTY_SESSION_SELECT in request.session:
            del request.session[settings.FRONTEND_PARTY_SESSION_SELECT]
            
        if slug.slug==settings.PARTY_WHOLESALE_DISCOUNT_SLUG and 'party' in request.GET:
            return set_party(request)

        return HttpResponseRedirect(reverse('manager_select_products'))
    except WholesaleDiscount.DoesNotExist:
        return HttpResponseRedirect(reverse('manager_shop'))


@login_required
def shop(request):
    discounts = WholesaleDiscount.objects.filter(active=True, show_in_frontend=True).order_by('percentage', 'slug')
    return render_to_response('manage/shop.html', {'discounts': discounts}, context_instance=RequestContext(request))


@login_required
def shop_category(request):
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        return HttpResponseRedirect(reverse('manager_shop'))
    elif request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        return HttpResponseRedirect(
            reverse('manager_shopcategoryview', kwargs={'slug': settings.MARKETING_PRODUCT_CATEGORY_SLUG}))
    return render_to_response('manage/shop_category.html', {}, context_instance=RequestContext(request))


def shop_get_product(request, product_slug=None, selected_options=(), default_view_tax=None):
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        return HttpResponseRedirect(reverse('manager_shop'))

    log = logging.getLogger('manage.product')
    errors = [m for m in get_messages(request) if m.level == constants.ERROR]

    try:
        product = Product.objects.get_by_site(active=True, slug=product_slug)
    except Product.DoesNotExist:
        return bad_or_missing(request, _('The product you have requested does not exist.'))

    """If sessions says that the user wants to buy a marketing material, do not show anything else that shouldnot be added to the cart"""
    if request.session.get(
        settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG:
        if Category.objects.get_by_site(slug=settings.MARKETING_PRODUCT_CATEGORY_SLUG) not in product.category.all():
            return HttpResponseRedirect(
                reverse('manager_shopcategoryview', kwargs={'slug': settings.MARKETING_PRODUCT_CATEGORY_SLUG}))

    if default_view_tax is None:
        default_view_tax = config_value('TAX', 'DEFAULT_VIEW_TAX')

    subtype_names = product.get_subtypes()
    # Save product id for xheaders, in case we display a ConfigurableProduct                                                                                                 
    product_id = product.id
    # Clone product object in order to have current product variations in context (extra_context)                                                                            
    current_product = product

    if 'ProductVariation' in subtype_names:
        selected_options = product.productvariation.unique_option_ids
        #Display the ConfigurableProduct that this ProductVariation belongs to.                                                                                              
        product = product.productvariation.parent.product
        subtype_names = product.get_subtypes()
    best_discount = find_best_auto_discount(product)

    if errors:
        error_message = errors[0]
    else:
        error_message = None

    extra_context = {
        'product': product,
        'current_product': current_product,
        'default_view_tax': default_view_tax,
        'sale': best_discount,
        'error_message': error_message,
        }

    # Get the template context from the Product.                                                                                                                             
    extra_context = product.add_template_context(context=extra_context,
        request=request, selected_options=selected_options,
        default_view_tax=default_view_tax)

    template = find_product_template(product, producttypes=subtype_names)
    context = RequestContext(request, extra_context)

    response = http.HttpResponse(template.render(context))
    populate_xheaders(request, response, Product, product_id)
    return response


def find_product_template(product, producttypes=None):
    log = logging.getLogger('manage.product')

    """Searches for the correct override template given a product."""
    if producttypes is None:
        producttypes = product.get_subtypes()

    templates = ["manage/product/detail_%s.html" % x.lower() for x in producttypes]
    templates.append('manage/product/product.html')
    log.debug("finding product template: %s", templates)
    return select_template(templates)


def shop_category_view(request, slug, parent_slugs='', template='manage/category.html'):
    if settings.WHOLESALE_DISCOUNT_SESSION_KEY not in request.session:
        return HttpResponseRedirect(reverse('manager_shop'))
    elif (
        request.session.get(
            settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug == settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG and
        (slug != settings.MARKETING_PRODUCT_CATEGORY_SLUG or parent_slugs != '')
        ):
        return HttpResponseRedirect(
            reverse('manager_shopcategoryview', kwargs={'slug': settings.MARKETING_PRODUCT_CATEGORY_SLUG}))

    try:
        category = Category.objects.get_by_site(slug=slug)
        products = list(category.active_products())
        sale = find_best_auto_discount(products)

    except Category.DoesNotExist:
        return bad_or_missing(request, _('The category you have requested does not exist.'))

    child_categories = category.get_all_children()

    all_products = []
    all_products += products
    for cat in child_categories:
        all_products += list(cat.active_products())

    ctx = {
        'category': category,
        'child_categories': child_categories,
        'sale': sale,
        'products': products,
        'all_products': list(set(all_products)),
        }
    index_prerender.send(Product, request=request, context=ctx, category=category, object_list=products)
    return render_to_response(template, context_instance=RequestContext(request, ctx))


@login_required
def security(request):
    return render_to_response('manage/security.html', {}, context_instance=RequestContext(request))


@login_required
def email(request):
    return render_to_response('manage/email.html', {}, context_instance=RequestContext(request))


@login_required
def commission(request):
    return render_to_response('manage/commission.html', {}, context_instance=RequestContext(request))

def user_details(request):
    user_id=request.GET.get('user_id', -1)
    try:
        user=User.objects.get(pk=user_id)
        if request.user in user.upline:
            volumes=user.current_month_details
            return HttpResponse(simplejson.dumps({"data":{'username':user.username, 'email':user.email, 'last_login':str(user.last_login), 'pcv':float(volumes['volume']['pcv']), 'commissions':float(volumes['comm']) if 'comm' in volumes else 0.00, 'active_frontline':int(user.active_frontline), 'new_recruits':int(user.new_recruits)}}), mimetype='application/json')
    except:
        pass
    raise Http404

def genealogy_tree(request):
    if not request.user.is_authenticated:
        return HttpResponse('{}')
    else:
        volumes=request.user.current_month_details
        return HttpResponse(simplejson.dumps({"name":"You", "user_id":request.user.id, "children":request.user.downline_dict, "data":{'username':request.user.username, 'email':request.user.email, 'last_login':str(request.user.last_login), 'pcv':float(volumes['volume']['pcv']), 'commissions':float(volumes['comm']) if 'comm' in volumes else 0.00, 'active_frontline':int(request.user.active_frontline), 'new_recruits':int(request.user.new_recruits)}}), mimetype='application/json')
    return render_to_response('manage/geneaology.html', {}, context_instance=RequestContext(request))


@login_required
def genealogy(request):
    return render_to_response('manage/geneaology.html', {}, context_instance=RequestContext(request))

@login_required
def training_and_marketing(request):
    return render_to_response('manage/training_and_marketing.html', {}, context_instance=RequestContext(request))


# Handle "Trip" registration
@login_required
def show_tour_registration(request):
    return render_to_response("tour/registration_form.html", {}, context_instance=RequestContext(request))


@login_required
def get_consultant_info(request):
    """
    Return the consultant info in an ajax request

    Data I need to return:
    Name, Username, Personal URL, Rank, Sponsor, Member since, Mailing Address, Email, Phone,
    Last Login
    """
    sponsor_id = request.GET.get("sponsor_id", None)
    #user_sponsor = get_object_or_404(UserSponsor, pk=sponsor_id)
    user = get_object_or_404(User, pk=sponsor_id)

    data_to_return = {
        "name": user.first_name + " " + user.last_name,
        "username": user.username,
        "personal_url": user.username + "." + Site.objects.get_current().domain,
        "rank": user.userrank.rank.name,
        "sponsor": user.sponsor,
        "member_since": user.date_joined,
        "mailing_address": {
            "street": user.contact.addressbook_set.all()[0].street1,
            "city": user.contact.addressbook_set.all()[0].city,
            "state": user.contact.addressbook_set.all()[0].state,
            "zip_code": user.contact.addressbook_set.all()[0].postal_code,
            "country": user.contact.addressbook_set.all()[0].country.name,
        },
        "email": user.email,
        "phone": user.profile.phone_number,
        "last_login": user.last_login,
        "parties": Event.objects.filter(owner=user.pk, status=3)
    }

    t = get_template("manage/consultant_info_table.html").render(RequestContext(request, data_to_return))

    return HttpResponse(simplejson.dumps({"template": t, "status": "success"}), mimetype="application/json")
