from django import forms
from django.contrib.auth.models import User


YESNO = (('1', 'Yes',), ('2', 'No',))
MONTHS = (
    (1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'),
    (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'),
    (10, 'October'), (11, 'November'), (12, 'December')
)

class UserForm(forms.ModelForm):
    
    class Meta:
        model=User
        fields=['first_name', 'last_name', 'email']


class PropayForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(PropayForm, self).__init__(*args, **kwargs)

    first_name = forms.CharField(label='First Name', widget = forms.TextInput(attrs={'class':'validate[required] textBig'}))
    last_name = forms.CharField(label='Last Name', widget = forms.TextInput(attrs={'class':'validate[required] textBig'}))
    email = forms.EmailField(label='Email', widget = forms.TextInput(attrs={'class':'validate[required,custom[email]] textBig'}))
    phone = forms.CharField(label='Phone', widget=forms.TextInput(attrs={'class':'validate[custom[telephone]] textBig'}))
    address = forms.CharField(label='Address', widget=forms.TextInput(attrs={'class':'validate[required] textBig'}))
    city = forms.CharField(label='City', widget=forms.TextInput(attrs={'class':'validate[required] textBig'}))
    state = forms.CharField(label='State', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    zip = forms.CharField(label='Zip', widget=forms.TextInput(attrs={'class':'validate[required] textBig'}))
    country = forms.CharField(label='Country', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    ssn = forms.CharField(max_length=9, min_length=9, label='SSN', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    re_ssn = forms.CharField(label='Re-enter SSN', widget=forms.TextInput(attrs={'class':'validate[required]'}))
    date_of_birth = forms.DateField(label='Date of Birth', widget=forms.TextInput(attrs={'class':'validate[required]'}))


class PartyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(PartyForm, self).__init__(*args, **kwargs)

    name = forms.CharField(label='Party Name',
        widget = forms.TextInput(attrs={'class':'validate[required]'}))
    party_time = forms.DateTimeField(label='Party Time',
        widget=forms.TextInput(attrs={'class':'validate[required]'}))
    
    dob_day = forms.ChoiceField(choices=[(x, x) for x in range(1, 32)])
    dob_month = forms.ChoiceField(choices=MONTHS)
    dob_year = forms.ChoiceField(choices=[(x, x) for x in range(1920, 2013)])
    
    
    city = forms.ChoiceField(choices=[(1, 'NY'), (2, 'Chicago')])
    state = forms.ChoiceField(choices=[(1, 'F'), (2, 'J')])
    
    city2 = forms.ChoiceField(choices=[(1, 'NY'), (2, 'Chicago')])
    state2 = forms.ChoiceField(choices=[(1, 'F'), (2, 'J')])
    
    city3 = forms.ChoiceField(choices=[(1, 'NY'), (2, 'Chicago')])
    state3 = forms.ChoiceField(choices=[(1, 'F'), (2, 'J')])
    
    
    choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=YESNO)
    choice_field2 = forms.ChoiceField(widget=forms.RadioSelect, choices=YESNO)
    choice_field3 = forms.ChoiceField(widget=forms.RadioSelect, choices=YESNO)
    
    street1 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    street2 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    street3 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    street4 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    street5 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    street6 = forms.CharField(label='Street',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    
    postalcode = forms.CharField(label='Zip/Postal',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    
    
    shpping_add_name = forms.CharField(label='Name',
        widget=forms.TextInput(attrs={'class':'validate[required]'}))
    host_info_name = forms.CharField(label='Host Info Name',
        widget=forms.TextInput(attrs={'class':'validate[required]'}))
    
    host_postalcode = forms.CharField(label='ZipPostal',
        widget=forms.TextInput(attrs={'class':''}), required=False)
    host_phone = forms.CharField(label='Phone',
        widget=forms.TextInput(attrs={'class':'validate[required]'}))
    host_email = forms.CharField(label='Email',
        widget=forms.TextInput(attrs={'class':'validate[required]'}))
