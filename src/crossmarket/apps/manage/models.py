from satchmo_store.shop.signals import satchmo_cart_add_verify
from satchmo_store.shop.exceptions import CartAddProhibited

def check_if_allowed(sender, cart=None, cartitem=None, added_quantity=None, details=None, **kwargs):
    items=cart.cartitem_set.all()
    if(len(items))>0:
        item=items[0]
        if (
            not item.product.main_category or
            not item.product.main_category.parent or 
            not cartitem.product.main_category or
            not cartitem.product.main_category.parent or
            item.product.main_category.parent != cartitem.product.main_category.parent):
            pass
            #raise CartAddProhibited(cartitem.product, 'Please check out the current cart and purchase this item again.')

satchmo_cart_add_verify.connect(check_if_allowed)
