import datetime
from django.conf import settings
from satchmo_store.shop.models import Cart, CartItem
from django.db.models import Q, Sum

def no_of_items_in_cart(request):
    cart=Cart.objects.from_request(request=request, create=True)
    result=CartItem.objects.filter(~Q(product__slug__in=settings.DISC_PRODUCT_SLUGS), cart=cart).aggregate(total=Sum('quantity'))
    if result['total'] is None:
        no_of_items_in_cart=0
    else:
        no_of_items_in_cart=result['total']
    return {'no_of_items_in_cart': no_of_items_in_cart}


def discount_product_slug(request):
    return {'discount_product_slug': settings.DISC_PRODUCT_SLUGS}
