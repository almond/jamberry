from satchmo_store.shop.models import Order
from satchmo_store.contact.models import Contact

def _get_user_orders(user):
    contacts=Contact.objects.filter(user=user)
    return Order.objects.filter(contact__in=contacts).order_by('-time_stamp')
