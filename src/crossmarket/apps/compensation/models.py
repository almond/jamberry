from django.db import models
from django.contrib.auth.models import User
from satchmo_store.shop.models import Order
import datetime
from decimal import *
from apps.events.models import Event, Attendee
from product.models import Product


def order_discounted_total(order):
    return order.sub_total-order.discount
Order.discounted_total=property(order_discounted_total)

class HostingReward(models.Model):
    min_sale=models.FloatField(default=0.00)
    max_sale=models.FloatField(default=0.00)
    percent_product_credit=models.FloatField(default=0.00)
    free_sheilds=models.IntegerField(default=0)
    half_priced_item=models.IntegerField(default=0)
    desc=models.CharField(max_length=256)

class Earning(models.Model):
    user=models.ForeignKey(User)
    month=models.IntegerField()
    year=models.IntegerField()
    earning_type=models.IntegerField(default=0)
    earning_basis=models.FloatField(default=0.00)
    earning_percentage=models.FloatField(default=0.00)
    earning_max=models.FloatField(null=True, default=None)
    earning_amount=models.FloatField(default=0.00)
    sourceid=models.IntegerField(null=True, default=None)
    source_orderid=models.IntegerField(null=True, default=None)
    desc=models.CharField(max_length=256)
    
class PartyOrder(models.Model):
    order=models.ForeignKey(Order, related_name='order_party')
    party=models.ForeignKey(Event)
    def __unicode__(self):
        return "Party #%d Order #%d"%(self.party.pk, self.order.pk)

    def details(self):
        po=PartyOrderDetails.objects.filter(party_order=self)
        porders={}
        for p in po:
            if not p.attendee in porders:
                porders[p.attendee]={'attendee':p.attendee, 'orders':[]}
            porders[p.attendee]['orders'].append({'product':p.product, 'quantity':p.quantity})
        return porders

    def direct_shippers(self):
        ids=[]
        po=PartyOrderDirectShip.objects.filter(party_order=self, direct_ship=True)
        for p in po:
            ids.append(p.attendee.pk)
        return ids

class PartyOrderDirectShip(models.Model):
    party_order=models.ForeignKey(PartyOrder)
    attendee=models.ForeignKey(Attendee)
    direct_ship=models.BooleanField(default=False)

class PartyOrderDetails(models.Model):
    party_order=models.ForeignKey(PartyOrder)
    attendee=models.ForeignKey(Attendee)
    product=models.ForeignKey(Product)
    quantity=models.IntegerField(default=0)

class Credit(models.Model):
    user=models.ForeignKey(User)
    credit_type=models.CharField(max_length=3,choices=(('bsc', 'Business Supply Credit'), ('pc', 'Product Credit')))
    transaction_type=models.CharField(max_length=1,choices=(('a', 'Given Credit'), ('s', 'Deducted Credit')))
    timestamp=models.DateTimeField()
    amount=models.FloatField(default=0.00)
    remarks=models.CharField(max_length=128, default='', blank=True)
    
class FastStart(models.Model):
    user=models.ForeignKey(User)
    enroll_date=models.DateTimeField(default=None)
    personally_enrolled=models.IntegerField(default=0)

    pv_month_1=models.FloatField(default=0.00)
    pv_month_2=models.FloatField(default=0.00)
    pv_month_3=models.FloatField(default=0.00)

    parties_month_1=models.FloatField(default=0.00)
    parties_month_2=models.FloatField(default=0.00)
    parties_month_3=models.FloatField(default=0.00)

    bonus_month_1=models.FloatField(default=0.00)
    bonus_month_2=models.FloatField(default=0.00)
    bonus_month_3=models.FloatField(default=0.00)

    credit_month_1=models.FloatField(default=0.00)
    credit_month_2=models.FloatField(default=0.00)
    credit_month_3=models.FloatField(default=0.00)

    bonus_sponsor=models.IntegerField(default=0)
    bonus_final=models.IntegerField(default=0)
    credit_final=models.FloatField(default=0)
    date_created=models.DateTimeField(default=None, null=True)

    achieved_month_1=models.BooleanField()
    achieved_month_2=models.BooleanField()
    achieved_month_3=models.BooleanField()
    achieved_all=models.BooleanField()

    ended_month_1=models.BooleanField()
    ended_month_2=models.BooleanField()
    ended_month_3=models.BooleanField()

    redeemed_month_1=models.BooleanField()
    redeemed_month_2=models.BooleanField()
    redeemed_month_3=models.BooleanField()
    redeemed_all=models.BooleanField()

    type_month_1=models.BooleanField()
    type_month_2=models.BooleanField()
    type_month_3=models.BooleanField()
    type_all=models.BooleanField()

    achieved_month_1_date=models.DateTimeField(default=None, null=True)
    achieved_month_2_date=models.DateTimeField(default=None, null=True)
    achieved_month_3_date=models.DateTimeField(default=None, null=True)
    achieved_month_all_date=models.DateTimeField(default=None, null=True)

    redeemed_month_1_date=models.DateTimeField(default=None, null=True)
    redeemed_month_2_date=models.DateTimeField(default=None, null=True)
    redeemed_month_3_date=models.DateTimeField(default=None, null=True)
    redeemed_month_all_date=models.DateTimeField(default=None, null=True)

    recognized_month_1=models.BooleanField()
    recognized_month_2=models.BooleanField()
    recognized_month_3=models.BooleanField()
    recognized_all=models.BooleanField()

class BSCLog(models.Model):
    #Keep a log of BSC user is entitled to receive if he reaches the monthly min pcv for BSC
    order=models.ForeignKey(Order)
    user=models.ForeignKey(User)
    processed=models.BooleanField(default=False)


class CommissionableOrder(models.Model):
    order=models.ForeignKey(Order)
    commission_to=models.ForeignKey(User)
    remarks=models.CharField(max_length=128)
    timestamp=models.DateTimeField()
    def __unicode__(self):
        return "User %s is commisionable for Order#%d - %s sales"%(self.commission_to.username, self.order.pk, self.remarks)

    def pcv(self):
        return (self.order.discounted_total)*Decimal(0.65)

    def retail_comm(self):
        return (self.order.discounted_total)*Decimal(0.30)


class Rank(models.Model):
    sequence=models.IntegerField(unique=True, blank=False, null=False)
    name=models.CharField(unique=True, blank=False, null=False, max_length=32)
    label=models.CharField(unique=True, blank=False, null=False, max_length=32)
    code=models.CharField(unique=True, blank=False, null=False, max_length=32)
    def __unicode__(self):
        return self.label

class RankRequirement(models.Model):
    rank=models.OneToOneField(Rank, related_name='rule')
    pcv_to_be_active=models.IntegerField(default=0)
    min_frontline_active=models.IntegerField(default=0)
    min_frontline_star_or_above=models.IntegerField(default=0)
    min_pcv_for_month=models.IntegerField(default=0)
    min_gcv_for_month=models.IntegerField(default=0)
    def __unicode__(self):
        return 'Requirement for: %s' % (self.rank.name)
    
    def commission(self):
        return RankCommission.obejcts.get(rank=self)

class RankCommission(models.Model):
    rank=models.OneToOneField(Rank, related_name='commission')
    ratail_commission=models.FloatField(default=0.00)
    volume_bonus_for_500_pcv=models.FloatField(default=0.00)
    business_supply_credit_for_300_pcv=models.FloatField(default=0.00)
    l1=models.FloatField(default=0.00)
    l2=models.FloatField(default=0.00)
    l3=models.FloatField(default=0.00)
    l4=models.FloatField(default=0.00)
    l5=models.FloatField(default=0.00)
    new_payrank_maintainance_bonus=models.FloatField(default=0.00)


class UserTitle(models.Model):
    user=models.OneToOneField(User)
    title=models.ForeignKey(Rank, related_name='title')
    def __unicode__(self):
        return 'User %s: %s' % (self.user.username, self.title)


class UserHistory(models.Model):
    user=models.ForeignKey(User)

    month=models.IntegerField()
    year=models.IntegerField()

    earned_rank=models.ForeignKey(Rank)

    retail_comm=models.FloatField(default=0.00)
    volume_bonus=models.FloatField(default=0.00)
    business_supply_credit=models.FloatField(default=0.00)
    commissions_on_downline=models.FloatField(default=0.00)
    pay_rank_bonus=models.FloatField(default=0.00)

    prv=models.FloatField(default=0.00)
    pcv=models.FloatField(default=0.00)
    pcv_quater=models.FloatField(default=0.00)
    gcv=models.FloatField(default=0.00)

    l1_pcv=models.FloatField(default=0.00)
    l2_pcv=models.FloatField(default=0.00)
    l3_pcv=models.FloatField(default=0.00)
    l4_pcv=models.FloatField(default=0.00)
    l5_pcv=models.FloatField(default=0.00)

    fs_1=models.FloatField(default=0.00)
    fs_2=models.FloatField(default=0.00)
    fs_3=models.FloatField(default=0.00)

    #ALTER TABLE `compensation_userhistory` ADD COLUMN `fs_1` DOUBLE DEFAULT 0.00, ADD COLUMN `fs_2` DOUBLE DEFAULT 0.00, ADD COLUMN `fs_3` DOUBLE DEFAULT 0.00;

    personally_enrolled=models.IntegerField(default=0)
    personally_enrolled_active=models.IntegerField(default=0)

    #active=models.IntegerField()
    #ALTER TABLE `compensation_userhistory` ADD COLUMN `personally_enrolled_star` INT DEFAULT 0, ADD COLUMN `wholsesale_sales` DOUBLE DEFAULT 0.00, ADD COLUMN `wholsesale_onretailsales` DOUBLE DEFAULT 0.00, ADD COLUMN `retail_diff` DOUBLE DEFAULT 0.00;

    personally_enrolled_star=models.IntegerField(default=0)
    wholsesale_sales=models.FloatField(default=0.00)
    wholsesale_onretailsales=models.FloatField(default=0.00)
    retail_diff=models.FloatField(default=0.00)


    def faststart(self):
        total=Decimal(0.00)
        if self.fs_1:
            total=total+Decimal(self.fs_1)
        if self.fs_2:
            total=total+Decimal(self.fs_2)
        if self.fs_3:
            total=total+Decimal(self.fs_3)

        return total

    def total(self, include_fs=False):
        total=Decimal(self.retail_comm+self.volume_bonus+self.commissions_on_downline+self.pay_rank_bonus)
        if include_fs:
            total=total+self.faststart()
        return total
    unique_together=(("user", "month", "year"), )



"""
Following piece of shit was taken from the last jamberry project. I swear to God, I am not responsible for this. I just imported it. - Aman.
"""

class DateDetails(models.Model):
    date=models.DateField()
    dw=models.IntegerField()
    dm=models.IntegerField()
    dy=models.IntegerField()
    day_name=models.CharField(max_length=32)
    day_abbr=models.CharField(max_length=32)
    wk=models.IntegerField()
    mo=models.IntegerField()
    month_name=models.CharField(max_length=32)
    month_abbr=models.CharField(max_length=32)
    qtr=models.IntegerField()
    qtr_abbr=models.CharField(max_length=5)
    yr=models.IntegerField()
    week_begin=models.DateField()
    week_end=models.DateField()
    period=models.IntegerField()
    publish_bonuses=models.DateField()
    autoship_week=models.IntegerField()

class Period(models.Model):
    begin=models.DateField()
    end=models.DateField()
    year=models.IntegerField()
    quater=models.IntegerField()
    month=models.IntegerField()
    
    def __unicode__(self):
        return "Period: %d, Begin: %s, End: %s."%(self.pk, str(self.begin), str(self.end))


