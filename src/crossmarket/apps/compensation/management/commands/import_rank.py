from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
import os, csv, random
from crossmarket.apps.compensation.models import *
from satchmo_store.contact.models import Contact, AddressBook, ContactRole
from l10n.models import Country

class Command(BaseCommand):
    help='Import Ranks from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_rank.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                rank=Rank.objects.get(pk=row[0])
            except Rank.DoesNotExist:
                rank=Rank(pk=row[0])
            
            rank.sequence=row[2]
            rank.name=row[1]
            rank.label=row[4]
            rank.code=row[3]
            rank.save()
