from django.core.management.base import BaseCommand, CommandError
import os, csv, random
from crossmarket.apps.compensation.models import *
from django.conf import settings

class Command(BaseCommand):
    help='Import Ranks from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_rank_rules.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                rr=RankRequirement.objects.get(pk=row[0])
            except RankRequirement.DoesNotExist:
                rr=RankRequirement(pk=row[0])
            
            rr.rank=Rank.objects.get(pk=row[3])
            rr.pcv_to_be_active=row[4]
            rr.min_pcv_for_month=row[5]
            rr.min_frontline_active=row[6]
            rr.min_frontline_star_or_above=row[7]
            rr.min_gcv_for_month=row[8]
            rr.save()
