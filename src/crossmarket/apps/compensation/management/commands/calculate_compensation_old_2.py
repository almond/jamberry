import datetime, copy
from decimal import *

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from satchmo_store.shop.models import Order

from apps.compensation.models import *
from apps.dashboard.models import *

class Command(BaseCommand):
    help='Calculate compensation for a Period.'
    debug=True
    
    def blank_structure(self):
        return {'orders':[], 'sales':{'self':Decimal(0), 'downline':Decimal(0), 'web':Decimal(0), 'downline_by_level':{}}, 'volumes':{'prv':Decimal(0), 'pcv':Decimal(0), 'pcvq':Decimal(0), 'gcv':Decimal(0)}}

    def handle(self, *args, **options):
        """
        Getting period for which the commissions should be paid out.
        Since their Periods table makes no sense right now, I'll do it for last month's first day to last day.
        """
  
        
        """Getting details of last month"""
        date=datetime.datetime.now()
        previous_month_end=datetime.datetime(date.year, date.month, 1, 23, 59, 59) - datetime.timedelta(days=1)
        previous_month_start=datetime.datetime(previous_month_end.year, previous_month_end.month, 1, 0, 0, 0)
        tmp=previous_month_start-datetime.timedelta(days=45)
        last_3rd_month_start=datetime.datetime(tmp.year, tmp.month, 1, 0, 0, 0)


        if self.debug:
            previous_month_end=datetime.datetime(2012, 4, 30, 23, 59, 59)
            previous_month_start=datetime.datetime(2012, 4, 1, 0, 0, 0)

        """Compensation Cache"""
        compensation_cache={}

        """Get orders in last month"""
        orders=Order.objects.filter(time_stamp__gte=last_3rd_month_start, time_stamp__lte=previous_month_end).order_by('-time_stamp')
        for order in orders:
            """Calculating PCV for previous two months, adding PCV of this month will give pcv quater"""
            if order.time_stamp<previous_month_start:
                if not order.contact.user:
                    try:
                        ordered_via=OrderWebsite.objects.get(order=order).website.user
                        if ordered_via not in compensation_cache:
                            compensation_cache[ordered_via]=self.blank_structure()
                            compensation_cache[ordered_via]['volumes']['pcvq']=compensation_cache[ordered_via]['volumes']['pcvq']+(Decimal(0.65)*order.total)
                    except:
                        pass
                
                """We will not process these data further, as they are for previous month only"""
                continue


            if not order.contact.user:
                """The purchases is not a part of system, commission will be paid to the site owner"""
                try:
                    ordered_via=OrderWebsite.objects.get(order=order).website.user
                    if ordered_via not in compensation_cache:
                        compensation_cache[ordered_via]=self.blank_structure()
                
                    if order not in compensation_cache[ordered_via]['orders']:
                        compensation_cache[ordered_via]['orders'].append({'order':order, 'comp_for':'website_sales'})
                        compensation_cache[ordered_via]['sales']['web']=compensation_cache[ordered_via]['sales']['web']+order.total
                        compensation_cache[ordered_via]['volumes']['pcv']=compensation_cache[ordered_via]['volumes']['pcv']+(Decimal(0.65)*order.total)
                        compensation_cache[ordered_via]['volumes']['prv']=compensation_cache[ordered_via]['volumes']['prv']+order.total
                        compensation_cache[ordered_via]['volumes']['gcv']=compensation_cache[ordered_via]['volumes']['gcv']+(Decimal(0.65)*order.total)
                        compensation_cache[ordered_via]['volumes']['pcvq']=compensation_cache[ordered_via]['volumes']['pcvq']+(Decimal(0.65)*order.total)
                
                except:
                    pass
            else:
                """Find out who all need to be paid"""
                if order.contact.user not in compensation_cache:
                    compensation_cache[order.contact.user]=self.blank_structure()

                compensation_cache[order.contact.user]['orders'].append({'order':order, 'comp_for':'self_sales'})
                compensation_cache[order.contact.user]['sales']['self']=compensation_cache[order.contact.user]['sales']['self']+order.total
                compensation_cache[order.contact.user]['volumes']['prv']=compensation_cache[order.contact.user]['volumes']['prv']+order.total
                compensation_cache[order.contact.user]['volumes']['pcv']=compensation_cache[order.contact.user]['volumes']['pcv']+(Decimal(0.65)*order.total)
                compensation_cache[order.contact.user]['volumes']['gcv']=compensation_cache[order.contact.user]['volumes']['gcv']+(Decimal(0.65)*order.total)
                compensation_cache[order.contact.user]['volumes']['pcvq']=compensation_cache[order.contact.user]['volumes']['pcvq']+(Decimal(0.65)*order.total)

                upline=order.contact.user.upline
                for (counter, user) in enumerate(upline):
                    level=counter+1
                    if user:
                        if user not in compensation_cache:
                            compensation_cache[user]=self.blank_structure()
                        compensation_cache[user]['orders'].append({'order':order, 'comp_for':'l%d'%(level)})
                        compensation_cache[user]['sales']['downline']=compensation_cache[user]['sales']['downline']+order.total
                        compensation_cache[user]['volumes']['gcv']=compensation_cache[user]['volumes']['gcv']+(Decimal(0.65)*order.total)

                        if level not in compensation_cache[user]['sales']['downline_by_level']:
                            compensation_cache[user]['sales']['downline_by_level'][level]=0
                        compensation_cache[user]['sales']['downline_by_level'][level]=compensation_cache[user]['sales']['downline_by_level'][level]+order.total
        
        #ranks=Rank.objects.filter(sequence__gte=3)#accociate consultant and above
        ranks=Rank.objects.all()
        rules=RankRule.objects.filter(rank__in=ranks).order_by('rank__sequence')
        rule_for_star=RankRule.objects.get(rank__sequence=5)
        
        for user, comp in compensation_cache.items():
            """
            we already have pcv and gcv of users.
            """

            """
            1) Checking no. of frontline consultant who are star or better
            2) Active frontline consultant
            """
            frontline=user.frontline
            star_or_better=0
            for u in frontline:
                if u in compensation_cache and  u.gcv>=rule_for_star.minGVC and u.pcv>=rule_for_star.minPCV:
                    """Matched the pcv and gcv criteria for star, now matching other criterias"""
                    """We need min rule_for_star.minActivePE consultants with pcv more than rule_for_star.MinPCVQ"""
                    active_front_line=0
                    fline=u.frontline
                    for fluser in fline:
                        if fluser in compensation_cache and compensation_cache[fluser]['pcv']>rule_for_star.minActivePE:
                            active_front_line=active_front_line+1
                    
                    if active_front_line>=rule_for_star.minActivePE:
                        star_or_better=star_or_better+1                    

            rank_rule=None
            for rule in rules:
                """Conditions"""
                #MinPCVQ=False #Sales vol (PCV) to be active

                MinActivePE=False #Minimum active frontline consultants
                MinStarPE=False #Min frontline consultants with pay rank of star consultant or better
                MinPCVM=False #Current month min. PCV to qualify for downline commission
                MinGVC=False #Min GCV to earn pay fro downline

                if comp['volumes']['gcv']>=rule.minGVC:
                    MinGVC=True
                if comp['volumes']['pcv']>=rule.minPCVM:
                    MinPCVM=True
                if star_or_better>=rule.minStarPE:
                    MinStarPE=True
                    
                min_active_frontline=0
                for u in user.frontline:
                    if u in compensation_cache and compensation_cache[u]['pcv']>=rule.minPCVQ:
                        min_active_frontline=min_active_frontline+1

                if min_active_frontline>=rule.minActivePE:
                    MinActivePE=True
                    
                if MinActivePE and MinStarPE and MinPCVM and MinGVC:
                    rank_rule=rule

            try:
                h=History.objects.get(user=user, month=previous_month_start.month, year=previous_month_start.year)
            except History.DoesNotExist:
                h=History(user=user, month=previous_month_start.month, year=previous_month_start.year)
            
            h.pcv=comp['volumes']['pcv']
            h.prv=comp['volumes']['prv']
            h.gcv=comp['volumes']['gcv']
            #h.personally_enrolled=personally_enrolled
            #h.personally_enrolled_active=personally_enrolled_active
            h.earned_rank=rank_rule.rank
            h.save()
