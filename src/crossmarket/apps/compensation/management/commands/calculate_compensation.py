import datetime, copy
from decimal import *

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.conf import settings


class Command(BaseCommand):
    help='Calculate compensation for a last month.'
    
    def handle(self, *args, **options):
        date=datetime.datetime.now()
        previous_month_end=datetime.datetime(date.year, date.month, 1, 23, 59, 59) - datetime.timedelta(days=1)
        previous_month_start=datetime.datetime(previous_month_end.year, previous_month_end.month, 1, 0, 0, 0)
        calc_comm(previous_month_start, previous_month_end)


def calc_comm(previous_month_start, previous_month_end, single_user=False):
    from satchmo_store.shop.models import Order
    from apps.compensation.models import *
    from apps.dashboard.models import *
    from apps.account.utils import _get_user_volumes_and_ranks_for_month, _get_personally_enrolled_in_a_month


    commisions={}
    com=RankCommission.objects.all()
    for c in com:
        commisions[c.rank]=c

    if single_user:
        users=[{'commission_to':single_user.pk}, ]
    else:
        users=CommissionableOrder.objects.values('commission_to').filter(timestamp__gte=previous_month_start, timestamp__lte=previous_month_end).distinct()

    for user in users:
        user_volumes_and_rank=_get_user_volumes_and_ranks_for_month(User.objects.get(pk=user['commission_to']), previous_month_start.month, previous_month_start.year, True, True)

        user_enrolles=_get_personally_enrolled_in_a_month(User.objects.get(pk=user['commission_to']), previous_month_start.month, previous_month_start.year)

        complan=commisions[user_volumes_and_rank['payrank']]
        rc=user_volumes_and_rank['volume']['prv']*Decimal(complan.ratail_commission)
        vb=Decimal(0) if user_volumes_and_rank['volume']['pcv'] <= 500 else (user_volumes_and_rank['volume']['pcv']*Decimal(complan.volume_bonus_for_500_pcv))
        bs=Decimal(0) if user_volumes_and_rank['volume']['pcv'] <= 300 else (user_volumes_and_rank['volume']['pcv']*Decimal(complan.business_supply_credit_for_300_pcv))
        l1=user_volumes_and_rank['volume']['level_pcvs']['l1']*Decimal(complan.l1)
        l2=user_volumes_and_rank['volume']['level_pcvs']['l2']*Decimal(complan.l2)
        l3=user_volumes_and_rank['volume']['level_pcvs']['l3']*Decimal(complan.l3)
        l4=user_volumes_and_rank['volume']['level_pcvs']['l4']*Decimal(complan.l4)
        l5=user_volumes_and_rank['volume']['level_pcvs']['l5']*Decimal(complan.l5)

        mon=previous_month_start.month
        yea=previous_month_start.year

        try:
            uh=UserHistory.objects.get(user=User.objects.get(pk=user['commission_to']), month=mon, year=yea)
        except UserHistory.DoesNotExist:
            uh=UserHistory(user=User.objects.get(pk=user['commission_to']), month=mon, year=yea)
        uh.earned_rank=user_volumes_and_rank['payrank']
        uh.retail_comm=rc
        uh.volume_bonus=vb
        uh.business_supply_credit=bs
        uh.commissions_on_downline=l1+l2+l3+l4+l5

        uh.prv=user_volumes_and_rank['volume']['prv']
        uh.pcv=user_volumes_and_rank['volume']['pcv']
        uh.pcv_quater=user_volumes_and_rank['volume']['pcv_quarter']
        uh.gcv=user_volumes_and_rank['volume']['gcv']

        uh.l1_pcv=user_volumes_and_rank['volume']['level_pcvs']['l1']
        uh.l2_pcv=user_volumes_and_rank['volume']['level_pcvs']['l2']
        uh.l3_pcv=user_volumes_and_rank['volume']['level_pcvs']['l3']
        uh.l4_pcv=user_volumes_and_rank['volume']['level_pcvs']['l4']
        uh.l5_pcv=user_volumes_and_rank['volume']['level_pcvs']['l5']

        uh.personally_enrolled=user_enrolles
        uh.personally_enrolled_active=user_volumes_and_rank['enrolles']['active']
        uh.save()
