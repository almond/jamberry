import datetime, copy
from decimal import *

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from satchmo_store.shop.models import Order

from apps.compensation.models import *
from apps.dashboard.models import *

class Command(BaseCommand):
    help='Calculate compensation for a Period.'
    debug=True
    
    def blank_structure(self):
        #structure: comp_cache[user]={'orders':[{'order':'', 'comp_amt':'', 'comp_for':''}, {}, {}], 'sales':{'self':0, 'downline':0, 'web':0}}
        return {'orders':[], 'sales':{'self':0, 'downline':0, 'web':0, 'downline_by_level':{}}, 'volumes':{'pcv':0, 'gcv':0}}

    def handle(self, *args, **options):
        """
        Getting period for which the commissions should be paid out.
        Since their Periods table makes no sense right now, I'll do it for last month's first day to last day.
        """
  
        
        """Getting details of last month"""
        date=datetime.datetime.now()
        previous_month_end=datetime.datetime(date.year, date.month, 1, 23, 59, 59) - datetime.timedelta(days=1)
        previous_month_start=datetime.datetime(previous_month_end.year, previous_month_end.month, 1, 0, 0, 0)

        if self.debug:
            previous_month_end=datetime.datetime(2012, 4, 30, 23, 59, 59)
            previous_month_start=datetime.datetime(2012, 4, 1, 0, 0, 0)

        """Compensation Cache"""
        compensation_cache={}
        #compensations_for: self_sales, website_sales, l1, l2, l3, l4, l5

        """Get orders in last month"""
        orders=Order.objects.filter(time_stamp__gte=previous_month_start, time_stamp__lte=previous_month_end).order_by('-time_stamp')
        for order in orders:
            if not order.contact.user:
                """The purchases is not a part of system, commission will be paid to the site owner"""
                try:
                    ordered_via=OrderWebsite.objects.get(order=order).website.user
                    if ordered_via not in compensation_cache:
                        compensation_cache[ordered_via]=self.blank_structure()
                
                    if order not in compensation_cache[ordered_via]['orders']:
                        compensation_cache[ordered_via]['orders'].append({'order':order, 'comp_for':'website_sales'})
                        compensation_cache[ordered_via]['sales']['web']=compensation_cache[ordered_via]['sales']['web']+order.total
                        compensation_cache[ordered_via]['volumes']['pcv']=compensation_cache[ordered_via]['volumes']['pcv']+(Decimal(0.65)*order.total)
                except:
                    pass
            else:
                """Find out who all need to be paid"""
                if order.contact.user not in compensation_cache:
                    compensation_cache[order.contact.user]=self.blank_structure()

                compensation_cache[order.contact.user]['orders'].append({'order':order, 'comp_for':'self_sales'})
                compensation_cache[order.contact.user]['sales']['self']=compensation_cache[order.contact.user]['sales']['self']+order.total
                compensation_cache[order.contact.user]['volumes']['pcv']=compensation_cache[order.contact.user]['volumes']['pcv']+(Decimal(0.65)*order.total)
                compensation_cache[order.contact.user]['volumes']['gcv']=compensation_cache[order.contact.user]['volumes']['gcv']+(Decimal(0.65)*order.total)
                
                """
                print "\n"
                print order.contact.user
                print order.contact.user.sponsor
                print order.contact.user.upline
                print order.contact.user.five_level_upline
                """

                upline=order.contact.user.upline
                for (counter, user) in enumerate(upline):
                    level=counter+1
                    if user:
                        if user not in compensation_cache:
                            compensation_cache[user]=self.blank_structure()
                        compensation_cache[user]['orders'].append({'order':order, 'comp_for':'l%d'%(level)})
                        compensation_cache[user]['sales']['downline']=compensation_cache[user]['sales']['downline']+order.total
                        compensation_cache[user]['volumes']['gcv']=compensation_cache[user]['volumes']['gcv']+(Decimal(0.65)*order.total)

                        if level not in compensation_cache[user]['sales']['downline_by_level']:
                            compensation_cache[user]['sales']['downline_by_level'][level]=0
                        compensation_cache[user]['sales']['downline_by_level'][level]=compensation_cache[user]['sales']['downline_by_level'][level]+order.total
        
        ranks=Rank.objects.filter(sequence__gte=3)#accociate consultant and above
        rules=RankRule.objects.filter(rank__in=ranks).order_by('rank__sequence')

        for user, comp in compensation_cache.items():            
            """Dummy Data"""
            d_MinPCVQ=300
            d_MinActivePE=0
            d_MinStarPE=1000
            d_MinPCVM=1000
            d_MinGVC=1000

            rank_rule=None
            for rule in rules:
                """Conditions"""
                MinPCVQ=False #Sales vol (PCV) to be active
                MinActivePE=False #Minimum active frontline consultants
                MinStarPE=False #Min frontline consultants with pay rank of star consultant or better
                MinPCVM=False #Current month min. PCV to qualify for downline commission
                MinGVC=False #Min GCV to earn pay fro downline

                if d_MinPCVQ>=rule.minPCVQ:
                    MinPCVQ=True
                if d_MinActivePE>=rule.minActivePE:
                    MinActivePE=True
                if d_MinStarPE>=rule.minStarPE:
                    MinStarPE=True
                if d_MinPCVM>=rule.minPCVM:
                    MinPCVM=True
                if d_MinGVC>=rule.minGVC:
                    MinGVC=True
                    
                if MinPCVQ and MinActivePE and MinStarPE and MinPCVM and MinGVC:
                    rank_rule=rule

            print "\n%s (%s): %s"%(str(user), str(rank_rule.rank.name) if rank_rule else '', str(comp))
        
            
