from django.core.management.base import BaseCommand, CommandError
import os, csv, random
from crossmarket.apps.compensation.models import *
from django.conf import settings
from decimal import *
class Command(BaseCommand):
    help='Import Ranks from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_rank_rules.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                rc=RankCommission.objects.get(pk=row[0])
            except RankCommission.DoesNotExist:
                rc=RankCommission(pk=row[0])
            
            rc.rank=Rank.objects.get(pk=row[3])
            rc.ratail_commission=row[9]
            rc.volume_bonus_for_500_pcv=row[10]
            rc.business_supply_credit_for_300_pcv=row[11]
            rc.l1=row[12]
            rc.l2=row[13]
            rc.l3=row[14]
            rc.l4=row[15]
            rc.l5=row[16]
            rc.new_payrank_maintainance_bonus=row[17].strip().lstrip('$').replace(',', '')
            rc.save()
