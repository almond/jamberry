from django.core.management.base import BaseCommand, CommandError
import os, csv, random
from crossmarket.apps.compensation.models import *
from django.conf import settings
from decimal import *
class Command(BaseCommand):
    help='Import Ranks from Old System\'s CSV.'

    def handle(self, *args, **options):
        filename=os.path.abspath('%s/../../resources/jamberry_db_faststart.csv' % (settings.DIRNAME))
        data=csv.reader(open(filename, 'rb'), delimiter=',')
        first=True
        for row in data:
            if first:
                first=False
                continue

            try:
                fs=FastStart.objects.get(user=User.objects.get(pk=row[0]))
            except User.DoesNotExist:
                continue
            except FastStart.DoesNotExist:
                fs=FastStart(user=User.objects.get(pk=row[0])) 

            enroll_date=row[1].split('-')
            fs.enroll_date="%s-%s-%s"%(enroll_date[0], enroll_date[1], enroll_date[2])

            fs.personally_enrolled=row[2]

            fs.pv_month_1=row[3]
            fs.pv_month_2=row[4]
            fs.pv_month_3=row[5]

            fs.parties_month_1=row[6]
            fs.parties_month_2=row[7]
            fs.parties_month_3=row[8]

            fs.bonus_month_1=row[9] if row[9]!='' else 0.00
            fs.bonus_month_2=row[10] if row[10]!='' else 0.00
            fs.bonus_month_3=row[11] if row[11]!='' else 0.00

            fs.credit_month_1=row[12] if row[12]!='' else 0.00
            fs.credit_month_2=row[13] if row[13]!='' else 0.00
            fs.credit_month_3=row[14] if row[14]!='' else 0.00

            fs.bonus_sponsor=row[15] if row[15]!='' else 0.00
            fs.bonus_final=row[16] if row[16]!='' else 0.00
            fs.credit_final=row[17] if row[17]!='' else 0.00

            dc=row[18].split('/')
            fs.date_created="%s-%s-%s"%(dc[2], dc[1], dc[0])
            
            
            fs.achieved_month_1=not (False if row[19]=='-1' else True)
            fs.achieved_month_2=not (False if row[20]=='-1' else True)
            fs.achieved_month_3=not (False if row[21]=='-1' else True)
            fs.achieved_all=not (False if row[22]=='-1' else True)
            """
            print False if row[19]=='-1' else True
            print fs.achieved_month_1
            
            print type(row[19]), ': ', fs.achieved_month_1
            print row[20], ': ', fs.achieved_month_2
            print row[21], ': ', fs.achieved_month_3
            
            exit(0)
            """
            fs.ended_month_1=not (False if row[23]=='-1' else True)
            fs.ended_month_2=not (False if row[24]=='-1' else True)
            fs.ended_month_3=not (False if row[25]=='-1' else True)

            fs.redeemed_month_1=False if row[26]=='-1' else True
            fs.redeemed_month_2=False if row[27]=='-1' else True
            fs.redeemed_month_3=False if row[28]=='-1' else True
            fs.redeemed_all=False if row[29]=='-1' else True

            fs.type_month_1=False if row[30]=='-1' else True
            fs.type_month_2=False if row[31]=='-1' else True
            fs.type_month_3=False if row[32]=='-1' else True
            fs.type_all=False if row[33]=='-1' else True

            
            fs.achieved_month_1_date=(row[34]) if row[34]!='' else None
            fs.achieved_month_2_date=(row[35]) if row[35]!='' else None
            fs.achieved_month_3_date=(row[36]) if row[36]!='' else None
            fs.achieved_month_all_date=(row[37]) if row[37]!='' else None

            fs.redeemed_month_1_date=getdate(row[38]) if row[38]!='' else None
            fs.redeemed_month_2_date=getdate(row[39]) if row[39]!='' else None
            fs.redeemed_month_3_date=getdate(row[40]) if row[40]!='' else None
            fs.redeemed_month_all_date=getdate(row[41]) if row[41]!='' else None
            
            fs.recognized_month_1=False if row[42]=='-1' else True
            fs.recognized_month_2=False if row[43]=='-1' else True
            fs.recognized_month_3=False if row[44]=='-1' else True
            fs.recognized_all=False if row[45]=='-1' else True

            fs.save()

            def getdate(str_date):
                sd=str_date.split('/')
                return '%s-%s-%s'%(sd[2], sd[1], sd[0])
