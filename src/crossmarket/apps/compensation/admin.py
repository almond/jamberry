from django.contrib import admin
from models import *

admin.site.register(PartyOrder)
admin.site.register(PartyOrderDetails)
admin.site.register(CommissionableOrder)
admin.site.register(Rank)
admin.site.register(Period)
admin.site.register(DateDetails)
admin.site.register(UserTitle)
admin.site.register(BSCLog)
admin.site.register(HostingReward)

class FastStartAdmin(admin.ModelAdmin):
    search_fields=['user__username', 'user__id']
    raw_fields=['user', ]

class CreditAdmin(admin.ModelAdmin):
    search_fields=['user__username', 'user__id']
    list_display=('user', 'credit_type', 'transaction_type', 'timestamp', 'amount')
    raw_fields=['user', ]

class UserHistoryAdmin(admin.ModelAdmin):
    search_fields=['user__username', 'user__id']
    list_display=('user', 'year', 'month', 'earned_rank')
    raw_fields=['user', ]

class EarningAdmin(admin.ModelAdmin):
    search_fields=['user__username', 'user__id']
    list_display=('user', 'year', 'month', )
    raw_fields=['user', ]

class RankCommissionAdmin(admin.ModelAdmin):
    list_display=('rank', 'ratail_commission', 'volume_bonus_for_500_pcv', 'business_supply_credit_for_300_pcv', 'l1', 'l2', 'l3', 'l4', 'l5', 'new_payrank_maintainance_bonus')

class RankRequirementAdmin(admin.ModelAdmin):
    list_display=('rank', 'pcv_to_be_active', 'min_frontline_active', 'min_frontline_star_or_above', 'min_pcv_for_month', 'min_gcv_for_month')

admin.site.register(FastStart, FastStartAdmin)
admin.site.register(Credit, CreditAdmin)
admin.site.register(Earning, EarningAdmin)
admin.site.register(UserHistory, UserHistoryAdmin)
admin.site.register(RankCommission, RankCommissionAdmin)
admin.site.register(RankRequirement, RankRequirementAdmin)
