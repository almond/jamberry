from django.utils import simplejson
from django.http import HttpResponse
from satchmo_store.shop.models import Cart, NullCart
from django.conf import settings

def ajax_render(data):
    return HttpResponse(simplejson.dumps(data, ensure_ascii=False), mimetype='application/javascript')

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def get_items_in_cart(request, all_data=False):
    items={}
    cart=Cart.objects.from_request(request=request, create=False)
    if cart.__class__==NullCart:
        return {}
    elif cart.is_empty:
        return {}
    else:
        all_items=cart.cartitem_set.all()
        for i in all_items:
            #print dir(i.product.unit_price)
            if (i.product.slug in settings.DISC_PRODUCT_SLUGS) or i.quantity==0:
                i.delete()
            else:
                if not all_data:
                    items[int(i.product_id)]=int(i.quantity)
                else:
                    items[int(i.product_id)]={'quantity': int(i.quantity), 'unit_price':"%.2f"%i.product.unit_price, 'slug':i.product.slug}
        return items
