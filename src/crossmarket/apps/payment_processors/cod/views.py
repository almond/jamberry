"""Simple wrapper for standard checkout as implemented in payment.views"""
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from livesettings import config_get_group
from payment.views import confirm, payship
    
cod = config_get_group('PAYMENT_COD')

@csrf_exempt
def pay_ship_info(request):
    return payship.credit_pay_ship_info(request, cod)
#pay_ship_info = never_cache(pay_ship_info)

@csrf_exempt
def confirm_info(request):
    return confirm.credit_confirm_info(request, cod)
#confirm_info = never_cache(confirm_info)

