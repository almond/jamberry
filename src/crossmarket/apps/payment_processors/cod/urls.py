from django.conf.urls.defaults import patterns
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     (r'^$', 'apps.payment_processors.cod.views.pay_ship_info', {'SSL':ssl}, 'COD_satchmo_checkout-step2'),
     (r'^confirm/$', 'apps.payment_processors.cod.views.confirm_info', {'SSL':ssl}, 'COD_satchmo_checkout-step3'),
     (r'^success/$', 'payment.views.checkout.success', {'SSL':ssl}, 'COD_satchmo_checkout-success'),
)
