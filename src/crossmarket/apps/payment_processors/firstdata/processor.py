from payment.modules.base import BasePaymentProcessor, ProcessorResult
from django.utils.translation import ugettext_lazy as _
import urllib2

class PaymentProcessor(BasePaymentProcessor):
    def __init__(self, settings):
        super(PaymentProcessor, self).__init__('firstdata', settings)
        self.live=settings.LIVE.value
        if self.live:
            self.store_number=settings.STORE_NUMBER.value
            self.post_url=settings.POST_URL.value
        else:
            self.store_number=settings.TEST_STORE_NUMBER.value
            self.post_url=settings.POST_TEST_URL.value
        
    def get_standard_charge_data(self, amount=None):
        print self.post_url
        return {'connection':self.post_url, 'logPostString':'test-string', 'postString':'test=test'}

    def send_post(self, data, testing=False, amount=None):
        self.log.info("About to send a request to firstdata: %(connection)s\n%(logPostString)s", data)
        conn = urllib2.Request(url=data['connection'], data=data['postString'])
        try:
            f = urllib2.urlopen(conn)
            all_results = f.read()
            print all_results
            self.log_extra('FirstData response: %s', all_results)
        except urllib2.URLError, ue:
            self.log.error("error opening %s\n%s", data['connection'], ue)
            result=ProcessorResult(self.key, False, _('Could not talk to firstdata gateway'))
            print result
            return result
        
    def capture_payment(self, testing=False, order=None, amount=None):
        if order:
            self.prepare_data(order)
        else:
            order = self.order

        if order.paid_in_full:
            self.log_extra('%s is paid in full, no capture attempted.', order)
            results = ProcessorResult(self.key, True, _("No charge needed, paid in full."))
            self.record_payment()
        else:
            self.log_extra('Capturing payment for %s', order)
            standard=self.get_standard_charge_data(amount=amount)
            results=self.send_post(standard, testing)
        return results
