from django.utils.translation import ugettext_lazy as _
from livesettings import *

PAYMENT_GROUP = ConfigurationGroup('PAYMENT_FIRSTDATA', _('First Data Payment Module Settings'), ordering = 99999)

config_register_list(
    BooleanValue(PAYMENT_GROUP,
        'LIVE',
        description=_("Accept real payments"),
        help_text=_("False if you want to be in test mode"),
        default=False),

    StringValue(PAYMENT_GROUP,
        'POST_URL',
        description=_('First Data Post URL'),
        help_text=_('The First Data URL for real transaction posting.'),
        default=""),

    StringValue(PAYMENT_GROUP,
        'POST_TEST_URL',
        description=_('First Data Test Post URL'),
        help_text=_('The First Data URL for test transaction posting.'),
        default=""),

    MultipleStringValue(PAYMENT_GROUP,
        'CREDITCHOICES',
        description=_('Available credit cards'),
        choices = (
            (('American Express', 'American Express')),
            (('Visa','Visa')),
            (('Mastercard','Mastercard')),
            (('Discover','Discover'))),
        default = ('American Express', 'Visa', 'Mastercard', 'Discover')),
    
    StringValue(PAYMENT_GROUP,
        'STORE_NUMBER',
        description=_('First Data Store Number'),
        default=""),

    StringValue(PAYMENT_GROUP,
        'TEST_STORE_NUMBER',
        description=_('First Data Test Store Number'),
        default=""),

    ModuleValue(PAYMENT_GROUP,
        'MODULE',
        description=_('Implementation module'),
        hidden=True,
        default = 'crossmarket.apps.payment_processors.firstdata'),

    StringValue(PAYMENT_GROUP,
        'KEY',
        description=_("Module key"),
        hidden=True,
        default = 'FIRSTDATA'),

    StringValue(PAYMENT_GROUP,
        'LABEL',
        description=_('English name for this group on the checkout screens'),
        default = 'First Data Payment',
        help_text = _('This will be passed to the translation utility')),

    StringValue(PAYMENT_GROUP,
        'URL_BASE',
        description=_('The url base used for constructing urlpatterns which will use this module'),
        default = '^first-data/'),

    BooleanValue(PAYMENT_GROUP,
        'EXTRA_LOGGING',
        description=_("Verbose logs"),
        help_text=_("Add extensive logs during post."),
        default=False)

)

