from decimal import Decimal
from livesettings import config_value
from apps.api_wrapper.wrapper import AvaTaxApi
from apps.geo.models import ZipCode
from django.conf import settings

class Processor(object):
    
    method="percent"
    
    def __init__(self, order=None, user=None, zip_code="84101"):
        """
        Any preprocessing steps should go here
        For instance, copying the shipping and billing areas

        Added zip_code paramater to be able to call it externally
        """
        # Set up the API library
        avatax_client = AvaTaxApi(settings.API_AVATAX["account_number"], settings.API_AVATAX["licence_key"], version=settings.API_AVATAX["version"])

        if order:
            # get contact info from order
            zip_code = order.contact.addressbook_set.all()[0].postal_code
            order_subtotal = Decimal(order.sub_total)
        elif user:
            # get contact info from user
            if user.contact:
                zip_code = user.contact.addressbook_set.all()[0].postal_code
                order_subtotal = Decimal(10)
        else:
            order_subtotal = Decimal(10)

        geo = ZipCode.objects.get(zip_code=zip_code)
        latlng = (geo.latitude, geo.longitude)
            
        response, content = avatax_client.geographical_tax(order_subtotal, str(latlng[0]), str(latlng[1]))
        self.percent = content["Tax"] * 100
        self.rate = content["Rate"] * 100
        self.order = order
        self.user = user


    def by_orderitem(self, orderitem):
        if orderitem.product.taxable:
            price = orderitem.sub_total
            return self.by_price(orderitem.product.taxClass, price)
        else:
            return Decimal("0.00")         


    def by_price(self, taxclass, price):
        #percent = config_value('TAX','PERCENT')
        #p = price * (percent/100) # Changed this
        p = price * Decimal(self.rate/100)
        return p

        
    def by_product(self, product, quantity=Decimal('1')):
        price = product.get_qty_price(quantity)
        taxclass = product.taxClass
        return self.by_price(taxclass, price)

        
    def get_percent(self, *args, **kwargs):
        return Decimal(self.percent)

    
    def get_rate(self, *args, **kwargs):
        return Decimal(self.rate)

        
    def shipping(self, subtotal=None):
        if subtotal is None and self.order:
            subtotal = self.order.shipping_sub_total

        if subtotal:
            subtotal = self.order.shipping_sub_total
            if config_value('TAX','TAX_SHIPPING'):
                percent = Decimal(self.percent)
                t = subtotal * (percent/100)
            else:
                t = Decimal("0.00")
        else:
            t = Decimal("0.00")
                
        return t


    def process(self, order=None):
        """
        Calculate the tax and return it
        """
        if order:
            self.order = order
        else:
            order = self.order

        percent = Decimal(self.rate)

        sub_total = Decimal("0.00")
        for item in order.orderitem_set.filter(product__taxable=True):
            sub_total += item.sub_total
        
        itemtax = sub_total * (percent/100)
        taxrates = {'%i%%' % percent :  itemtax}
        
        if config_value('TAX','TAX_SHIPPING'):
            shipping = order.shipping_sub_total
            sub_total += shipping
            ship_tax = shipping * (percent/100)
            taxrates['Shipping'] = ship_tax
        
        tax = sub_total * (percent/100)
        return tax, taxrates
