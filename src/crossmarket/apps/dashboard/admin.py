from django.contrib import admin

from apps.dashboard.models import OrderWebsite, OrderType, Section, CmsContent

class SectionInline(admin.StackedInline):
    verbose_name = "Sub-Section"
    prepopulated_fields = {"slug": ("title",)}
    model = Section
    extra = 0


class OrderWebsiteAdmin(admin.ModelAdmin):
    list_display=('order', 'website')
    raw_id_fields=['order', 'website']


class OrderTypeAdmin(admin.ModelAdmin):
    raw_id_fields=['order']


class SectionAdmin(admin.ModelAdmin):
    list_display=('title',)
    prepopulated_fields = {"slug": ("title",)}
    inlines = [SectionInline]

    fieldsets = (
        ("Section", {
            'fields': ('title', 'slug', 'description', 'ordering', 'is_enabled',)
        }),
    )

    def queryset(self, request):
        """
        Only show top level category items.
        """
        qs = super(SectionAdmin, self).queryset(request)
        return qs.filter(parent__isnull=True)

admin.site.register(CmsContent)
admin.site.register(Section, SectionAdmin)
admin.site.register(OrderWebsite, OrderWebsiteAdmin)
admin.site.register(OrderType, OrderTypeAdmin)
