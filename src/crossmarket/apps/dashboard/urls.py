from django.conf.urls.defaults import patterns, include, url

from apps.dashboard.views import *


urlpatterns = patterns('',
    #Support
    url(r'^commissions/$', view_commissions, {}, name="dashboard_view_commissions"), 
    url(r'^support/view/(?P<support_id>[\d_]+)/$', view_support, {}, name="dashboard_view_support"), 

    #Events
    url(r'^parties/requested/$', requested_parties, name="dashboard_requested_parties"),
                   

    # Reports
    url(r'reports/re-overview/$', report_overview, name="dashboard_report_overview"),
    url(r'reports/re-new-users-per-day/$', report_new_users_per_day, name="dashboard_report_new_users_per_day"),
    url(r'reports/re-sales-by-consultants/$', report_sales_by_consultants, name="dashboard_report_sales_by_consultants"),
    url(r'reports/re-faststart/$', report_faststart, name="dashboard_report_faststart"),
    url(r'reports/re-top-recruiter/$', report_top_recruiter, name="dashboard_report_top_recruiter"),
    url(r'reports/re-members-per-state/$', report_members_per_state, name="dashboard_report_members_per_state"),
    url(r'reports/detailed-bonuses/$', report_detailed_bonuses, name="dashboard_report_detailed_bonuses"),
    url(r'reports/detailed-metrices/$', report_detailed_metrices, name="dashboard_report_detailed_metrices"),
    url(r'reports/detailed-earning/$', report_detailed_earning, name="dashboard_report_detailed_earning"),


                       

    # Dashboard search
    url(r'search/?$', search, name="dashboard_search"),

    # Dashboard profile
    (r'^profile/(?P<user_id>[\d_]+)/$', profile, {}, 'dashboard_profile'),
    (r'^profile/contact/(?P<contact_id>[\d_]+)/$', contact_profile, {}, 'dashboard_contact'),

    (r'^profile/(?P<user_id>[\d_]+)/(?P<type>[-\w]+)/add/$', add, {}, 'dashboard_profile_add'),
    (r'^profile/(?P<data_id>[\d_]+)/(?P<type>[-\w]+)/update/$', update, {}, 'dashboard_profile_update'),
    (r'^profile/(?P<user_id>[\d_]+)/(?P<type>[-\w]+)/delete/$', delete, {}, 'dashboard_profile_delete'),

    # Dashboard profile
    (r'^virtual-login/(?P<user_id>[\d_]+)/$', virtual_login, {}, 'dashboard_virtuallogin'),
    (r'^re-login-as-admin/(?P<admin_user_id>[\d_]+)/$', re_login_as_admin, {}, 'dashboard_re_login_as_admin'),

    # Sections
    url(r'commissions/$', commision, name="dashboard_commisions"),
    url(r'communications/$', communicate, name="dashboard_communications"),
    # url(r'products/$', section, {'section': 'products'}, name="dashboard_products"),
    # url(r'reports/$', section,  {'section': 'reports'}, name="dashboard_reports"),
    # url(r'store/$', section, {'section': 'products'}, name="dashboard_store"),
    # url(r'settings/$', settings, name="dashboad_settings"),

    # Account login logout password reset
    url(r'^login/$', dashboard_login, name='dashboard_login'),
    url(r'^logout/$', dashboard_logout, name='dashboard_logout'),
    url(r'^password-change/$', dashboard_change_password, name='dashbaord_change_password'),

    # Top level section items with out a sub-section.
    url(r'(?P<section>documents|users|content-management|commision)/add/$', content_manager, name="dashboard_top_section_add"),
    url(r'(?P<section>documents|users|content-management|commision)/(?P<object_id>[\d]+)/$', content_manager, name="dashboard_top_section_edit"),
    url(r'(?P<section>documents|users|content-management|commision)/(?P<object_id>[\d]+)/delete/$', content_removal, name="dashboard_top_section_delete"),
    url(r'(?P<section>documents|users|content-management|commision)/$', content_list, name="dashboard_top_section"),
    
    # Sub-Section List, add, edit, & delete views
    url(r'(?P<section>[-\w]+)/(?P<sub_section>[-\w]+)/add/$', content_manager, name="dashboard_section_add"),
    url(r'(?P<section>[-\w]+)/(?P<sub_section>[-\w]+)/(?P<object_id>[\d]+)/$', content_manager, name="dashboard_section_edit"),
    url(r'(?P<section>[-\w]+)/(?P<sub_section>[-\w]+)/(?P<object_id>[\d]+)/delete/$', content_removal, name="dashboard_section_delete"),
    url(r'(?P<section>[-\w]+)/(?P<sub_section>[-\w]+)/$', content_list, name="dashboard_section_list"),

    # Top level sections with out a sub-section.
    url(r'(?P<section>[-\w]+)/add/$', section, name="dashboard_section_add"),
    url(r'(?P<section>[-\w]+)/(?P<object_id>[\d]+)/$', section, name="dashboard_section_edit"),
    url(r'(?P<section>[-\w]+)/(?P<object_id>[\d]+)/delete/$', section, name="dashboard_section_delete"),

    # Dashboard section
    url(r'(?P<section>[-\w]+)/$', section, name="dashboard_section"),

    # Dashboard Home
    url(r'$', section, name="dashboard"),
)
