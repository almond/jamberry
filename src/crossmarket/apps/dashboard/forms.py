import re

from django import forms
from django.contrib.auth.models import User
from django.contrib.flatpages.models import FlatPage
from django.contrib import admin
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from django.forms.models import inlineformset_factory

from product.modules.subscription.models import SubscriptionProduct
from product.models import Category, Product, Discount, ProductImage

from satchmo_store.shop.models import *
from satchmo_store.contact.models import Contact, AddressBook, ContactRole, PhoneNumber
from satchmo_utils.thumbnail.field import ImageWithThumbnailField
from satchmo_utils.thumbnail.widgets import AdminImageWithThumbnailWidget

from django.utils.html import escape

from apps.events.models import Event
from apps.notice.models import Notice
from apps.compensation.models import Credit
from apps.account.models import UserType
# This is not installed. Maybe we are using something else to handle subscriptions.
# from satchmo_store.shop.addons.models import Subscription

from apps.archive.models import Document


ATTR_DICT = {'class': 'required'}
USERNAME_RE = re.compile(r'^\w+$')

# class UserForm(forms.ModelForm):    
#     class Meta:
#         model = User
#         #fields = ('username', 'first_name', 'last_name', 'email')
#         exclude = ('is_staff', 'is_active', 'is_superuser')

#CreditFormset=inlineformset_factory(User, Credit)
ContactFormset=inlineformset_factory(User, Contact, can_delete=False, extra=1, exclude=('is_primary',))
AddressFormset=inlineformset_factory(Contact, AddressBook, can_delete=False, extra=1, exclude=('description',))
#UserTypeFormset=modelformset_factory(UserType, form=UserForm,fields=('username', 'first_name', 'last_name', 'email'), can_delete=False, extra=0)

class UserTypeForm(forms.ModelForm):
    class Meta:
        model=UserType
        exclude=('user')

class NoticeForm(ModelForm):
    class Meta:
        model = Notice

class CreditForm(ModelForm):
    class Meta:
        model = Credit
        exclude = ('timestamp', 'user', )


class OrderAuthorizationForm(ModelForm):
    class Meta:
        model = OrderAuthorization


class OrderPaymentForm(ModelForm):
    class Meta:
        model = OrderPayment


class EventForm(ModelForm):
    class Meta:
        model = Event


class OrderForm(ModelForm):
    class Meta:
        model = Order


class CartItemForm(ModelForm):
    class Meta:
        model = CartItem


class CartForm(ModelForm):
    class Meta:
        model = Cart


class ConfigForm(ModelForm):
    class Meta:
        model = Config

class OrderItemForm(ModelForm):
    class Meta:
        model = OrderItem


class CategoryForm(ModelForm):
    class Meta:
        model = Category


class FlatPageForm(ModelForm):
    class Meta:
        model = FlatPage
        exclude = (
            'enable_comments', 'template_name', 'registration_required',
        )


class DiscountForm(ModelForm):
    class Meta:
        model = Discount


class CommunicationForm(forms.Form):
    to = forms.CharField()
    subject = forms.CharField()
    # subscription_type = forms.ChoiceField()
    # subscription = forms.ChoiceField()
    # level = forms.ChoiceField()
    body = forms.CharField()

    def send(self):
        pass


class DocumentForm(ModelForm):
    class Meta:
        model = Document

class SubscriptionProductForm(ModelForm):
    class Meta:
        model = SubscriptionProduct


class ProductImage_Inline(admin.StackedInline):
    model = ProductImage
    extra = 3

    formfield_overrides = {
        ImageWithThumbnailField : {'widget' : AdminImageWithThumbnailWidget},
    }


class ProductForm(ModelForm):
    class Meta:
        model = Product
        inlines=[ProductImage_Inline, ]



class OrderForm(ModelForm):
    class Meta:
        model = Order


class UserForm(ModelForm):
    """
    Add extra fields to the User form used by the modelformset in the views.
    """
    phone_number = forms.CharField(max_length=40, required=False)
    promo_page = forms.CharField(max_length=1000, widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        """
        If present, load the value of the profile.phone_number field belonging to the user
        """
        if 'instance' in kwargs:
            initial = kwargs.setdefault("initial", {})
            initial["phone_number"] = kwargs.get("instance").profile.phone_number
            initial["promo_page"] = kwargs.get("instance").profile.promo_page

        return super(UserForm, self).__init__(*args, **kwargs)


    # def clean_promo_page(self):
    #     """
    #     For some weird reason django won't let me type any text on it's textwidget. 
    #     Doing escaping myself
    #     """


        

    def save(self, *args, **kwargs):
        """
        Store Phone Number in the Profile model
        """
        instance = super(UserForm, self).save(*args, **kwargs)
        profile = instance.profile
        profile.phone_number = self.cleaned_data["phone_number"]
        profile.promo_page = self.cleaned_data["promo_page"]
        profile.save()

        return instance


    class Meta:
        model = User
        fields = (
            "username", "first_name", "last_name", "email",
        )
        # exclude = (
        #     'password', 'last_login', 'date_joined',
        #     # We may want these 2 items back in at some 
        #     # point for now they are commented out.
        #     'groups','user_permissions'
        # )
        

class AuthenticationRememberMeForm(AuthenticationForm):
    remember_me = forms.BooleanField(initial=False, required=False)


class ChangePasswordForm(forms.Form):
    def __init__(self, user=None, *args, **kwargs):
        self._user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    old_password = forms.CharField(label="Old password",
        widget=forms.PasswordInput(attrs=ATTR_DICT))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=ATTR_DICT),
        label='New password')
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=ATTR_DICT),
        label='New password (again)')
    _user = None

    def clean_old_password(self):
        old_password = self.data['old_password']
        if not self._user.check_password(old_password):
            raise forms.ValidationError("current password is invalid")
        return old_password

    def clean_password2(self):
        password1 = self.data['password1']
        password2 = self.data['password2']

        if not password1 == password2:
            raise forms.ValidationError("new passwords do not match")

        if not len(password1) >=8:
            raise forms.ValidationError("Passwords must be at least 8 characters long")

        return password2

    def save(self):
        password = self.cleaned_data.get('password1', self.data['password1'])
        self._user.set_password(password)
        self._user.save()
        return self._user
