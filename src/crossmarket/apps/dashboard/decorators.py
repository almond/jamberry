from functools import wraps

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.urlresolvers import reverse

from apps.dashboard.forms import AuthenticationRememberMeForm


def staff_authenication_required(view_func):
    @wraps(view_func)
    def _checklogin(request, *args, **kwargs):
        if request.user.is_active and request.user.is_staff:
            return view_func(request, *args, **kwargs)

        from apps.dashboard.views import dashboard_login
        redirect_to = request.META.get('PATH_INFO', reverse('dashboard'))
        return dashboard_login(request, next=redirect_to)
    return _checklogin
