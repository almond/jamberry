import datetime, time, hashlib

from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.contrib.flatpages.models import FlatPage
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory, modelformset_factory
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Q, Sum
from django.shortcuts import get_object_or_404
from django.contrib.auth import login, authenticate
from django.views.decorators.http import require_GET, require_POST
from django.contrib.admin.views.decorators import staff_member_required
#from django.forms.models import inlineformset_factory, modelformset_factory


from satchmo_store.contact.models import Contact, AddressBook, ContactRole, PhoneNumber
from satchmo_store.shop.models import Order

from product.models import Category, Product
from product.utils import find_best_auto_discount
from product.signals import index_prerender

from apps.events.models import Event
from apps.archive.models import Document
#from apps.dashboard.decorators import staff_authenication_required
from apps.dashboard.forms import *
from apps.dashboard.models import Section
from apps.dashboard.utils import paginate
from apps.dashboard.config import SECTION_SETTINGS
from apps.utils import *
from apps.account.models import *
from apps.compensation.models import *
from apps.support.models import *
from apps.account.utils import _get_user_volumes_and_ranks_for_month, _get_downline_rank_and_volume_for_month, get_commissionable_order
from apps.account.models import Profile, UserType
from apps.pricing.models import Offer
from apps.support.models import SupportRequest
import math, csv, time
from django.template import defaultfilters

from apps.support.models import SUPPORT_REQUEST_STATUS_CHOICES 

def get_month_list(fr, to):
    month_list=[]
    while fr<=to:
        month_list.append('%d-%d'%(fr.year, fr.month))
        fr=datetime.datetime((fr.year if fr.month!=12 else fr.year+1), (fr.month+1 if fr.month!=12 else 1), 1, 0, 0, 0)
    return month_list

def report_detailed_earning(request):
    table_title=['ID', 'Name', 'Mon', 'Year', 'EarningType', 'Desc', 'Source', 'Earning Basis', 'Earning %age', 'Earning Amt.', 'Source User ID', 'Source Order ID']
    table_data=[]    
    fr=request.GET.get('from', False)
    to=request.GET.get('to', False)
    report=False
    try:
        q=Q()
        if fr:
            fr=fr.split('-')
            fr=datetime.datetime(int(fr[0]), int(fr[1]), 1, 0, 0, 0)
            q=Q(month=fr.month) & Q(year=fr.year)

        if to:
            to=to.split('-')
            to=datetime.datetime(int(to[0]), int(to[1]), 1, 0, 0, 0)
            if to>=fr:
                q=Q()
                ml=get_month_list(fr, to)
                for m in ml:
                    m=m.split('-')
                    q= q | (Q(month=int(m[1])) & Q(year=int(m[0])))

                report=True
        elif fr:
            report =True
    except:
        pass
    if report:
        earnings=Earning.objects.filter(q).order_by('-year', '-month', 'user__pk')

        for u in earnings:
            source_user=None
            try:
                source_user=User.objects.get(pk=u.sourceid)
            except:
                pass

            table_data.append([
                u.user.id,
                u.user.get_full_name(),
                u.month,
                u.year,
                u.earning_type,
                u.desc,
                source_user.get_full_name() if source_user else '',
                u.earning_basis,
                u.earning_percentage,
                u.earning_amount,
                source_user.pk if source_user else '',
                u.source_orderid if u.source_orderid else ''
                ])

    if request.GET.get('export', None)=='csv':
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=detailed-earning.csv' #% (defaultfilters.slugify(sd), defaultfilters.slugify(td))
        writer = csv.writer(response)
        writer.writerow(table_title)
        for rowdata in table_data:
            writer.writerow(rowdata)
        return response






    return render_to_response('dashboard/report-wide.html', {'title':'Earning Details', 'table_title':table_title, 'table_data':table_data, 'from':request.GET.get('from', False), 'to':request.GET.get('to', False), 'report':report}, context_instance=RequestContext(request))

def report_detailed_metrices(request):
    table_title=['ID', 'Name', 'Mon', 'Year', 'PE', 'PCV', 'PCVQ', 'PE Active', 'PE Star', 'PCV Dl', 'GCV', 'Retail Sales', 'Wholesale Sales', 'Wholesale on Retail Sales', 'Retail Diff', 'Paid Rank']
    table_data=[]    

    fr=request.GET.get('from', False)
    to=request.GET.get('to', False)
    
    report=False
    try:
        q=Q()
        if fr:
            fr=fr.split('-')
            fr=datetime.datetime(int(fr[0]), int(fr[1]), 1, 0, 0, 0)
            q=Q(month=fr.month) & Q(year=fr.year)

        if to:
            to=to.split('-')
            to=datetime.datetime(int(to[0]), int(to[1]), 1, 0, 0, 0)
            if to>=fr:
                q=Q()
                ml=get_month_list(fr, to)
                for m in ml:
                    m=m.split('-')
                    q= q | (Q(month=int(m[1])) & Q(year=int(m[0])))

                report=True
        elif fr:
            report =True
    except:
        pass


    if report:
        uh=UserHistory.objects.filter(q).order_by('-year', '-month', 'user__pk')

        for u in uh:
            table_data.append([
                u.user.id,
                u.user.get_full_name(),
                u.month,
                u.year,
                "%.2f"%u.personally_enrolled,
                "%.2f"%u.pcv,
                "%.2f"%u.pcv_quater,
                "%.2f"%u.personally_enrolled_active,
                "%.2f"%u.personally_enrolled_star,
                "%.2f"%(u.l1_pcv+u.l2_pcv+u.l3_pcv+u.l4_pcv+u.l5_pcv),
                "%.2f"%u.gcv,
                "%.2f"%u.prv,
                "%.2f"%u.wholsesale_sales,
                "%.2f"%u.wholsesale_onretailsales,
                "%.2f"%u.retail_diff,
                u.earned_rank.name
                ])

    if request.GET.get('export', None)=='csv':
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=detailed-metrices.csv' #% (defaultfilters.slugify(sd), defaultfilters.slugify(td))
        writer = csv.writer(response)
        writer.writerow(table_title)
        for rowdata in table_data:
            writer.writerow(rowdata)
        return response


    return render_to_response('dashboard/report-wide.html', {'title':'Metrices', 'table_title':table_title, 'table_data':table_data, 'from':request.GET.get('from', False), 'to':request.GET.get('to', False), 'report':report}, context_instance=RequestContext(request))


def report_detailed_bonuses(request):
    table_title=['ID', 'Name', 'Paid Rank', 'Month', 'Year', 'Retail', 'Vol. Bonus', 'BSC', 'Downline Com.', 'Pay Rank Bonus', 'Fast Start', 'Total']
    table_data=[]    

    fr=request.GET.get('from', False)
    to=request.GET.get('to', False)
    
    report=False
    try:
        q=Q()
        if fr:
            fr=fr.split('-')
            fr=datetime.datetime(int(fr[0]), int(fr[1]), 1, 0, 0, 0)
            q=Q(month=fr.month) & Q(year=fr.year)

        if to:
            to=to.split('-')
            to=datetime.datetime(int(to[0]), int(to[1]), 1, 0, 0, 0)
            if to>=fr:
                q=Q()
                ml=get_month_list(fr, to)
                for m in ml:
                    m=m.split('-')
                    q= q | (Q(month=int(m[1])) & Q(year=int(m[0])))

                report=True
        elif fr:
            report =True
    except:
        pass

    total={
        'retail':Decimal(0.00),
        'volume':Decimal(0.00),
        'bsc':Decimal(0.00),
        'dl_comm':Decimal(0.00),
        'prb':Decimal(0.00),
        'fs':Decimal(0.00),
        'total':Decimal(0.00),
    }


    if report:
        uh=UserHistory.objects.filter(q).order_by('-year', '-month', 'user__pk')

        for u in uh:
            total_amount=u.total(include_fs=True)
            if total_amount>0:
                table_data.append([
                    u.user.id,
                    u.user.get_full_name(),
                    u.earned_rank.name,
                    u.month,
                    u.year,
                    "%.2f"%u.retail_comm,
                    "%.2f"%u.volume_bonus,
                    "%.2f"%u.business_supply_credit,
                    "%.2f"%u.commissions_on_downline,
                    "%.2f"%u.pay_rank_bonus,
                    "%.2f"%u.faststart(),
                    "%.2f"%u.total(include_fs=True)
                    ])
                total['retail']=total['retail']+Decimal(u.retail_comm)
                total['volume']=total['volume']+Decimal(u.volume_bonus)
                total['bsc']=total['bsc']+Decimal(u.business_supply_credit)
                total['dl_comm']=total['dl_comm']+Decimal(u.commissions_on_downline)
                total['prb']=total['prb']+Decimal(u.pay_rank_bonus)
                total['fs']=total['fs']+u.faststart()
                total['total']=total['total']+Decimal(total_amount)

    if request.GET.get('export', None)=='csv':
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=detailed-bonuses.csv' #% (defaultfilters.slugify(sd), defaultfilters.slugify(td))
        writer = csv.writer(response)
        writer.writerow(table_title)
        writer.writerow(['', '', '', '', '', "%0.2f"%total['retail'], "%0.2f"%total['volume'], "%0.2f"%total['bsc'], "%0.2f"%total['dl_comm'], "%0.2f"%total['prb'], "%0.2f"%total['fs'], "%0.2f"%total['total']])
        for rowdata in table_data:
            writer.writerow(rowdata)
        return response


    return render_to_response('dashboard/report-wide.html', {'total':total, 'title':'Detailed Bonuses', 'table_title':table_title, 'table_data':table_data, 'from':request.GET.get('from', False), 'to':request.GET.get('to', False), 'report':report}, context_instance=RequestContext(request))


from satchmo_store.shop.signals import satchmo_cart_details_query, satchmo_cart_add_complete
from satchmo_store.shop.views.cart import display
#from django.views.decorators.cache import never_cache


def cart_display(request, cart=None, error_message='', default_view_tax=None, frontend=True):
    from django.conf import settings

    cart=Cart.objects.from_request(request=request, create=True)
    items=cart.cartitem_set.all()
    total_item_in_cart=0
    offerable_quant_total=0

    product_slug=None
    offer_value=Decimal(0.00)
    item_price=Decimal(15.00)
    for i in items:
        if i.product.slug in settings.DISC_PRODUCT_SLUGS:
            if i.product.slug==settings.OFFER_PRODUCT_SLUG:
                i.delete()
            else:
                if i.quantity>1:
                    i.quantity=1
                    i.save()
                product_slug=i.product.slug
                offer_value=i.product.unit_price            
        else:
            if i.quantity<1:
                i.delete()
            else:
                total_item_in_cart=total_item_in_cart+i.quantity
                if i.product.slug not in settings.OFFERLESS_PRODUCTS_SLUG:
                    offerable_quant_total += i.quantity
                    item_price=i.product.unit_price

    offered_count=((-1)*offer_value)/item_price
    left_over=offerable_quant_total-offered_count
    if left_over>0:
        offers=Offer.objects.filter(active=True)
        apply_offer=None
        applicable_offers=[]
        for offer in offers:
            if left_over%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':0, 'offer':offer})
            elif (left_over+offer.free)%(offer.buy+offer.free)==0:
                apply_offer=True
                applicable_offers.append({'need_more':offer.free, 'offer':offer})
            else:
                units=int(left_over/(offer.buy+offer.free))
                if units>0:
                    apply_offer=True
                    applicable_offers.append({'need_more':0, 'offer':offer}) #we will show need more only of that is the no of free products

        if apply_offer:
            apply_offer=applicable_offers[0]    
            free=int(left_over/(apply_offer['offer'].buy+apply_offer['offer'].free))*apply_offer['offer'].free
            if free>0:
                request.session[settings.OFFER_AMT_SESSION_SLUG]=Decimal(free) 
                product=Product.objects.get(slug=settings.OFFER_PRODUCT_SLUG)
                quantity=1
                details=[]
                formdata=request.POST
                satchmo_cart_details_query.send(
                    cart,
                    product=product,
                    quantity=quantity,
                    details=details,
                    request=request,
                    form=formdata
                )
                added_item=cart.add_item(product, quantity, details)
                satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item, product=product, request=request, form=formdata)


    return display(request, cart=None, error_message='', default_view_tax=None)


@staff_member_required
def view_commissions(request):
    page = int(request.GET.get('page', 1))
    content_list = User.objects.all()
    content = paginate(content_list, page)

    commisions={}
    com=RankCommission.objects.all()
    for c in com:
        commisions[c.rank]=c

    now=datetime.datetime.now()
    table_data=[]
    for c in content.object_list:
        user_volumes_and_rank=_get_user_volumes_and_ranks_for_month(c, now.month, now.year)

        complan=commisions[user_volumes_and_rank['payrank']]
        
        rc=user_volumes_and_rank['volume']['pcv']*Decimal(complan.ratail_commission)
        vb=Decimal(0) if user_volumes_and_rank['volume']['pcv'] <= 500 else (user_volumes_and_rank['volume']['pcv']*Decimal(complan.volume_bonus_for_500_pcv))
        bs=Decimal(0) if user_volumes_and_rank['volume']['pcv'] <= 500 else (user_volumes_and_rank['volume']['pcv']*Decimal(complan.business_supply_credit_for_300_pcv))
        
        table_data.append(
            [
                c.username, 
                complan.rank.name,
                '%g'%(rc),
                '%g'%(vb),
                '%g'%(bs)
            ]
            )
    
    table_title=['Username', 'Rank', 'Retail Commission', 'Volume Bonus', 'Business Supply Credit']
    page_data={'num_pages':content.paginator.num_pages, 'has_previous':content.has_previous, 'previous_page_number':content.previous_page_number, 'page_range':content.page_range, 'number':content.number, 'next_page_number':content.next_page_number, 'has_next':content.has_next}
    return render_to_response('dashboard/view-commissions.html', {'title':'Sales by consultants', 'table_title':table_title, 'table_data':table_data, 'page_data':page_data}, context_instance=RequestContext(request))


@staff_member_required
def view_support(request, support_id):
    sr=SupportRecipients.objects.filter(user=request.user, request__id=support_id)
    if len(sr)<1:
        raise Http404
    else:
        sr=sr[0]
        status=dict(SUPPORT_REQUEST_STATUS_CHOICES)
        del status['unread']


        if request.method=='POST':
            body=request.POST.get('body', None)
            if body:
                SupportRequest(body=body, by=request.user, category=sr[0].request.category, reply_to=sr[0].request).save()
                SupportRecipients.objects.filter(~Q(user=request.user), request=sr[0].request).update(read_on=None)

            new_status=request.POST.get('status-to', None)
            if new_status:
                if new_status in status:
                    srequest=sr.request
                    srequest.status=new_status
                    srequest.save()

            forward=request.POST.get('forward-to', None)
            if forward:
                try:
                    g=Group.objects.get(pk=forward)
                    recepients=User.objects.filter(groups=g).distinct()
                    for r in recepients:
                        try:
                            r=SupportRecipients.objects.get(request=sr[0], user=r)
                        except:
                            rec=SupportRecipients(request=sr[0], user=r).save()
                            if r==request.user:
                                rec.read_on=datetime.datetime.now()
                                rec.save()
                except:
                    pass

        if sr.request.status=='unread':
            sr.request.status='open'
            srequest=sr.request
            srequest.status='open'
            srequest.save()

        if sr.read_on is None:
            sr.read_on=datetime.datetime.now()
            sr.save()
            
        current_status=sr.request.status
        groups=Group.objects.all()
        return render_to_response('dashboard/tools-support-read.html', {'requested':sr, 'groups':groups, 'status':status, 'current_status':current_status}, context_instance=RequestContext(request))



@staff_member_required
def report_overview(request):
    table_data=[]
    
    now=datetime.datetime.now()
    month_start=datetime.datetime(now.year, now.month, 1, 0, 0, 0)
    month_end=datetime.datetime((now.year if (now.month<12) else now.year+1), ((now.month+1) if (now.month<12) else 1), 1, 23, 59, 59) - datetime.timedelta(days=1)
    
    today_start=datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
    today_end=datetime.datetime(now.year, now.month, now.day, 23, 59, 59)

    week_start=today_start-datetime.timedelta(today_start.weekday())
    week_end=today_end+datetime.timedelta(6-today_start.weekday())

    r_total=Order.objects.filter(contact__user=None).count()
    r_month=Order.objects.filter(contact__user=None, time_stamp__gte=month_start, time_stamp__lte=month_end).count()
    r_week=Order.objects.filter(contact__user=None, time_stamp__gte=week_start, time_stamp__lte=week_end).count()
    r_day=Order.objects.filter(contact__user=None, time_stamp__gte=today_start, time_stamp__lte=today_end).count()

    c_total=User.objects.filter(is_active=True).count()
    c_month=User.objects.filter(is_active=True, date_joined__gte=month_start, date_joined__lte=month_end).count()
    c_week=User.objects.filter(is_active=True, date_joined__gte=week_start, date_joined__lte=week_end).count()
    c_day=User.objects.filter(is_active=True, date_joined__gte=today_start, date_joined__lte=today_end).count()

    table_data.append(['New Today:', r_day, c_day, r_day+c_day])
    table_data.append(['New This Week:', r_week, c_week, r_week+c_week])
    table_data.append(['New This Month:', r_month, c_month, r_month+c_month])
    table_data.append(['Total Customers:', r_total, c_total, r_total+c_total])

    table_title=['', 'Retail Customer', 'Consultant', 'Total']
    return render_to_response(
        'dashboard/report.html',
        {'title':'Overview', 'table_title':table_title, 'table_data':table_data, },
        context_instance=RequestContext(request))

def report_new_users_per_day(request):
    search_date_s_string=request.GET.get('q-ds', None)
    search_date_e_string=request.GET.get('q-de', None)
    date_search=False
    if search_date_s_string and search_date_e_string:
        search_date_s=datetime.datetime.strptime(search_date_s_string, "%m/%d/%Y")
        search_date_e=datetime.datetime.strptime(search_date_e_string, "%m/%d/%Y")
        if search_date_e>=search_date_s:
            date_search=True
            start=datetime.datetime(search_date_e.year, search_date_e.month, search_date_e.day, 0, 0, 0)
            delta=search_date_e-search_date_s
            days_count=delta.days+1

    if not date_search:
        now=datetime.datetime.now()
        start=datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
        days_count=120

    table_data=[]
    for n in range(days_count):
        d=start-datetime.timedelta(n)
        retail=Order.objects.filter(contact__user=None, time_stamp__gte=d, time_stamp__lte=datetime.datetime(d.year, d.month, d.day, 23, 59, 59)).count()
        consultant=User.objects.filter(is_active=True, date_joined__gte=d, date_joined__lte=datetime.datetime(d.year, d.month, d.day, 23, 59, 59)).count()
        table_data.append([d.strftime('%d %h %Y'), retail, consultant, retail+consultant])
    
    table_title=['Date', 'Retail Customer', 'Consultant', 'Total']
    return render_to_response('dashboard/report.html', {'title':'New Users Per Day', 'table_title':table_title, 'table_data':table_data, 'search':True, 'search_date':True, 'search_date_s':search_date_s_string, 'search_date_e':search_date_e_string}, context_instance=RequestContext(request))

@staff_member_required
def report_sales_by_consultants(request):

    page = int(request.GET.get('page', 1))
    content_list = User.objects.all()

    q=request.GET.get('q', None)        
    if q:
        content_list=content_list.filter(Q(first_name__icontains=q)|Q(last_name__icontains=q))
        
    content = paginate(content_list, page)
    
    table_data=[]
    for c in content.object_list:
        uh=UserHistory.objects.filter(user=c).order_by('-year', '-month')
        if len(uh)>0:
            uh=uh[0]
        else:
            uh=None

        sales=get_commissionable_order(user_in=[c], remarks_in=['self', 'web'])
        
        table_data.append(
            [
                c.pk,
                c.get_full_name,
                c.username,
                UserSponsor.objects.filter(~Q(user=c), sponsor=c).count(),
                uh.earned_rank.name if uh else '',
                "%g"%float(sales[0]['discounted_total__sum'] if (len(sales)==1 and sales[0]['discounted_total__sum']) else 0),
                ]
            )
    table_title=['ID', 'Full Name', 'Username', 'Consultants Enrolled', 'Last Pay Rank', 'Sales']
    page_data={'num_pages':content.paginator.num_pages, 'has_previous':content.has_previous, 'previous_page_number':content.previous_page_number, 'page_range':content.page_range, 'number':content.number, 'next_page_number':content.next_page_number, 'has_next':content.has_next}
    return render_to_response('dashboard/report.html', {'title':'Sales by consultants', 'table_title':table_title, 'table_data':table_data, 'page_data':page_data, 'q':q, 'search':True, 'search_string':True}, context_instance=RequestContext(request))

@staff_member_required
def report_faststart(request):
    
    return render_to_response('dashboard/report.html', {'title':'Faststart', }, context_instance=RequestContext(request))

@staff_member_required
def requested_parties(request):
    table_title=['ID', 'Requested To', 'Requestor', 'Requested On']
    table_data=[]
    parties=Event.objects.filter(status=2)
    for p in parties:
        table_data.append(
        [
            '<a href="/backend/dashboard/events/events-list/'+str(p.pk)+'/">'+str(p.pk)+'</a>',
            p.owner.get_full_name(),
            p.host.name if p.host else '',
            p.created_at
        ]
        )
    
        
    return render_to_response('dashboard/report.html', {'title':'Requested Parties', 'table_title':table_title, 'table_data':table_data}, context_instance=RequestContext(request))

@staff_member_required
def report_top_recruiter(request):
    return render_to_response('dashboard/report.html', {'title':'Top recruiter', }, context_instance=RequestContext(request))

@staff_member_required
def report_members_per_state(request):
    return render_to_response('dashboard/map.html', {'title':'Members per state', }, context_instance=RequestContext(request))

@staff_member_required
def search(request):
    q=request.GET.get('q', None)
    users=None
    retail_users=None
    if q:
        q=q.split(' ', 1)
        if len(q)==1:
            users=User.objects.filter(Q(username__icontains=q[0]) | Q(first_name__icontains=q[0]) | Q(last_name__icontains=q[0]) | Q(profile__phone_number__icontains=q[0]))
            #retail_users=.objects.filter(Q(first_name__icontains=q[0]) | Q(last_name__icontains=q[0]) | Q(profile__phone_number__icontains=q[0]))
            contact_ids=PhoneNumber.objects.values_list('contact__id', flat=True).filter(phone__icontains=q[0])
            retail_users=Contact.objects.filter(Q(first_name__icontains=q[0]) | Q(last_name__icontains=q[0]) | Q(id__in=contact_ids), user__isnull=True)

        else:
            users=User.objects.filter(first_name__icontains=q[0], last_name__icontains=q[1])
            retail_users=Contact.objects.filter(first_name__icontains=q[0], last_name__icontains=q[1])
            
    return render_to_response('dashboard/search_results.html', {
      'users':users,
      'retail_users':retail_users
    }, context_instance=RequestContext(request))

@login_required
def re_login_as_admin(request, admin_user_id):
    from django.conf import settings
    follow=request.GET.get('follow', None)
    admin_user_id = request.session.get("admin_user_id", None)
    if admin_user_id:
        member = get_object_or_404(User, pk = int(admin_user_id))
        member = authenticate(username=member.username, pass_hash=settings.ADMIN_HASH_SECRET)
        login(request, member)
        if follow:
            return HttpResponseRedirect("http://%s"%follow)
        else:
            return HttpResponseRedirect("http://%s"%(member.website.domain()))
            #return HttpResponseRedirect(reverse("dashboard"))
    else:
        return HttpResponseRedirect('/')


def virtual_login(request, user_id):
    from django.conf import settings
    passed_admin_user_id = request.user.id
    params = {}

    member = get_object_or_404(User, pk = int(user_id))

    hash = hashlib.md5()
    hash.update("%s:%s:%s" % ('user', user_id, settings.ADMIN_HASH_SECRET))
    request_hash = request.GET.get("hash", "")
    if request_hash != hash.hexdigest().upper():
        raise Exception("invalid hash value")
    
    member = authenticate(username=member.username, pass_hash=settings.ADMIN_HASH_SECRET)
    login(request, member)
    
    request.session["admin_user_id"] = passed_admin_user_id
    #return HttpResponse(member.website.domain())
    return HttpResponseRedirect("http://%s/manage"%(member.website.domain()))


@staff_member_required
@require_POST
def delete(request, type, user_id):
    user=get_object_or_404(User, pk=user_id)
    id=request.POST.get('id', -1)
    sycceff=False
    if type=='address':
        contact=Contact.objects.get(user=user)
        AddressBook.objects.filter(contact=contact, id=id).delete()
        success=True
    else:
        raise Http404


    if success:
        return ajax_render({'status': {'success':True}, 'data':id})
    else:
        return ajax_render({'status':{'success':False}, 'data':id})


@staff_member_required
@require_POST
def add(request, type, user_id):
    user=get_object_or_404(User, pk=user_id)
    clear=False
    data={}
    if type=='credit':
        credit_obj=Credit(user=user, timestamp=datetime.datetime.now())
        form=CreditForm(data=request.POST, instance=credit_obj)
        clear=True
        def data():
            return {'pc':user.pc, 'bsc':user.bsc}
    else:
        raise Http404

    if form.is_valid():
        form.save()
        data=data()
        return ajax_render({'status': {'success':True, 'url':request.POST.get('action_url', False), 'callback':request.POST.get('action_callback', False), 'clear':clear}, 'data':data})
    else:
        return ajax_render({'status':{'success':False, 'url':request.POST.get('action_url', False)}, 'errors': form.errors})

@staff_member_required
@require_POST
def update(request, type, data_id):
    """
    MRVG: Did some small hacks here so as not disturb the formset arquitecture.
    """
    
    if type=='user':
        user=get_object_or_404(User, pk=data_id)
        UserFormset=modelformset_factory(User, form=UserForm, can_delete=False, extra=0)
        form = UserFormset(request.POST)
    elif type=='contacts':
        user=get_object_or_404(User, pk=data_id)
        form = ContactFormset(data=request.POST, instance=user)
    elif type=='address':
        user=get_object_or_404(User, pk=data_id)
        contact=Contact.objects.get(user=user)
        form = AddressFormset(data=request.POST, instance=contact)
    elif type=='contacts_contact':
        contact=get_object_or_404(Contact, pk=data_id)
        ContactFormset=modelformset_factory(Contact, can_delete=False, extra=0)
        form=ContactFormset(data=request.POST, queryset=Contact.objects.filter(id=contact.id))
    elif type=='address':
        contact=get_object_or_404(Contact, pk=data_id)
        form = AddressFormset(data=request.POST, instance=contact)
    elif type=='type':
        user=get_object_or_404(User, pk=data_id)
        usertype=get_object_or_404(UserType, user=user)
        form=UserTypeForm(instance=usertype, data=request.POST)
    else:
        raise Http404
    
    if form.is_valid():
        form.save()
        return ajax_render({'status': {'success':True}})
    else:
        return ajax_render({'status':{'success':False}, 'errors': form.errors})

@staff_member_required
def contact_profile(request, contact_id):
    contact=get_object_or_404(Contact, pk=contact_id)
    ContactFormset=modelformset_factory(Contact, can_delete=False, extra=0)
    contacts_form=ContactFormset(queryset=Contact.objects.filter(id=contact.id))
    addresses_form=AddressFormset(instance=contact)
    all_order=Order.objects.filter(contact=contact)

    return render_to_response('dashboard/contact_profile.html', {
        'addresses_form': addresses_form,
        'contacts_form':contacts_form,
        'all_order':all_order,
        'contact':contact,
    }, context_instance=RequestContext(request))


    return HttpResponse(contact_id)

@staff_member_required
def profile(request, user_id):
    user=get_object_or_404(User, pk=user_id)
    UserFormset=modelformset_factory(User, form=UserForm,fields=('username', 'first_name', 'last_name', 'email'), can_delete=False, extra=0)
    user_form=UserFormset(queryset=User.objects.filter(id=user.id))
    contact=Contact.objects.get(user=user)
    addresses_form=AddressFormset(instance=contact)
    contacts_form=ContactFormset(instance=user)
    dl_rank=_get_downline_rank_and_volume_for_month(user, 9, 2011)
    usertype_form=UserTypeForm(instance=UserType.objects.get(user=user))

    credit=Credit.objects.filter(user=user)
    credit_form=CreditForm()
    all_order=CommissionableOrder.objects.filter(commission_to=user, remarks__in=['self', 'web'], )
    all_parties=Event.objects.filter(owner=user)

    return render_to_response('dashboard/profile.html', {
        'user_form':user_form,
        'addresses_form': addresses_form,
        'contacts_form':contacts_form,
        'member':user,
        'dl_rank':dl_rank,
        'credit':credit,
        'credit_form':credit_form,
        'all_order':all_order,
        'all_parties':all_parties,
        'usertype_form':usertype_form
    }, context_instance=RequestContext(request))

@staff_member_required
def section(request, section=None):
    if section:
        sections = Section.objects.filter(parent__slug=section, is_enabled=True)
    else:
        sections = Section.objects.filter(parent__isnull=True, is_enabled=True)

    return render_to_response('dashboard/section.html', {
        "sections": sections, "section": section
    }, context_instance=RequestContext(request))

@staff_member_required
def content_list(request, section, sub_section=None):
    # special case for top level items
    if not sub_section:
        sub_section = section
    
    try:
        section = Section.objects.get(slug=sub_section, is_enabled=True)
    except Section.DoesNotExist:
        raise Http404
    # Get the section settings.
    section_settings = SECTION_SETTINGS.get(sub_section, {})
    if not section_settings:
        raise Http404

    content_model = section_settings['model']
    fields = section_settings['list_display']
    
    content_list = content_model.objects.all()

    search_string=request.GET.get('q', None)
    search=True if 'search_fields' in section_settings and len(section_settings['search_fields'])>0 else False
    if search and search_string:
        q=Q()
        for field in section_settings['search_fields']:
            kwargs={'%s__icontains'%field: search_string}
            q=q | Q(**kwargs)
            
        if q:
            content_list=content_list.filter(q)

    search_date_s_string=request.GET.get('q-ds', None)
    search_date_e_string=request.GET.get('q-de', None)
    search_date=True if 'search_date_fields' in section_settings and len(section_settings['search_date_fields'])>0 else False
    date_in_pagination=False
    if search_date and search_date_s_string and search_date_e_string:
        search_date_s=datetime.datetime.strptime(search_date_s_string, "%m/%d/%Y")
        search_date_e=datetime.datetime.strptime(search_date_e_string, "%m/%d/%Y")
        if search_date_e>=search_date_s:
            date_in_pagination=True
            q=Q()
            for field in section_settings['search_date_fields']:
                kwargs={'%s__gte'%field: search_date_s, '%s__lte'%field: search_date_e }
                q=q | Q(**kwargs)

            if q:
                content_list=content_list.filter(q)
            
            
    page = int(request.GET.get('page', 1))
    content = paginate(content_list, page)

    return render_to_response('dashboard/content.html', {
        'section': section, 'sub_section': sub_section,
        'items': content, 'fields': fields, 'search':search, 'search_string':search_string, 'search_date':search_date, 'search_date_s_string':search_date_s_string, 'search_date_e_string':search_date_e_string, 'date_in_pagination':date_in_pagination
    }, context_instance=RequestContext(request))

@staff_member_required
def content_manager(request, section, object_id=None, sub_section=None):
    # special case for top level items
    if not sub_section:
        sub_section = section

    try:
        section = Section.objects.get(slug=sub_section, is_enabled=True)
    except Section.DoesNotExist:
        raise Http404
    # Get the section settings.
    section_settings = SECTION_SETTINGS.get(sub_section, {})
    if not section_settings:
        raise Http404

    content_model = section_settings['model']
    content_form = section_settings['form']
    content_formsets = section_settings['formsets']

    content = None
    try:
        content = content_model.objects.get(pk=object_id)
        # MRVG: If the content object has an attribute named status change it to read
        # when an admin user has clicked on it
        if isinstance(content, SupportRequest):
            content.status = 'open'
        action_type = 'Updated'
    except content_model.DoesNotExist:
        action_type = 'Created'

    if request.method == "POST":
        form = content_form(data=request.POST, files=request.FILES, instance=content)
        formsets = []

        valid_forms = True
        for fs in content_formsets:
            formset_data=fs(data=request.POST, files=request.FILES, instance=content)
            formsets.append(formset_data)
            if formset_data.is_valid():
                formset_data.save()
            else:
                valid_forms=False

        if form.is_valid():
            form.save()
        else:
            valid_forms=False

        if valid_forms:
            messages.success(request, "%s Successfully %s" % (section.title, action_type))
            if section != sub_section:
                return HttpResponseRedirect(reverse('dashboard_section_list', args=[section.slug, sub_section]))
            return HttpResponseRedirect(reverse('dashboard_section', args=[section.slug,]))
    else:
        form = content_form(instance=content)
        formsets = []
        for fs in content_formsets:
            formsets.append(fs(instance=content))

    return render_to_response('dashboard/content_manager.html', {
        'section': section, 'sub_section': sub_section,
        'form': form, 'formsets': formsets, 'content': content
    }, context_instance=RequestContext(request))

@staff_member_required
def content_removal(request, section, object_id, sub_section=None):
    # special case for top level items
    if not sub_section:
        sub_section = section

    try:
        section = Section.objects.get(slug=sub_section, is_enabled=True)
    except Section.DoesNotExist:
        raise Http404

    # Get the section settings.
    section_settings = SECTION_SETTINGS.get(sub_section, {})
    if not section_settings:
        raise Http404

    content_model = section_settings['model']

    content = None
    try:
        content = content_model.objects.get(pk=object_id)
    except content_model.DoesNotExist:
        raise Http404

    if request.method == "POST":
        content.delete()
        messages.success(request, "%s Successfully Removed" % section.title)
        if section != sub_section:
            return HttpResponseRedirect(reverse('dashboard_section_list', args=[section.slug, sub_section]))
        return HttpResponseRedirect(reverse('dashboard_section', args=[section.slug,]))
    return render_to_response('dashboard/content_removal.html', {
        'section': section, 'sub_section': sub_section,
        'content': content
    }, context_instance=RequestContext(request))



#############################################################################b
# Section views for updating content
#############################################################################
@staff_member_required
def userinfo_manager(request, object_id=None):
    return content_manager(request, UserForm, User, 'dashboard_users', 'users', object_id)

@staff_member_required
def flatpages_manager(request, object_id=None):
    return content_manager(request, FlatPageForm, FlatPage, 'dashboard_content', 'content-management', object_id)

@staff_member_required
def documents_manager(request, object_id=None):
    return content_manager(request, DocumentForm, Document, 'dashboard_documents', 'documents', object_id)
    
def products_categories_manager(request, object_id=None):
    return content_manager(request, DocumentForm, Document, 'dashboard_documents', 'documents', object_id)

@staff_member_required
def commision(request):
    invalid_date = False
    today = datetime.date.today()
    yesterday = today-datetime.timedelta(1)
    last_7_day = today-datetime.timedelta(7)
    last_30_day = today-datetime.timedelta(30)    
    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    only_tools = request.GET.get('only_tools', None)

    if only_tools == 'true':
        only_tools = True
    else:
        only_tools = False
    
    if sd is None or sd == '':
        sd = datetime.date.strftime(yesterday, "%Y-%m-%d")
    
    if td is None or td == '':
        td = datetime.date.strftime(yesterday, "%Y-%m-%d")
    
    try:
        start_date = time.strptime("%s 00:00:00"%(sd), "%Y-%m-%d %H:%M:%S")
        to_date = time.strptime("%s 23:59:59"%(td), "%Y-%m-%d %H:%M:%S")
    except:
       invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'
    
    if not invalid_date:
        if to_date < start_date:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'
    
    return render_to_response('dashboard/commission.html', {},
        context_instance=RequestContext(request))

@staff_member_required
def settings(request):
    return render_to_response('dashboard/settings.html', {},
        context_instance=RequestContext(request))

@staff_member_required
def communicate(request):  
    return render_to_response('dashboard/communicate.html', {},
        context_instance=RequestContext(request))

def category_view(request, slug, parent_slugs='', template='product/category.html'):
    """Display the category, its child categories, and its products.

    Parameters:
     - slug: slug of category
     - parent_slugs: ignored
    """
    try:
        category =  Category.objects.get_by_site(slug=slug)
        products = list(category.active_products())
        sale = find_best_auto_discount(products)

    except Category.DoesNotExist:
        return bad_or_missing(request, _('The category you have requested does not exist.'))

    child_categories = category.get_all_children()

    all_products=[]
    all_products += products
    for cat in child_categories:
        all_products += list(cat.active_products())

    ctx = {
        'category': category,
        'child_categories': child_categories,
        'sale' : sale,
        'products' : products,
        'all_products' : list(set(all_products)),
    }
    index_prerender.send(Product, request=request, context=ctx, category=category, object_list=products)
    return render_to_response(template, context_instance=RequestContext(request, ctx))

def dashboard_change_password(request):
    if request.method == "POST":
        form = ChangePasswordForm(data=request.POST, user=request.user)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'Password successfully updated')
            return HttpResponseRedirect(reverse('dashboard'))
    else:
        form = ChangePasswordForm(user=request.user)

    return render_to_response('dashboard/password_change.html', {
        'form': form
    }, context_instance=RequestContext(request))


def dashboard_login(request, next=None, template_name='dashboard/login.html'):
    if next:
        redirect_to = next
    else:
        redirect_to = request.REQUEST.get('next', '/')

    if not request.user.is_anonymous() and request.user.is_authenicated():
        return HttpResponseRedirect(redirect_to)
        
    if request.method == "POST":
        form = AuthenticationRememberMeForm(data=request.POST)

        if form.is_valid():
            if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL

            if not form.cleaned_data['remember_me']:
                request.session.set_expiry(0)

            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()

            return HttpResponseRedirect(redirect_to)
    else:
        form = AuthenticationRememberMeForm(request)

    return render_to_response(template_name, {
        'next': redirect_to,
        'form': form
    }, context_instance=RequestContext(request))

def dashboard_logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('dashboard'))
