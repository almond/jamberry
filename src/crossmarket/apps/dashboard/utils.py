from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, InvalidPage


PAGINATOR_PAGE_RANGE = 10


def paginate(object_list, page=1, size=50):
    """Paginator shortcut."""
    paginator = Paginator(object_list, size)

    try:
        objects = paginator.page(page)
    except (EmptyPage, InvalidPage):
        objects = paginator.page(paginator.num_pages)

    # Custom page range giving a sample of page numbers instead of the entire thing.
    max_number = min(objects.number+PAGINATOR_PAGE_RANGE, objects.paginator.num_pages+1)
    min_number = min(objects.number-PAGINATOR_PAGE_RANGE, objects.number)

    if min_number < 0:
        min_number = 1

    if min_number == 0:
        min_number = 1

    objects.page_range = range(min_number, max_number)

    return objects