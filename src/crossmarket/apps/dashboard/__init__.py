from satchmo_store.shop.signals import * #order_success
from product.models import Product
from satchmo_store.shop.models import Cart, CartItem
from django.db.models import Sum
import datetime
from decimal import *
import chimpy

def pay_upline_commission(whose_upline, order):
    from apps.compensation.models import CommissionableOrder
    upline=whose_upline.upline
    for (counter, user) in enumerate(upline):
        if user.status.type.action not in ['active']:
            continue #user not eligible for commissions!
        CommissionableOrder(commission_to=user, order=order, remarks='l%d'%(counter+1), timestamp=datetime.datetime.now()).save()
        update_user_history(user)

def process_bsc(order, user):
    from apps.compensation.models import Credit, BSCLog

    BSCLog(order=order, user=user, processed=False).save()
    details=user.current_month_details
    if details['volume']['pcv']>=300:
        date=datetime.datetime.now()
        month_start=datetime.datetime(date.year, date.month, 1, 0, 0, 0)
        unprocessed=BSCLog.objects.filter(processed=False, user=user, order__time_stamp__gte=month_start)
        for l in unprocessed:
            amount=l.order.discounted_total
            pcv=Decimal('0.65')*Decimal(amount)
            bsc=Decimal(details['payrank'].commission.business_supply_credit_for_300_pcv)*pcv
            Credit(user=user, credit_type='bsc', transaction_type='a', timestamp=l.order.time_stamp, amount=bsc).save()#Keeping same timestamp will help relating an order
            l.processed=True
            l.save()


def store_details(sender, order=None, **kwargs):
    from threaded_multihost import threadlocals 
    from apps.dashboard.models import OrderWebsite, OrderType
    from apps.pricing.models import WholesaleDiscount
    from apps.events.models import Event, Attendee, EventAttendeeCart
    from apps.compensation.models import CommissionableOrder, PartyOrder, PartyOrderDetails, Credit, BSCLog, PartyOrderDirectShip
    from django.conf import settings

    request = threadlocals.get_current_request() 
    OrderWebsite(order=order, website=request.referred_by.website).save()
    order_type=request.session.get('wholesale_discount')
    if not order_type:
        order_type=WholesaleDiscount.objects.get(slug='Regular Order')
    ot=OrderType(order=order, type=order_type)
    ot.save()

    if (request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY) and
    request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug==settings.PARTY_WHOLESALE_DISCOUNT_SLUG):
        for_whome=request.session['whose_party_cart']
        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
            party=Event.objects.get(pk=request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY])

            if for_whome=="party":
                EventAttendeeCart.objects.filter(event=party, active=True, direct_ship=False).update(active=False)
            else:
                EventAttendeeCart.objects.filter(event=party, active=True, attendee__id=for_whome).update(active=False)
                
            porder=PartyOrder(order=order, party=party)
            porder.save()

            credits_given=Credit.objects.filter(user=party.owner, remarks='order-%d'%order.id, transaction_type='a', credit_type='pc').aggregate(total=Sum('amount'))
            credits_given=credits_given['total'] if credits_given['total'] else Decimal(0.00)
            reward=party.hostess_rewards()
            percent_product_credit=0.00
            if len(reward)>0:
                percent_product_credit=Decimal(reward[0].percent_product_credit/100)*Decimal(order.discounted_total)
            balance=Decimal(credits_given)-Decimal(percent_product_credit)
            Credit(user=party.owner, remarks='order-%d'%order.id, transaction_type='a', credit_type='pc', amount=balance, timestamp=datetime.datetime.now()).save()

        if settings.PARTY_ORDERDETAILS_SESSION_KEY in request.session:
            direct_ship_to=request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY] if settings.PARTY_SHIPDETAILS_SESSION_KEY in request.session else []
            party_order_details=request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]

            direct_ship_to_tmp=direct_ship_to
            party_order_details_tmp=party_order_details

            for attendee, details in party_order_details.items():
                att=Attendee.objects.get(pk=attendee)
                if for_whome=="party":
                    if str(att.pk) not in direct_ship_to:
                        del party_order_details_tmp[str(att.pk)]
                        for product, quantity in details.items():
                            PartyOrderDetails(party_order=porder, attendee=att, product=Product.objects.get(pk=product), quantity=quantity).save()
                else:
                    if str(att.pk)==str(for_whome):
                        del party_order_details_tmp[str(att.pk)]
                        direct_ship_to_tmp.remove(str(att.pk))
                        PartyOrderDirectShip(party_order=porder, attendee=att, direct_ship=True).save()
                        for product, quantity in details.items():
                            PartyOrderDetails(party_order=porder, attendee=att, product=Product.objects.get(pk=product), quantity=quantity).save()
                
            request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]=party_order_details_tmp
            request.session[settings.PARTY_SHIPDETAILS_SESSION_KEY]=direct_ship_to_tmp


    elif (request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY) and
    request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug==settings.WHOLESALE_DISCOUNT_MARKETING_MATERIAL_SLUG):



        if settings.BSC_AMT_SESSION_SLUG in request.session:
            has_bsc_item=False
            cart=Cart.objects.from_request(request=request, create=True)
            items=cart.cartitem_set.all()
            for i in items:
                if i.product.slug==settings.BSC_PRODUCT_SLUG:
                    has_bsc_item=True
            if has_bsc_item:
                Credit(user=request.user, credit_type='bsc', transaction_type='s', amount=request.session[settings.BSC_AMT_SESSION_SLUG], timestamp=datetime.datetime.now()).save()
                del request.session[settings.BSC_AMT_SESSION_SLUG]

        if settings.PC_AMT_SESSION_SLUG in request.session:
            has_pc_item=False
            cart=Cart.objects.from_request(request=request, create=True)
            items=cart.cartitem_set.all()
            for i in items:
                if i.product.slug==settings.PC_PRODUCT_SLUG:
                    has_pc_item=True
            if has_pc_item:
                Credit(user=request.user, credit_type='pc', transaction_type='s', amount=request.session[settings.PC_AMT_SESSION_SLUG], timestamp=datetime.datetime.now()).save()
                del request.session[settings.PC_AMT_SESSION_SLUG]

    
    #elif (request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT, None) and Event.objects.filter(status=3, pk=request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT)).exists()):
    elif (request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT, None)):
        party=Event.objects.get(pk=request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT))
        porder=PartyOrder(order=order, party=party)
        porder.save()
        attendees=Attendee.objects.filter(event=party, email=order.contact.email)
        if len(attendees)>0:
            attendee=attendees[0]
        else:
            attendee=Attendee(event=party, name=order.contact.first_name, status=1, email=order.contact.email)
            attendee.save()
        cart=Cart.objects.from_request(request=request, create=True)
        items=cart.cartitem_set.all()
        PartyOrderDirectShip(party_order=porder, attendee=attendee, direct_ship=True).save()
        for i in items:
            if i.product.slug not in settings.DISC_PRODUCT_SLUGS:
                PartyOrderDetails(party_order=porder, attendee=attendee, product=i.product, quantity=i.quantity).save()


        credits_given=Credit.objects.filter(user=party.owner, remarks='order-%d'%order.id, transaction_type='a', credit_type='pc').aggregate(total=Sum('amount'))
        credits_given=credits_given['total'] if credits_given['total'] else Decimal(0.00)
        reward=party.hostess_rewards()
        percent_product_credit=Decimal(0.00)
        if len(reward)>0:
            percent_product_credit=Decimal(reward[0].percent_product_credit/100)*Decimal(order.discounted_total)
        balance=percent_product_credit-credits_given
        Credit(user=party.owner, remarks='order-%d'%order.id, transaction_type='a', credit_type='pc', amount=balance, timestamp=datetime.datetime.now()).save()



    if not order.contact.user:
        CommissionableOrder(commission_to=request.referred_by, order=order, remarks='web', timestamp=datetime.datetime.now()).save()
        update_user_history(request.referred_by)
        pay_upline_commission(request.referred_by, order)
        process_bsc(order=order, user=request.referred_by)

        try:
            tmp_chimpy=chimpy.Connection(settings.API_MAILCHIMP['password'], data_center="us2")
            tmp_chimpy.list_subscribe(settings.API_MAILCHIMP['retail_list_id'], order.contact.email, {'FIRST': order.contact.first_name, 'LAST': order.contact.last_name})
        except:
            pass




    else:
        non_commissionable_orders_slug=['Wholesale - Personal', ] #this should later be pulled from db
        if order_type.slug not in non_commissionable_orders_slug:
            CommissionableOrder(commission_to=request.user, order=order, remarks='self', timestamp=datetime.datetime.now()).save()
            #BSCLog(order=order, user=request.user, processed=False).save()
            update_user_history(request.user)
            pay_upline_commission(request.user, order)
            process_bsc(order=order, user=request.user)

    """
    clear_these=[settings.PARTY_ORDERDETAILS_SESSION_KEY, settings.PARTY_SHIPDETAILS_SESSION_KEY, settings.FRONTEND_PARTY_SESSION_SELECT]
    for c in clear_these:
        if c in request.session:
            del request.session[c]
    """
    if settings.FRONTEND_PARTY_SESSION_SELECT in request.session:
        del request.session[settings.FRONTEND_PARTY_SESSION_SELECT]


    if 'registration-form' in request.session:
        from apps.account.forms import RegistrationForm
        reg=RegistrationForm(request.session['registration-form-data'], request=request)
        if reg.is_valid():
            user=reg.save()
        del request.session['registration-form-data']
        del request.session['registration-form']

order_success.connect(store_details)


from apps.compensation.management.commands.calculate_compensation import calc_comm
def update_user_history(user):
    date=datetime.datetime.now()
    month_start=datetime.datetime(date.year, date.month, 1, 0, 0, 0)
    month_end=datetime.datetime(date.year if date.month!=12 else date.year+1, date.month+1 if date.month!=12 else 1, 1, 23, 59, 59) - datetime.timedelta(days=1)
    calc_comm(month_start, month_end, user)





from satchmo_store.shop.signals import satchmo_cart_changed
from django.conf import settings
def item_changed(sender, cart=None, request=None, **kwargs):
    cart=Cart.objects.from_request(request=request, create=True)
    items=cart.cartitem_set.all()

    product_slug=None
    total_cart_value=Decimal(0.00)
    total_cart_quantity=Decimal(0.00)
    offerable_product_quantity=Decimal(0.00)
    offerable_product_amount=Decimal(0.00)
    offer_value=Decimal(0.00)

    product_price=Decimal(15.00)
    for i in items:
        o_slug=i.product.slug
        if o_slug in settings.DISC_PRODUCT_SLUGS:
            if i.product.slug==settings.OFFER_PRODUCT_SLUG:
                i.delete()
            else:
                if i.quantity>1:
                    i.quantity=1
                    i.save()
                product_slug=i.product.slug
                offer_value=i.product.unit_price
        else:
            total_cart_quantity=total_cart_quantity+i.quantity
            total_cart_value=total_cart_value+(i.unit_price*i.quantity)
            if i.product.slug not in settings.OFFERLESS_PRODUCTS_SLUG:
                offerable_product_quantity+=Decimal(i.quantity)
                offerable_product_amount+=Decimal(i.unit_price*i.quantity)
                product_price=i.unit_price

    if total_cart_value==Decimal(0.00):
        cart.empty()
        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
            del request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY]
        if settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY in request.session:
            del request.session[settings.PARTY_WHOLESALE_DISCOUNT_SESSION_KEY]
        if settings.BSC_AMT_SESSION_SLUG in request.session:
            del request.session[settings.BSC_AMT_SESSION_SLUG]
        if settings.FRONTEND_PARTY_SESSION_SELECT in request.session:
            del request.session[settings.FRONTEND_PARTY_SESSION_SELECT]
    elif ((-1)*offer_value)>offerable_product_amount:
        if product_slug:
            CartItem.objects.get(product__slug=product_slug, cart=cart).delete()
            if i.product.slug==settings.BSC_PRODUCT_SLUG:
                new_offer_amt=offerable_product_amount
                request.session[settings.BSC_AMT_SESSION_SLUG]=new_offer_amt
            elif product_slug==settings.PC_PRODUCT_SLUG:
                new_offer_amt=offerable_product_quantity
                request.session[settings.PC_AMT_SESSION_SLUG]=new_offer_amt

            details = []
            formdata = request.POST
            quantity = 1
            product = Product.objects.get(slug=product_slug)
            satchmo_cart_details_query.send(
                cart,
                product=product,
                quantity=quantity,
                details=details,
                request=request,
                form=formdata
            )
            added_item = cart.add_item(product, quantity, details)
            satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item, product=product, request=request, form=formdata)
    

satchmo_cart_changed.connect(item_changed)
