from django.contrib.auth.models import User
from django.contrib.flatpages.models import FlatPage
from django.forms.models import inlineformset_factory

from product.modules.subscription.models import SubscriptionProduct
from product.models import Category, Product, Discount, ProductImage, Price, ProductAttribute
from satchmo_store.shop.models import *

from apps.archive.models import Document
from apps.dashboard.forms import *
from apps.notice.models import Notice
from apps.events.forms import EventPhotoForm
from apps.events.models import Event, EventPhoto
from apps.support.models import SupportRequest, SupportCategory
from apps.support.forms import SupportRequestForm, SupportCategoryForm


SECTION_SETTINGS = {
    "users": {
        'model': User,
        'form': UserForm,
        'formsets': [],
        'list_display': (
            ('username', 'Username'),
            ('email', 'Email'),
            ('first_name', 'First Name'),
            ('last_name', 'Last Name'),
            ('is_staff', "Staff Member"),
            ('phone_number', "Phone Number"),
            ('title', "Rank"),
        ),
        'search_fields':['username', 'first_name', 'last_name', 'email']
    },
    "products-list": {
        'model': Product,
        'form': ProductForm,
        'formsets': [inlineformset_factory(Product, ProductImage), inlineformset_factory(Product, Price), inlineformset_factory(Product, ProductAttribute), ],
        'list_display': (
            ('name', 'Name'),
            ('price_set', 'Price'),
            ('items_in_stock', 'Quantity'),
            ('active', 'Active'),
            ('featured', 'Featured'),
        ),
        'search_fields':['name', ]

    },
    'products-orders': {
        'model': Order,
        'form': OrderForm,
        'formsets': [
            inlineformset_factory(Order, OrderItem),
            inlineformset_factory(Order, OrderStatus),
            inlineformset_factory(Order, OrderVariable),
            inlineformset_factory(Order, OrderTaxDetail),
            inlineformset_factory(Order, OrderAuthorization),
            inlineformset_factory(Order, OrderPayment),
            inlineformset_factory(Order, OrderPaymentFailure),
         ],
        'list_display': (
            ('id', 'Order ID'),
            ('contact', 'Customer'),
            ('time_stamp', 'Timestamp'),
            ('total', 'Order Total'),
            ('balance_forward', 'Balance Forward'),
            ('status', "Status"),
            ('invoice', "Invoice"),
            ('packingslip', 'Packing Slip'),
            ('shippinglabel', 'Shipping Label'),
        ),
        #'search_fields':['orderitem__product__name', ]
        'search_fields':['contact__first_name', 'contact__last_name', 'pk']
    },
    'products-subscriptions': {
        'model': SubscriptionProduct,
        'form': SubscriptionProductForm,
        'formsets': [],
        'list_display': (
            ('product', 'Product'),
            ('recurring', 'Is Recurring'),
            ('recurring_times', 'Recurring Times'),
            ('expire_legnth', 'Expiration'),
        )
    },
    'products-categories': {
        'model': Category,
        'form': CategoryForm,
        'formsets': [],
        'list_display': (
            ('name', 'Name'),
            ('parent', 'Parent Category'),
            ('is_active', 'active'),
        )
    },
    'products-discounts': {
        'model': Discount,
        'form': DiscountForm,
        'formsets': [],
        # 'filters': (
        #     ("filter_name", (
        #         ("model_field", "Human_field"),
        #         ("model_field", "Human_field"),
        #     ),
        #     ("filter_name", (
        #         ("model_field", "Human_field"),
        #         ("model_field", "Human_field"),
        #     ),
        # ),
        'list_display': (
            ('description', 'Discount'),
            ('is_active', 'active'),
        )
    },
    "store": {
    },
    "reports": {
    },
    "communications": {
    },
    "products": {
    },
    "events": {
    },
    "events-list": {
        'model': Event,
        'form': EventForm,
        'formsets': [],
        'list_display': (
            ('name', 'name'),
            ('owner', 'owner'),
            ('starts_at', 'Starts at'),
            ('city', 'City'),
            ('state', 'State'),
            ('get_status_display', 'Status'),
        ),
        'search_fields':['name', 'owner__username' ],
        'search_date_fields':['starts_at' ]
    },
    "events-photos": {
        'model': EventPhoto,
        'form': EventPhotoForm,
        'formsets': [],
        'list_display': (
            ('title', 'title'),
            ('is_enabled', 'Is Enabled'),
        )
    },
    "documents": {
        'model': Document,
        'form': DocumentForm,
        'formsets': [],
        'list_display': (
            ('title', 'Title'), 
            ('document', 'Document')
        ),
    },
    "commissions": {
        'model': User,
        'form': UserForm,
        'formsets': [],
        'list_display': (
            ('ibo_number', 'IBO Number'), 
            ('first_name', 'First Name'),
            ('last_name', 'Last Name'),
            ('eligible', 'Eligible'),
            ('reason', 'Reason'),
            ('amount', 'Amount'),
         )
    },
    "page": {
        'model': FlatPage,
        'form': FlatPageForm,
        'formsets': [],
        'list_display': (
            ('url', 'URL'),
            ('title', "Title")
        ),
    },
    "notice": {
        'model': Notice,
        'form': NoticeForm,
        'formsets': [],
        'list_display': (
            ('title', "Title"),
        ),
    },
    "store-cart-items": {
        'model': CartItem,
        'form': CartItemForm,
        'formsets': [],
        'list_display': (
            ('cart', 'Cart'),
            ('product', 'Product'),
            ('quantity', 'Quantity'),
        ),
    },
    "store-order-line-items": {
        'model': OrderItem,
        'form': OrderItemForm,
        'formsets': [],
        'list_display': (
            ('order', 'Cart'),
            ('product', 'Product'),
            ('quantity', 'Quantity'),
            ('unit_price', 'Unit Price'),
            ('line_item_price', 'Line Item Price'),
            ('tax', 'Tax'),
            ('expire_date', 'Expiration Date'),
            ('completed', 'Completed'),
            ('discount', 'Discount'),
        ),
    },
    "store-order-payment-authorizations": {
        'model': OrderAuthorization,
        'form': OrderAuthorizationForm,
        'formsets': [],
        'list_display': (
            ('order', 'Order'),
            ('capture', 'Order Payment'),
            ('complete', 'Is Complete'),
        ),
    },
    "store-order-payments": {
        'model': OrderPayment,
        'form': OrderPaymentForm,
        'formsets': [],
        'list_display': (
            ('id', 'id'), ('order', 'order'),
            ('order', 'Order'),
            ('payment', 'Payment'),
            ('amount_total', 'Amount Total'),
            ('time_stamp', 'Created at'),
        ),
    },
    "store-product-orders": {
        'model': Order,
        'form': OrderForm,
        'formsets': [],
        'list_display': (
            ('id', 'Id'),
            ('contact', 'Contact'),
            ('time_stamp', 'Created at'),
            ('order_total', 'Order Total'),
            ('balance_forward', 'Balance Forward'),
            ('status', 'Status'),
            ('invoice', 'Invoice'),
            ('packingslip', 'Packing Slip'),
            ('shippinglabel', 'Shipping Label'),
        ),
    },
    "store-shopping-carts": {
        'model': Cart,
        'form': CartForm,
        'formsets': [],
        'list_display': (
            ('site', 'site'),
            ('customer', 'Customer'),
            ('date_time_created', 'Created at'),
        ),
    },
    "store-configurations": {
        'model': Config,
        'form': ConfigForm,
        'formsets': [],
        'list_display': (
            ('site', 'Site'),
            ('store_name', 'Store Name'),
        ),
    },

    "support-requests": {
        'model': SupportRequest,
        'form': SupportRequestForm,
        'formsets': [],
        'list_display': (
            ('by', 'By'),
            ('body', 'Body'),
            ('sent_on', 'Date'),
            ('subject', 'Subject'),
            ('status', 'Status'),
        ),
        'search_fields':['by__username', 'by__first_name', 'by__last_name', 'subject', 'status'],
        'search_date_fields': ["sent_on"],
    },

    'support-categories': {
        'model': SupportCategory,
        'form': SupportCategoryForm,
        'formsets': [],
        'list_display': (
            ('parent', 'Parent'),
            ('name', 'Name'),
        ),
    }
}
