from django.db import models

class CategoryManager(models.Manager):
    
    def children(self, section_obj):
        """
        Returns any children related to the passed section_obj.
        """
        return self.filter(parent=section_obj, is_enabled=True)
