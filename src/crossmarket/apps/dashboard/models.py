from django import forms
from django.db import models
from django.db.models import signals

from payment.forms import SimplePayShipForm, PaymentContactInfoForm
#from payment.modules.purchaseorder.forms import PurchaseorderPayShipForm
from payment.listeners import form_terms_listener
from satchmo_store.shop.models import Order
from signals_ahoy.signals import form_init

from apps.account.models import UserWebsite
from apps.dashboard.manager import CategoryManager
from apps.pricing.models import WholesaleDiscount

class CmsContent(models.Model):
    slug = models.SlugField()
    description = models.TextField(blank=True)
    heading = models.TextField(blank=True)
    sub_heading = models.TextField(blank=True)
    text = models.TextField(blank=True)
    def __unicode__(self):
        return self.slug

class Section(models.Model):
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children')
    title = models.CharField(max_length=50, help_text="Section title")
    slug = models.SlugField()
    description = models.TextField(help_text="Section description", blank=True)
    ordering = models.IntegerField(blank=True, null=True, help_text="Section ordering")
    is_enabled = models.BooleanField(default=True, help_text="Enable Section")

    objects = CategoryManager()

    class Meta:
        ordering = ['ordering', 'title']

    def has_children(self):
        return bool(Section.objects.children(self).count())

    def __str__(self):
        return self.title
    #insert into dashboard_section values (37, NULL, 'Site Management', 'site-management', '', 9, 1);
    #insert into dashboard_section values (38, 37, 'Page', 'page', '', 1, 1);
    #insert into dashboard_section values (39, 37, 'Notice', 'notice', '', 2, 1);
    #insert into dashboard_section values (39, 5, 'Detailed Reporting', 'detailed-reporting', '', 6, 1);

class OrderWebsite(models.Model):
    order=models.ForeignKey(Order)
    website=models.ForeignKey(UserWebsite)
    def __unicode__(self):
        return "Order #%d, Website: %s"%(self.order.pk, self.website.user.username)


class OrderType(models.Model):
    order=models.ForeignKey(Order)
    type=models.ForeignKey(WholesaleDiscount)
    def __unicode__(self):
        return "Order #%d: %s"%(self.order.pk, self.type)
