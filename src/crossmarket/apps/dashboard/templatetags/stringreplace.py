from django import template
from django.core.urlresolvers import reverse, NoReverseMatch


register = template.Library()


@register.filter
def replace_with_whitespce(value):
    return value.replace("_", " ")