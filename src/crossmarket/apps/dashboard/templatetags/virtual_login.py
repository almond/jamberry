import hashlib
from django import template
from django.conf import settings

register = template.Library()  

@register.filter()
def hash(type, id):
    hash = hashlib.md5()
    hash.update("%s:%s:%s" % (type, id, settings.ADMIN_HASH_SECRET))
    return hash.hexdigest().upper()

