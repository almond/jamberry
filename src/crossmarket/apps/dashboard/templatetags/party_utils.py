from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.conf import settings
from product.templatetags.satchmo_discounts import discount_price
from product.utils import find_best_auto_discount
from product.models import Product
from apps.events.models import Attendee, Event
import datetime
register = template.Library()

@register.filter
def allow_party_selection_in_opc(request):
    #return request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY]

    if (not request.user.is_authenticated()) and request.referred_by and has_upcoming_parties(request.referred_by):
        return True
    elif request.user.is_authenticated() and settings.WHOLESALE_DISCOUNT_SESSION_KEY in request.session and request.session[settings.WHOLESALE_DISCOUNT_SESSION_KEY].slug==settings.WHOLESALE_DISCOUNT_REGULAR_ORDER_SLUG and has_upcoming_parties(request.user):
        return True
    else:
        return False

@register.filter
def has_upcoming_parties(value):
    today=datetime.datetime.today()
    return Event.objects.filter(status=3, owner=value, starts_at__gte=today).exists()

@register.filter
def upcoming_parties(value):
    today=datetime.datetime.today()
    return Event.objects.filter(status=3, owner=value, starts_at__gte=today)

@register.filter
def is_party(value, request):
    if (request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY) and
    request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug==settings.PARTY_WHOLESALE_DISCOUNT_SLUG):
        return True
    return False

@register.filter
def has_party_cart(request):
    if (request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY) and
    request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug==settings.PARTY_WHOLESALE_DISCOUNT_SLUG):
        return True
    return False

@register.filter
def has_frontend_party(value, request):
    if request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT):
        return True
    return False

@register.filter
def party_name(value, request):
    return Event.objects.get(status=3, pk=request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT)).name

@register.filter
def can_attach_party(value, request):
    today=datetime.datetime.today()
    if (request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT) and Event.objects.filter(status=3, pk=request.session.get(settings.FRONTEND_PARTY_SESSION_SELECT), starts_at__gte=today).exists()):
        return True
    return False

@register.filter
def party_order_details(value, request, include_pcv=False, as_dict=False):
    if (request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY) and
    request.session.get(settings.WHOLESALE_DISCOUNT_SESSION_KEY).slug==settings.PARTY_WHOLESALE_DISCOUNT_SLUG):
        if settings.PARTY_ORDERDETAILS_SESSION_KEY in request.session:
            details=request.session[settings.PARTY_ORDERDETAILS_SESSION_KEY]
            

            products=[]
            for uid, purchases in details.items():
                products += purchases.keys()
            selected_product=Product.objects.filter(active=True, id__in=products)
            selected_product=list(selected_product)
            sale=find_best_auto_discount(selected_product)

            selected_product={str(p.id):p for p in selected_product}

            data={}
            for uid, purchases in details.items():
                userpurchases=[]
                for item, count in purchases.items():
                    p=selected_product[item]
                    purchase_data={'count':count, 'product':p, 'unit_price':float(discount_price(p, sale))}
                    purchase_data['sub_total']=float(count)*purchase_data['unit_price']
                    if include_pcv:
                        purchase_data['pcv']=0.65*purchase_data['sub_total']
                    userpurchases.append(purchase_data)
                data[Attendee.objects.get(pk=uid)]=userpurchases
            
            if as_dict:
                return data
            return data.items()

    return {}
