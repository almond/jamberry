from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

@register.filter
def get_field(value, arg):
    
    # Foreign Key Ref.
    if 'set' in arg:
        try:
            val = getattr(value, arg, "").all()[0]
        except IndexError:
            val = ''
        except:
            val = ''
    else:
        val = getattr(value, arg, "")
    # Refering to a function name.
    if callable(val):
        val = val()
    if val is None:
        val = ''
    return val

