from django import template
from apps.pricing.models import Offer
register = template.Library()


@register.tag
def offers(parser, token):
    try:
        tag_name, tmp, variable_name=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag is user improperly." % token.contents.split()[0])
    return OfferItems(variable_name)

class OfferItems(template.Node):
    def __init__(self, variable_name):
        self.variable_name=variable_name

    def render(self, context):
        context[self.variable_name]=Offer.objects.filter(active=True)

        return ''
