from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.conf import settings
from product.templatetags.satchmo_discounts import discount_price
from product.utils import find_best_auto_discount
from product.models import Product
from apps.pricing.models import Offer
from decimal import Decimal
import datetime
register = template.Library()

@register.filter
def need_more(cart):
    if cart:
        items=cart.cartitem_set.all()
        total_item_in_cart=0
        offerable_quant_total=0
        product_slug=None
        offer_value=Decimal(0.00)
        item_price=Decimal(15.00)
        for i in items:
            if i.product.slug in settings.DISC_PRODUCT_SLUGS:
                if not i.product.slug==settings.OFFER_PRODUCT_SLUG:
                    if i.quantity>1:
                        i.quantity=1
                        i.save()
                    product_slug=i.product.slug
                    offer_value=i.product.unit_price            
            else:
                total_item_in_cart=total_item_in_cart+i.quantity
                if i.product.slug not in settings.OFFERLESS_PRODUCTS_SLUG:
                    offerable_quant_total += i.quantity
                    item_price=i.product.unit_price

        offered_count=((-1)*offer_value)/item_price
        left_over=offerable_quant_total-offered_count
        if left_over>0:
            offers=Offer.objects.filter(active=True)
            apply_offer=None
            applicable_offers=[]
            for offer in offers:
                if left_over%(offer.buy+offer.free)==0:
                    apply_offer=True
                    applicable_offers.append({'need_more':0, 'offer':offer})
                elif (left_over+offer.free)%(offer.buy+offer.free)==0:
                    apply_offer=True
                    applicable_offers.append({'need_more':offer.free, 'offer':offer})
                else:
                    units=int(left_over/(offer.buy+offer.free))
                    if units>0:
                        apply_offer=True
                        applicable_offers.append({'need_more':0, 'offer':offer}) #we will show need more only of that is the no of free products

            if apply_offer:
                apply_offer=applicable_offers[0]    
                return apply_offer['need_more']

    return 0


