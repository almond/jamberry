from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

@register.filter
def makelink(value):
    if '/' in value:
        return value
    try:
        return reverse(value)
    except NoReverseMatch:
        return ''
        