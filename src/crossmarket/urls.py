from django.conf import settings
from django.conf.urls.defaults import patterns, include, url

from satchmo_store.urls import urlpatterns
from satchmo_utils import urlhelper

from apps.events.views import event_details, event_attendees, event_hostessportal, event_attendee_status


urlpatterns += patterns('',
     (r'^accounts/', include('apps.account.urls')),
     #(r'^manage/messages/', include('apps.messages.urls')),
     (r'^manage/tools/support/', include('apps.support.urls')),
     (r'^manage/', include('apps.manage.urls')),
     (r'^one-page-checkout/', include('apps.one_page_checkout.urls')),
     (r'^backend/dashboard/', include('apps.dashboard.urls')),
     (r'^tinymce/', include('tinymce.urls')),
     (r'^dashboard/$', 'apps.home.views.dashboard', {}),
     (r'^accounts/login/$', 'django.contrib.auth.views.login', {}),
     (r'^accounts/login/$', 'django.contrib.auth.views.login', {}),
     (r'^', include('apps.home.urls'), {}),
)

urlpatterns += patterns('',
    url(r'^parties/(?P<code>[\{\}\w-]+)/$', event_details, name="events_detail"),
    url(r'^parties/(?P<code>[\{\}\w-]+)/invite/$', event_attendees, name="events_attendees"),
    url(r'^parties/(?P<code>[\{\}\w-]+)/hostessportal/$', event_hostessportal, name="events_hostessportal"),
    url(r'^parties/(?P<code>[\{\}\w-]+)/attend/(?P<attendee_code>[\w]+)/$', event_attendee_status, name="events_attendee_status"),
    # url(r'^parties/(?P<code>[\{\}\w-]+)/attend/(?P<attendee_code>[\w]+)/(?P<status>yes|no|maybe|pending)$', event_attendee_status, name="events_attendee_status_auto"),
)

# This can be removed. Only used to test SOAP API
urlpatterns += patterns('',
    url(r'^testing_soap/', include("apps.api_wrapper.urls"), name="testing_soap"),
)

urlhelper.replace_urlpatterns(
    urlpatterns, [
        url(r'^(?P<parent_slugs>([-\w]+/)*)?(?P<slug>[-\w]+)/$', 'apps.dashboard.views.category_view', {}, name='satchmo_category'),
        url(r'^cart/$', 'apps.dashboard.views.cart_display', {}, 'satchmo_cart'),
    ]
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':  settings.MEDIA_ROOT, 'show_indexes': True}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':  settings.STATIC_ROOT, 'show_indexes': True}),
        (r'^static-admin/(?P<path>.*)$', 'django.views.static.serve', {'document_root':  settings.ADMIN_MEDIA_ROOT, 'show_indexes': True}),
    )
