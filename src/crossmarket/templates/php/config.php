<?php
$adminEmail='support@invent-themes.com';

//Translate this as you wish:
$nameHeader = 'Name';
$emailHeader = 'E-mail';
$subject = 'Harmony Contact Form';
$messageHeader = 'Message';
$errorResponseMessage = 'Ups, something went wrong. Please try again later.';
$succesResponseMessage = 'The message has been sent successfully, I will contact you soon.';
